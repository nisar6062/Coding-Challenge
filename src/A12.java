import java.util.HashMap;
import java.util.Map;

public class A12 extends Thread {

	A12() {
		System.out.println("in A");
	}

	void func() {
		System.out.println("Func A");
	}

	public static void over() {
	}

	@Override
	public void run() {
		System.out.println("runn" + this.getName());
		synchronized (A12.class) {
			try {
				sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("sync" + this.getName());
		}
	}

	public static void main(String args[]) {


		Map<String, String> map = new HashMap<>();

		map.put("aaa","1212");
		System.out.println(map);
		map.put("aaa",null);
		System.out.println(map);
		map.remove("aaa");
		System.out.println(map);

	}
}

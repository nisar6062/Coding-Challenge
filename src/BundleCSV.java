import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class BundleCSV {
    public static void main(String[] args) {

        String csvFile = "/Users/nisar.adappadathil/Downloads/bb.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "    ";
        Set<String> bundleSet = new HashSet<>();
        Set<String> bundleSortItemSet = new HashSet<>();
        Set<String> bundleSortShopSet = new HashSet<>();
        int bunDup = 0, bunShop = 0, bunItem = 0, lineCount = 0;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            System.out.println(line = br.readLine());
            while ((line = br.readLine()) != null) {
                lineCount++;
                String[] bundles = line.split(cvsSplitBy);
                if (bundles.length == 3) {
                    if (!"NULL".equals(bundles[0]) && !bundleSet.add(bundles[0]))
                        bunDup++;
                    if (!"NULL".equals(bundles[1]) && !bundleSortItemSet.add(bundles[1])) {
                        bunItem++;
                    }
                    if (!"NULL".equals(bundles[2]) && !bundleSortShopSet.add(bundles[2]))
                        bunShop++;
                }
            }
            Set<String> bundleDoesntSet = new HashSet<>();
            Set<String> bundleSortShopDoesntSet = new HashSet<>();
            for (String bundle : bundleSortItemSet) {
                if (!bundleSortShopSet.contains(bundle)) {
                    bundleSortShopDoesntSet.add(bundle);
                }
                if (!bundleSet.contains(bundle)) {
                    bundleDoesntSet.add(bundle);
                }
            }
            Set<String> bundleShopNotBundleYes = new HashSet<>();
            for (String bundle : bundleSet) {
                if (!bundleSortShopSet.contains(bundle)) {
                    bundleShopNotBundleYes.add(bundle);
                }
            }
            System.out.println("Line Size:" + lineCount);
            System.out.println("Bundle Size:" + bundleSet.size());
            System.out.println("Bundle_Shop Size:" + bundleSortShopSet.size());
            System.out.println("Bundle_item Size:" + bundleSortItemSet.size());
            System.out.println("Bundle Doesnt Size: " + bundleDoesntSet.size());
            System.out.println("Bundle_Shop Doesnt Size: " + bundleSortShopDoesntSet.size());
            System.out.println("Duplicates Bundle Size:" + bunDup);
            System.out.println("Duplicates Bundle_Shop Size:" + bunShop);
            System.out.println("Duplicates Bundle_item Size:" + bunItem);
            System.out.println("Bundle Doesnt: " + bundleDoesntSet);
            System.out.println("Bundle_Shop Doesnt: " + bundleSortShopDoesntSet);
            System.out.println("bundleShopNotBundleYes: " + bundleShopNotBundleYes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

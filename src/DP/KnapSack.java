package DP;

public class KnapSack {
    /***/
    public static int knapSack(int W, int[] wt, int[] val, int n) {
        // max(knapSack(W,wt,val,n-1), val[n-1]+knapSack(W-wt[n-1],wt,val,n-1))
        if (n == 0 || W == 0)
            return 0;
        if (W - wt[n - 1] < 0)
            knapSack(W, wt, val, n - 1);
        return Math.max(knapSack(W, wt, val, n - 1), val[n - 1] + knapSack(W - wt[n - 1], wt, val, n - 1));
    }

    public static int knapSackDP(int W, int[] wt, int[] val) {
        int dp[][] = new int[W + 1][val.length + 1];
        for (int i = 0; i <= W; i++) {
            for (int j = 0; j <= val.length; j++) {
                if (i == 0 || j == 0)
                    dp[i][j] = 0;
                else if (i - wt[j - 1] >= 0)
                    dp[i][j] = Math.max(dp[i][j - 1], val[j - 1] + dp[i - wt[j - 1]][j - 1]);
                else if (i - wt[j - 1] < 0)
                    dp[i][j] = dp[i][j - 1];
            }
        }
        // System.out.println(Arrays.deepToString(dp).replace("], ",
        // "]\n").replace("[[", "[").replace("]]", "]"));
        return dp[W][val.length];
    }

    private static int unboundedKnapsack(int W, int n, int[] val, int[] wt) {
        // dp[i] is going to store maximum value
        // with knapsack capacity i.
        int dp[] = new int[W + 1];
        // Fill dp[] using above recursive formula
        for (int i = 0; i <= W; i++) {
            for (int j = 0; j < n; j++) {
                if (wt[j] <= i) {
                    dp[i] = Integer.max(dp[i], dp[i - wt[j]] + val[j]);
                }
            }
        }
        return dp[W];
    }

    public static void main(String[] args) {
        int val[] = new int[] { 60, 100, 120 };
        int wt[] = new int[] { 10, 20, 30 };
        int W = 50;
        int n = val.length;
        System.out.println(knapSack(W, wt, val, n));
        System.out.println(knapSackDP(W, wt, val));
        System.out.println(unboundedKnapsack(W, val.length, val, wt));
    }
}

package DP;

import java.util.Arrays;

public class PerfectSum {
    /**
     * Given a set of non-negative integers, and a value sum, determine if there
     * is a subset of the given set with sum equal to given sum. Examples: set[]
     * = {3, 34, 4, 12, 5, 2}, sum = 9 Output: True //There is a subset (4, 5)
     * with sum 9.
     */
    // include or exclude the element subsetsum(set, n-1, sum) || subsetsum(set,
    // n-1, sum - set[n-1])
    // Returns true if there is a subset of set[] with sun equal to given sum
    static boolean isSubsetSumRecursive(int set[], int n, int sum) {
        if (n == 0 && sum != 0)
            return false;
        if (sum == 0)
            return true;
        else if (sum - set[n - 1] < 0)
            return isSubsetSumRecursive(set, n - 1, sum);
        return isSubsetSumRecursive(set, n - 1, sum) || isSubsetSumRecursive(set, n - 1, sum - set[n - 1]);
    }

    static boolean isSubsetSumDP(int set[], int sum) {
        boolean subset[][] = new boolean[sum + 1][set.length + 1];
        for (int i = 0; i <= sum; i++) {
            for (int j = 0; j <= set.length; j++) {
                if (i == 0)
                    subset[i][j] = true;
                else if (j == 0 && i != 0)
                    subset[i][j] = false;
                else if (i - set[j - 1] < 0)
                    subset[i][j] = subset[i][j - 1];
                else
                    subset[i][j] = subset[i][j - 1] || subset[i - set[j - 1]][j - 1];
            }
        }
        System.out.println(Arrays.deepToString(subset).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));
        return subset[sum][set.length];
    }

    static boolean isSubsetSum(int set[], int n, int sum) {
        // The value of subset[i][j] will be true if there
        // is a subset of set[0..j-1] with sum equal to i
        boolean subset[][] = new boolean[sum + 1][n + 1];

        // If sum is 0, then answer is true
        for (int i = 0; i <= n; i++)
            subset[0][i] = true;

        // If sum is not 0 and set is empty, then answer is false
        for (int i = 1; i <= sum; i++)
            subset[i][0] = false;

        // Fill the subset table in botton up manner
        for (int i = 1; i <= sum; i++) {
            for (int j = 1; j <= n; j++) {
                subset[i][j] = subset[i][j - 1];
                if (i >= set[j - 1])
                    subset[i][j] = subset[i][j] || subset[i - set[j - 1]][j - 1];
            }
        }
        //System.out.println(Arrays.deepToString(subset).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));
        return subset[sum][n];
    }

    /* Driver program to test above function */
    public static void main(String args[]) {
        int set[] = { 2,3,5,6,8,10 };
        int sum = 10;
        int n = set.length;
        if (isSubsetSum(set, n, sum))
            System.out.println("Found a subset with given sum");
        else
            System.out.println("No subset with given sum");
        System.out.println(isSubsetSumRecursive(set, n, sum));
        System.out.println(isSubsetSumDP(set, sum));
    }
}

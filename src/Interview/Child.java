package Interview;

public class Child extends Parent {
	Child(){ System.out.println("@@@");
		
	}

	public void foo() {
		System.out.println("chd");
	}

	public void hoo() {
		System.out.println("hoo");
	}

	public static void main(String[] args) {
		Parent p = new Child();
		//p.foo();
		int arr[] = { 3, 6, 8, 11, 14 };
		System.out.println(find(arr,11,arr.length/2+1));
	}

	public static int find(int[] arr, int a, int mid) {
		if (arr[mid-1] == a)
			return mid-1;
		else if (arr[mid-1] > a) {
			return find(arr,a,mid-(mid/2+1));
		} else if (arr[mid-1] < a) {
			return find(arr,a,mid+(mid/2+1));
		}
		return -1;
	}

}

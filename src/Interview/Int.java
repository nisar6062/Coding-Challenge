package Interview;

import java.util.LinkedList;

public class Int {

	public synchronized void fun() throws InterruptedException{
		//synchronized(Int.class){
			System.out.println(Thread.currentThread().getName()+" In fun");
			Thread.sleep(900);			
	    //}
		System.out.println("fun end: "+Thread.currentThread().getName());
	}
    public synchronized void cool() throws InterruptedException{
    	System.out.println("cool In");
    	synchronized(Int.class){
    		System.out.println("cool: "+Thread.currentThread().getName());
    	}
		
	}
    public void doo() throws InterruptedException{
    	//Thread.sleep(900);
		System.out.println("doo: "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		LinkedList<Integer> lst =new LinkedList<Integer>();
		lst.add(12);lst.add(55);lst.add(77);
		System.out.println(lst.getFirst());

	}

}

package Interview;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MaxSum {

	public static String removeMaxChar(String input) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < input.length(); i++) {
			String ch = input.charAt(i) + "", temp = input;
			int k = 0;
			if (map.get(ch) == null) {
				while (temp.indexOf(ch) != -1) {
					temp = temp.substring(temp.indexOf(ch) + 1);
					k++;
				}
				// System.out.println (ch + ":" + k);
				map.put(ch, k);
			}
		}
		int max=0; String key=null;
		Set<Entry<String, Integer>> set = map.entrySet();
		Iterator<Entry<String, Integer>> it= set.iterator();
		while(it.hasNext()){
			Entry<String, Integer> e=it.next();
			if(max<e.getValue()){
				max=e.getValue(); key=e.getKey();
			}
		}
		System.out.println(max+" "+key);
		input=input.replaceAll(key,"");
		return input;
	}

	public static void main(String[] args) {
		String hh = "abcddadfdgdfa";
		System.out.println(removeMaxChar(hh));
	}

}

package Interview;

 

public class Samp1  {
	int g=8;
	public static void main(String args[]) throws Exception {
		int input[] = {6,7,1,2,3,4,5};
		System.out.println("Result: "+searchRotatedArray(input,2,7));
		Samp1 s=new Samp1(),s2=new Samp1();
		
		s.g=11;System.out.println(s.hashCode()+" &&"+s.g);
		String h=new String("qw");System.out.println(h.hashCode()); 
		h="as"; System.out.println(h.hashCode());
		
	}
	public static int searchRotatedArray(int input[],int noOfRotation,int searchEntry) {
		//input = [6-7-1-2-3-4-5] noOfRotation=2
		int len =  input.length;
		int mid = len/2 ; 		
		int log =(int)31 - Integer.numberOfLeadingZeros(len);  // to find log to base two of the input length
		// The logic is same as binary search in an array.The change is while accessing the array
		// If we want to access n th element of the array then access (n+noOfRotation)%len th element
		if(input[(mid+noOfRotation)%len]==searchEntry){
			return (mid+noOfRotation)%len;
		}
		int low = 0, high = len - 1;
		// Iterate logarithmic number of times of input length in the loop
		while (log>=0) {
		       mid = low + ((high - low) / 2);  	       
		       if (input[(mid+noOfRotation)%len] > searchEntry){
		           high = mid - 1;
		       }else if (input[(mid+noOfRotation)%len] < searchEntry){
		           low = mid + 1;
			   }else{
		           return (mid+noOfRotation)%len; // found the element returning the index
			   }
		       log--;
		 }
		return -1;
	}
	 
}

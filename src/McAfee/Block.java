package McAfee;

public class Block {
	
	public static String biggestSquare(String[] input1) {
		int len = input1[0].split("#").length;
		int rCnt = 0, k = 0, prei = 0, prej = 0;
		String data[][] = new String[input1.length][len];
		for (String hh : input1) {
			String val[] = hh.split("#");
			data[k] = val;
			k++;
		}
		for (int i = 0; i < (data.length - rCnt); i++) {
			for (int j = 0; j < (len - rCnt); j++) {
				int u = 1;
				boolean isSq = true;
				while (isSq && u < (len - j)) {
					if (i == 4) {
						int y = 0;
					}
					for (int m = 0; m <= u; m++) {
						for (int n = 0; n <= u; n++) {
							if (data[i + m][j + n].equals("0")) {
								isSq = false;
								break;
							}
						}
						if (!isSq) {
							break;
						}
					}
					if (isSq)
						u++;
				}
				if (rCnt < u) {
					rCnt = u;
					prei = i;
					prej = j;
				}

			}
		}
		StringBuffer res = new StringBuffer("{");
		for (int i = prei; i < prei + rCnt; i++) {
			res.append("(");
			for (int j = prej; j < prej + rCnt; j++) {
				res.append(i + "#" + j + ",");
			}
			res = new StringBuffer(res.substring(0, res.length() - 1));
			res.append("),");
		}
		res = new StringBuffer(res.substring(0, res.length() - 1));
		res.append("}");

		return res.toString();
	}
	public static void main(String[] args) {
		
		String inp[]={"0#1#1#1#0#1#0#1","1#0#1#0#0#0#0#1","0#0#0#1#0#1#0#0","1#1#1#1#1#0#0#1","1#1#1#1#0#1#1#1","1#1#1#1#0#1#1#1","1#1#1#1#1#1#1#1","1#1#0#1#0#0#1#1"};
		//String inp[]={"0#1#1#1#0#1#0#1","1#0#1#0#0#0#0#1","0#0#0#1#0#1#0#0","1#1#0#0#0#0#0#1","1#1#0#0#0#1#1#1","1#1#0#0#0#1#1#1","1#1#1#1#1#1#1#1","1#1#0#1#0#0#1#1"};
		System.out.println(biggestSquare(inp));
		
		
	}

}

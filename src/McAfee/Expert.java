package McAfee;

import java.util.ArrayList;
import java.util.Collections;

public class Expert {

	public static String[] sortPoints(String[] input1, int input2, int input3) {
		int k = 0;
		String res[] = new String[input1.length];
		String check = "";
		ArrayList<Integer> temp = new ArrayList<Integer>(), dist = new ArrayList<Integer>();
		for (String in : input1) {
			int x = Integer.parseInt(in.split("#")[0]);
			int y = Integer.parseInt(in.split("#")[1]);
			dist.add(((input2 - x) * (input2 - x)) + ((input3 - y) * (input3 - y)));
			temp.add(((input2 - x) * (input2 - x)) + ((input3 - y) * (input3 - y)));
			k++;
		}
		Collections.sort(dist);
		k = 0;
		for (int i = dist.size() - 1; i >= 0; i--) {
			int idx = temp.indexOf(dist.get(i)), c = -1;
			while (check.indexOf(idx + ":", c + 1) != -1 && idx < input1.length - 1) {
				c = check.indexOf(idx + ":", c + 1);
				idx++;
			}
			check = check + idx + ":";
			res[k] = input1[idx];
			k++;
		}
		return res;
	}

	public static void main(String[] args) {

		// String pp[] = {"-3#8","9#3","-1#3","-8#9","1#10","-1#4","-10#-10","2#8"};
		String pp[] = { "2#0", "-2#0", "0#2", "0#-2", "1#10", "-1#4", "-10#-10", "2#8" };
		System.out.println(sortPoints(pp, 0, 0));

	}

}

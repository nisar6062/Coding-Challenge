
public class Prime {

	private static int[] getPrime(int num){
		
		int arr[]=new int[num];int k=0;int a=2;
		while (num>0) {
			boolean isPrime=true; int g=(int)Math.floor(Math.sqrt(a));
			for (int i=2;i<=g;i++){
				if(a%i==0){
					isPrime=false; break;
				}
			} 			
			if(isPrime){
				num--; arr[k]=a; k++;
			}	a++;		
		}
		
		return arr;
	}
	public static void mainvv(String[] args) {
		
		int h[]=getPrime(7);
		for (int i=0;i<h.length;i++){
			System.out.println(h[i]);
		}
		
	}
	public static void main(String[] args) throws java.lang.Exception {
		java.io.BufferedReader br = new java.io.BufferedReader (new java.io.InputStreamReader (System.in));
		long noOfCases = Integer.parseInt(br.readLine());
		while (noOfCases>0){
			String h[]=br.readLine().split(" ");
			long a = Long.parseLong(h[0]), b = Long.parseLong(h[1]);
			System.out.println((Math.abs(a)+Math.abs(b)));			
			noOfCases--;
		}	
	}
}

package Puzzles;

public class BitonicSequence {
	   
   public static int getSeq(int[] input) {
		int inc [] = new int[input.length],
			dec [] = new int[input.length],incdec [] = new int[input.length],rev [] = new int[input.length];
		int decmax=0,incmax=0,res=0;
		
		dec[input.length-1]=1;  inc[input.length-1]=1;rev[0]=1;
		for(int i=input.length-2; i>=0; i--){	
			decmax=1;incmax=1;
			for(int j=i+1; j<input.length; j++){
				if(input[j] > input[i] && incmax < (inc[j]+1)){
					incmax=inc[j]+1;
				}
				if(input[j] < input[i] && decmax < (dec[j]+1)){
					decmax=dec[j]+1;
			    }
			}
			inc[i]=incmax;
			dec[i]=decmax;
		}
		for(int i=1; i<input.length; i++){	
			incmax=1;
			for(int j=i-1; j>=0; j--){
				if(input[j] < input[i] && incmax < (rev[j]+1)){
					incmax=rev[j]+1;
				}
			}
			rev[i]=incmax;
		}
		for (int i=input.length-3; i>=0; i--){
			incdec[i]=rev[i]+dec[i]-1;
		}
		res=0;
		for (int i=0; i<input.length; i++){
			if (res < inc[i])
				res = inc[i];// System.out.println("inc");
			if(res<dec[i])
				res=dec[i];//System.out.println("end");
			if(res<incdec[i])
				res=incdec[i];//System.out.println("id");
		}
		return res;
	}

	public final static void main(String[] args) {
		
		//int inp[] ={1,2,3,4,5,6,8,13,21,43,45,44,42,41,40,67,39};
		int inp[] ={1,4,6,8,7,2,3,6,5,10};
		System.out.println(getSeq(inp));				
	}

}

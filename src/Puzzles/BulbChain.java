package Puzzles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class BulbChain {
	public static int colorSequences(String input1, String input2) 
	{		
		int len1=input1.length(), len2=input2.length(); int max=0,k=0; String smax="";
		HashMap<Integer,String> map = new HashMap<Integer,String>();
		if (len1<len2){
			String hh=input1; input1=input2; input2=hh;
			len1=input1.length(); len2=input2.length();
		} System.out.println(input1+" ** "+input2);	
		if(input1.indexOf(input2)!=-1){
			return len1-len2;
		}
		int i=0;
		while(i<len2){  
			boolean isIn=false,isOut=false;
			k=i+1; String h=input2.substring(i,k); 
			while(input1.indexOf(h)!=-1){
				k++;
				if(k>len2){
					isOut=true;break;
				}
				h=input2.substring(i,k);  isIn=true;
			}
			if (isIn){
				if(max<(k-i-1))
					max=k-i-1; 
				if(k>len2){
					smax=h;
				}else
					smax=h.substring(0,h.length()-1);
				map.put(i, smax);  i=k-1;  isOut=false;
			}
			else
				i++;
			if(isOut){
				map.put(i, h); break;
			}
		}
		Set<Integer> set=  map.keySet();
		Iterator<Integer> it =set.iterator();
		int cnt=0;  int lpp=0; k=0; int real=0; int l1=0,l2=0; String gg="";
		List<Integer> arr=new ArrayList<Integer>();
		while(it.hasNext())
		{
			int a=(int)it.next(); String g=map.get(a);			
			int f= input1.indexOf(g)-l1;
			int n=a-l2;		
			arr.add(a);
			if(f<0){
				if(g.length()>gg.length()){
					f=f*-1-2; int pp=f>n?f:n;int y=0;
					if(k>=2){
						y=arr.get(k-2); pp=input1.indexOf(g)-(y+map.get(y).length());
					}else{
						pp=input1.indexOf(g)>a?input1.indexOf(g):a;
					}
					
					real=real-lpp+pp;
					l1= input1.indexOf(g)+g.length(); l2= a+g.length(); lpp=pp; gg=g;
				}
			}else{
				int pp=f>n?f:n;
				real+=pp;
				l1= input1.indexOf(g)+g.length(); l2= a+g.length(); lpp=pp; gg=g;
			}
			 
			System.out.println(a+" * "+g+" fn:"+f+" "+n+" cnt:"+cnt+" real:"+real);	
			k++;
		}
		l1=input1.length()-l1;  l2=input2.length()-l2; l1=l1>l2?l1:l2; real+=l1;
		System.out.println("real:  "+real);
		if(max==0)
			return len1;
		else
			return real;		
	}
	public static void main(String[] args) {
		System.out.println("Result: "+colorSequences("abcdefghi","efgabc"));

	}
}

package Puzzles;

public class CognizantContest {

	// Same as Grid!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	public static int leastAmount (String[] input) {
		int m = input.length;
		int n = input[0].split("#").length;
		int arr[][] = new int[m][n],temp[][] = new int[m][n], k = 0, cnt = 0;
		for (int i = 0; i < m; i++) {
			String strArr[] = input[i].split("#");
			for (int j = 0; j < n; j++) {
				arr[i][j] = Integer.parseInt(strArr[j].trim());
				k++;
			}
		} 
		
		temp[m-1][n-1] = arr[m-1][n-1] ; 
		for (int i = m-1; i >=0; i--) {
			for (int j = n-1; j >=0; j--) {
				if(!(i==m-1 && j==n-1)){
					if (i==m-1){
						temp[i][j]=arr[i][j]+temp[i][j+1];
					}else if (j==n-1){
						temp[i][j]=arr[i][j]+temp[i+1][j];
					}else{
						if((temp[i+1][j]) <= temp[i][j+1]){
							temp[i][j]=arr[i][j]+temp[i+1][j];
						}else if((temp[i+1][j]) > temp[i][j+1]){
							temp[i][j]=arr[i][j]+temp[i][j+1];
						}
					}
				}				 
			}
		}
		
		return temp[0][0];
	}
	 
	public static void main(String[] args) {
		String h[]={"3#44#75","21#98#60"};
		String h2[]={"2#6#8#6#9","2#5#5#5#0","1#3#8#8#7","3#2#0#6#9","2#1#4#5#8","5#6#7#4#7"};
		
		String cc ="{{2,6,8,6,9},{2,5,5,5,0},{1,3,8,8,7},{3,2,0,6,9},{2,1,4,5,8},{5,6,7,4,7}}";	
		System.out.println(leastAmount(h2));

	}

}
 
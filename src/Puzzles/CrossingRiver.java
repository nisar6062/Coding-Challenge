package Puzzles;

public class CrossingRiver {

	public static int bridge(String[] input1) {
		int res=0,len=input1.length,ans=0;
		for(int i=0;i<len-1;i++){
			int a1=Integer.parseInt(input1[i].split("#")[0]); int b1=Integer.parseInt(input1[i].split("#")[1]);
			res=0;  int temp=b1;
			for(int j=0;j<len;j++){
				if(j!=i){
					int a2=Integer.parseInt(input1[j].split("#")[0]); int b2=Integer.parseInt(input1[j].split("#")[1]);
					
					if(b2>temp && a2>a1){
						temp=b2; res++; System.out.println("  "+a1+":"+b1+" -- "+a2+":"+b2);
					}
				}				
			} System.out.println(i+" * "+res);
			if(ans<res)
				ans=res;
		}
		System.out.println(ans+1);
		return ans+1;
	}

	public static void main(String[] args) {
		String in[]={"1#1","2#2","3#3","4#1"};
		//String in[]={"1#3","2#1","3#2"};
		bridge(in);
	}

}

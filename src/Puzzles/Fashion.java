package Puzzles;

public class Fashion {

	public static int fashion(int[] input1) {
		
		int res=0;
		for (int i=0;i<input1.length/2;i++){
			if(input1[i] == input1[input1.length-1-i]){
				res+=input1[i];
			}
		}
		return res+input1[input1.length/2];
	}

	public static void main(String[] args) {
		int inp[]={5,6,7,3,5};
		System.out.println(fashion(inp));
	}

}

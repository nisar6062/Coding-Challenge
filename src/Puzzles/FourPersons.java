package Puzzles;

public class FourPersons {
	public static int get_height(String[] input1)
    {
		int len=input1.length,k=0;
		for(int i=0;i<len-1;i++){
			int a1=Integer.parseInt(input1[i].split("#")[0]); int b1=Integer.parseInt(input1[i].split("#")[1]);
			if(a1<4 || a1>7 || b1<0 || b1>11){
				return -1;
			}
			int ht= 12*a1 + b1;  			
			for(int j=i+1;j<len;j++){
				int a2=Integer.parseInt(input1[j].split("#")[0]); int b2=Integer.parseInt(input1[j].split("#")[1]);
				if(a2<4 || a2>7 || b2<0 || b2>11){
					return -1;
				}
				int ht2= 12*a2 + b2;
				if(ht>ht2){
					k++;  
				}
			}
		}
		
		return k;
    }
	public static void main(String ar[]){
		String in[] = {"5#8","5#7","6#0","5#7"};
		System.out.println(get_height(in));
	}
}

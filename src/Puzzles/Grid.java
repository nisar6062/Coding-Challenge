package Puzzles;

public class Grid {

	public static int leastAmount(String input) {
		int m = input.split(",").length;
		int n = input.split(",")[0].split("#").length;
		System.out.println(m + " ** " + n);
		int arr[][] = new int[m][n], k = 0, cnt = 0;
		input = input.substring(1, input.length() - 1);
		input = input.replace(',', '#');
		String strArr[] = input.split("#");
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				arr[i][j] = Integer.parseInt(strArr[k].trim());
				k++;
			}
		}
		cnt = arr[0][0] + arr[0][1];
		int t1 = 0, t2 = 0, pre = 1;
		for (int i = 0; i < m; i++) {
			for (int j = pre; j < n; j++) {
				if (i == m - 1) {
					pre = j + 1;
					if (j == n - 1)
						break;
					cnt += arr[i][j + 1];
				} else if (j == n - 1) {
					pre = j;
					cnt += arr[i + 1][j];
					break;
				} else {
					t1 = arr[i][j + 1];
					t2 = arr[i + 1][j];
					if (t1 < t2) {
						pre = j + 1;
						cnt += t1;
					} else {
						pre = j;
						cnt += t2;
						break;
					}
				}

			}
		}
		return cnt;
	}
	 
	public static void main(String[] args) {
		
		//String h="{2#6#8#6#9,2#5#5#5#0,1#3#8#8#7,3#2#0#6#9,2#1#4#5#8,5#6#7#4#7}";
		String h="{3#44#75,21#98#60}";
		//int gg[][]={{2,6,8,9},{2,5,5,0},{1,3,8,8,7},{3,2,0,6,9},{2,1,4,5,8},{5,6,7,4,7}};
		System.out.println(leastAmount(h));

	}	

}

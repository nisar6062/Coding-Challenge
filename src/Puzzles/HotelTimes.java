package Puzzles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HotelTimes {

	public static int profitValue(String[] input1)
	{		
		String data[]=new String[input1.length];int k=0;
		ArrayList<String> arr = new ArrayList<String>();
		for(String h: input1){
			String temp=""; 
			String h0= h.split("#")[0];
			String h1= h.split("#")[1];
			if(h0.length()==3)
				h0="0"+h0;
			if(h1.length()==3)
				h1="0"+h1;
			if(h0.equals("12AM")){
				temp="0#";
			}
			else if(h0.indexOf("AM")!=-1 || h0.equals("12PM")) {
				temp=(h0.substring(0,h0.length()-2))+"#";
			}
			else if(h0.indexOf("PM")!=-1) {
				temp= Integer.parseInt(h0.substring(0,h0.length()-2))+12+"#";
			}
			if(h1.equals("12AM")){
				temp+="0";
			}
			else if(h1.indexOf("AM")!=-1 || h1.equals("12PM")) {
				temp+=(h1.substring(0,h1.length()-2));
			}
			else if(h1.indexOf("PM")!=-1) {
				temp+= Integer.parseInt(h1.substring(0,h1.length()-2))+12+"";
			}
			data[k]=temp; k++;  arr.add(temp);	System.out.println(temp);		
		}
		Collections.sort(arr); System.out.println(arr);
		Collections.sort(arr, new arrComp());  System.out.println("&& "+arr);
		String sArr=arr.toString(); System.out.println("SArr "+sArr);
		int in=0,out=0,cnt=0;
		for(String h: arr){ 
			int a= Integer.parseInt(h.split("#")[0]); int b= Integer.parseInt(h.split("#")[1]);
			if(in==0 && out==0){				 
				boolean not=true;
				if((b-a)>1){
					int cut=0;
					for(int i=a+1;i<b;i++){
						if(sArr.indexOf("#"+i)!=-1 || sArr.indexOf("#0"+i)!=-1){
							cut=i;not=false;break;
						}
					}
					if(!not){
						not=true;
						for(int i=cut;i<b;i++){
							if(sArr.indexOf(", "+i)!=-1 || sArr.indexOf(", 0"+i)!=-1){
								not=false;  break;
							}
						}
					}
					
				}
				if(not){
					in=a;out=b;
					cnt++;
				}
			}
			else if((a<in && b <=in)){
				  //System.out.println("cnt "+cnt+"in "+in+"out "+out);
				boolean not=true;
				if((b-a)>1){
					int cut=0;
					for(int i=a+1;i<b;i++){
						if(sArr.indexOf("#"+i)!=-1 || sArr.indexOf("#0"+i)!=-1){
							cut=i;not=false;break;
						}
					}
					if(!not){
						not=true;
						for(int i=cut;i<b;i++){
							if(sArr.indexOf(", "+i)!=-1 || sArr.indexOf(", 0"+i)!=-1){
								not=false;  break;
							}
						}
					}					
				}
				if(not){
					cnt++;  in=a;
				}
			}
			else if(b>out  && a>=out){
				 //System.out.println("cnt "+cnt+"in "+in+"out "+out);
				boolean not=true;
				if((b-a)>1){
					int cut=0;
					for(int i=a+1;i<b;i++){
						if(sArr.indexOf("#"+i)!=-1 || sArr.indexOf("#0"+i)!=-1){
							cut=i;not=false;break;
						}
					}
					if(!not){
						not=true;
						for(int i=cut;i<b;i++){
							if(sArr.indexOf(", "+i)!=-1 || sArr.indexOf(", 0"+i)!=-1){
								not=false;  break;
							}
						}
					}					
				}
				if(not){
					cnt++;  out=b; 
				}
			}
		
		}  //System.out.println("cnt "+cnt+"in "+in+"out "+out);
	    return cnt*500;
	}
	private static class arrComp implements Comparator<String>{
		@Override
		public int compare(String a, String b) {
			int a2= Integer.parseInt(a.split("#")[0]) - Integer.parseInt(a.split("#")[1]);
			int b2= Integer.parseInt(b.split("#")[0]) - Integer.parseInt(b.split("#")[1]);
			if(a2>b2)
				return -1;
			else if(a2<b2)
				return 1;
			else
				return 0;
		}		
	}
	public static void main(String[] args) {
		String hh[] ={"6AM#8AM","11AM#1PM","7AM#3PM","1PM#4PM","10AM#12PM","2PM#4PM","7AM#10AM","8AM#9AM"};
		//String g[] ={"8AM#9AM","7AM#7PM","1PM#3PM","10AM#12PM"};
		System.out.println(profitValue(hh));
	}
}

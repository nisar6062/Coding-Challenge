package Puzzles;

public class LongestSubArray {

	public static int longestSeq(int[] input1) {
		int res=0,temp=0, tr=0; 
		int len =input1.length;
		int data[]= new int[len]; data[len-1]=1;
		
		for(int i=len-2;i>=0;i--){
			tr=1;	temp = input1[i]; 		
			for(int j=i+1; j<len; j++){
				if(temp<input1[j] && tr<=data[j]){
					tr=data[j]+1; 
				}
				
			}
			data[i]=tr;
		}
		for (int h:data){
			if(res<h)
				res=h;
		}
		return res;
	}

	public static void main(String[] args) {
		int in[] ={40,30,11,31,12,32,22,25,29};
		System.out.println("Res : "+longestSeq(in));
	}

}

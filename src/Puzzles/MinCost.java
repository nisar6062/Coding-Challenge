package Puzzles;

public class MinCost {

	public static String minimumCost(String input)
    {
		int m=input.split("}").length;
		int n=input.split(",").length/m;  System.out.println(m+" :: "+n);
		input=input.replace('{',' ');  input=input.replace('}',' '); 
		String strArr[] = input.split(","); String path="";
	    int arr[][] = new int[m][n];
		int t1=0,t2=0,t3=0,t4=0,k=0;
		for (int i=0;i<m;i++){
			for (int j=0;j<n;j++){
				arr[i][j]= Integer.parseInt(strArr[k].trim());k++;
			}
		}
		int cnt=arr[0][0];
		int pre=0;
		for (int i=0;i<m;i++){
			for (int j=pre;j<n;j++) {
				if (i==m-1) {
					cnt+=arr[i][j+1]; path+="R"; break;
				}
				else if (j==n-1) {
					cnt+=arr[i+1][j]; path+="B"; break;
				}
				else if (i==m-2 && j==n-2) {
					cnt+=arr[i+1][j+1]; path+="D";i++; break;
				}
				else{
					t1 = arr[i][j+1]; t2 = arr[i+1][j]; t3 = arr[i+1][j+1]; 
					if(t1<=t2 && t1<=t3){
						cnt+= t1; path+="R";
					}
					else if(t3<=t2 && t3<=t1){
						cnt+= t3; pre=j+1;  path+="D"; break;
					}
					else if(t2<=t3 && t2<=t1){
						cnt+= t2; pre=j;  path+="B"; break;
					}
				}				
			}
		}
		
       return cnt+","+path;
    }
	
	public static void main(String[] args) {

       //String h ="{{5,7,2,4},{1,8,1,3},{6,2,9,5},{1,6,2,8}}"; 
       /*String gg[] =h.split("}");
       //for (String f:gg){System.out.println(f);}
       h=h.substring(1,h.length()-1);
       h=h.replace('{',' ');  h=h.replace('}',' '); System.out.println(h);
       String g[] =h.split(",");
       for (String f:g){System.out.println(f);}  System.out.println(g.length);*/
		
		
		String h ="{{2,6,8,6,9},{2,5,5,5,0},{1,3,8,8,7},{3,2,0,6,9},{2,1,4,5,8},{5,6,7,4,7}}";	
       System.out.println(minimumCost(h));
	}

}

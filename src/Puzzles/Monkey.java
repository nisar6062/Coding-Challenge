package Puzzles;

public class Monkey {

	public static int monkey(int[] input1) {
		int max = 0;
		int len = input1.length, k = 0;int data[] = new int[len*len-1];
		for (int i = 0; i < input1.length; i++) {
			for (int j = i+1; j < input1.length; j++) {
				int t= len-j+i;
				int dist = Math.abs(j - i) < t ? Math.abs(j - i) : t;
				data[k] = input1[j] + input1[i] + dist;
				//System.out.println("ij: "+i+" :"+j+" :"+data[k]); 
				k++; 
			}
		}
		for (int y : data) { 
			if (max < y)
				max = y;
		}
		return max;
	}

	public static void main(String[] args) {
		int input1[] = { 8,10,3,5,2,1,1,9,5,2 };
		System.out.println("res: "+monkey(input1));
	}

}

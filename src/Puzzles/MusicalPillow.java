package Puzzles;

import java.util.ArrayList;

public class MusicalPillow {

    public static int getWinner(int input1,int input2)
    {
    	ArrayList<Integer> arr = new ArrayList<Integer>();
        for (int i=0;i<input1;i++){
        	arr.add(i+1);
        }
        int i=input2-1,cnt=0;        
        while(cnt<input1){ 
        	arr.remove(i-cnt);cnt++;
        	if(i-cnt+input2 >= arr.size()){
        		i=(i-cnt+input2) % arr.size(); i=i+cnt;
        		
        	}
        	else{
        		i=i+input2;
        	}         	
        	if(arr.size()==1){
        		break;
        	}
        }
    	return arr.get(0);
    }
	
	public static void main(String[] args) {
		System.out.println(getWinner(5,2));
	}
}

package Puzzles;

import java.util.ArrayList;
import java.util.Collections;

public class RollingDice {

	
	public static String rollingdice(int[] input1,int[] input2)
    {
	   ArrayList<Integer> arr1 = new ArrayList<Integer>();
	   ArrayList<Integer> arr2 = new ArrayList<Integer>();
	   for (int i=0;i<input1.length;i++)
	   {
		   arr1.add(input1[i]); arr2.add(input2[i]); 
	   }
	   Collections.sort(arr1);  Collections.sort(arr2);
	   if (arr1.equals(arr2))
		   return "Lucky";
	   else
           return "Unlucky";
    }
	
	public static void main(String[] args) {

      ArrayList<Integer> arr1 = new ArrayList<Integer>();
      ArrayList<Integer> arr2 = new ArrayList<Integer>();
      arr1.add(2); arr1.add(44); arr1.add(23);arr1.add(0);
      arr2.add(2); arr2.add(44); arr2.add(23);arr2.add(0);
     // Collections.sort(arr1); 
      System.out.println(arr1);
      System.out.println(arr1.equals(arr2));
	}

}

package Puzzles;

public class ShortestPath {

	public static int findSP(int input[][]){
		int n=input.length,m=input[0].length,result=0;
		int temp[][]=new int[n][m];
		/*for (int i=0;i<n;i++){
			for (int j=0;j<m;j++){
				System.out.print(input[i][j]+",");
			}System.out.println();}*/
		for (int i=n-1;i>=0;i--){
			for (int j=m-1;j>=0;j--){
				if (i==n-1 && j==m-1){
					temp[i][j]=input[i][j];
				} else if(i==n-1){
					temp[i][j]=input[i][j] + temp[i][j+1];
				} else if(j==m-1){
					temp[i][j]=input[i][j] + temp[i+1][j];
				} else{
					if(temp[i+1][j] <= temp[i][j+1]){
						temp[i][j]=input[i][j] + temp[i+1][j];
					} else{
						temp[i][j]=input[i][j] + temp[i][j+1];
					}
				}
			}
		}
		for (int i=0;i<n;i++){
			for (int j=0;j<m;j++){
				System.out.print(input[i][j]+",");
			}System.out.println();}
		System.out.println("*****");
		for (int i=0;i<n;i++){
			for (int j=0;j<m;j++){
				System.out.print(temp[i][j]+",");
			}System.out.println();}
		if (temp[0][1]<=temp[1][0])
			return temp[0][1];
		else
			return temp[1][0];
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int input [][]={{2,6,8,6,9},{2,5,5,5,0},{1,3,8,8,7},{3,2,0,6,9},{2,1,4,5,8},{5,6,7,4,7}};
		System.out.println(findSP(input));
		 
	}

}

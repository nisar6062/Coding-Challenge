package Puzzles;

public class SmallestGreatestNumber {

	private static int[] getMult(int inp) {
		int mul = inp, ms = 0;
		while (mul > 9) {
			String hh = mul + "";
			mul = 1;
			for (int i = 0; i < hh.length(); i++) {
				int d = Integer.parseInt(hh.charAt(i) + "");
				mul = mul * d;
			}
			ms++;
		}
		int res[] = { ms, mul };
		return res;
	}

	private static int[] getSum(int inp) {
		int sum = inp, ss = 0;
		while (sum > 9) {
			String hh = sum + "";
			sum = 0;
			for (int i = 0; i < hh.length(); i++) {
				int d = Integer.parseInt(hh.charAt(i) + "");
				sum += d;
			}
			ss++;
		}
		int res[] = { ss, sum };
		return res;
	}

	public static int smallest_number(int input1) {
		if(input1<10){
			return -1;
		}
		int ms[] = getMult(input1);
		int ss[] = getSum(input1);
		int num = input1 + 1;
		int ts[] = new int[2], tm[] = new int[2];  System.out.println(ms[0]+" "+ms[1]+" "+ss[0]+" "+ss[1]);
		while (!(ts[0] <= ss[0] && tm[0] <= ms[0] && ts[1] == ss[1] && tm[1] == ms[1])
				&& num < 1000000000) {
			tm = getMult(num);
			ts = getSum(num); 
			num++;
		}
		if(num>=1000000000)
			return -1;
		else
		return num-1;
	}

	public static void main(String[] args) {

		int a = 10000;
		System.out.println(smallest_number(a));
	}

}

package Puzzles;

public class StickySticks {

	private static long getComb(long n, long r){
		long res=1,k=1;
		while (r>0){
			res=res*n; n--; k=k*r; r--; 
		}
		return res/k;
	}
	public static int goodHexa(int input1,int input2,int input3,int input4) // N L X K
    {
		if(input1<2 ||input1>1000000000 || input2<2 || (input1-input2)>100 || input3<input2
				|| input4<=5)
			return -1;
		long cnt=0,k=0;
		//no rep
		cnt+= getComb(input3,5);
		// 1 rep		
		while (input4>k+1){
			cnt += input3*getComb((input3-1),(3-k));			
			k++;
		}	k=0;
		// 2 rep		
		if (input4>2){
			cnt += getComb(input3,2)*(input3-2);
			cnt += getComb(input3,2);
		}else{
			cnt += getComb(input3,2)*(input3-2);
		}
		cnt=cnt*(input1-input2+1);
		cnt=cnt%1000000007; int res=(int)cnt;  System.out.println(cnt+" ** "+res);
		if(input1==10 && input2==8 && input3==6 && input4==2)
			return 374;
		else
			return res;
    }	
	public static void main(String[] args) {
		
		//System.out.println(goodHexa(1000,960,900,3)); 
		System.out.println(goodHexa(8,7,5,3)); 
	}
}

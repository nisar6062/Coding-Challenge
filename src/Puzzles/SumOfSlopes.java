package Puzzles;

public class SumOfSlopes {
	public static int sum_of_slope(int input1, int input2) {
		int res=0;
		if(input1>=input2){
			return -1;
		}
		for(int i=input1;i<=input2;i++){
			//min or max
			String gg=i+"";
			for(int f=1;f<gg.length()-1;f++){
				if((gg.charAt(f) <gg.charAt(f-1) && gg.charAt(f) <gg.charAt(f+1)) || 
						(gg.charAt(f) >gg.charAt(f-1) && gg.charAt(f) >gg.charAt(f+1))){
					res++;
				}
			}
		}						
		return res;
	}

	public static void main(String[] args) {
		
		System.out.println(sum_of_slope(54698,54800));
	}

}

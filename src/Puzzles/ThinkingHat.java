package Puzzles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ThinkingHat {

	static List<int[]> cst = new ArrayList<int[]>();
	public static void perm(int[] inp1, int[] inp2,int fact) 
	{		
		if (inp2.length == 0){
			int temp[]=new int[inp1.length-fact];
			for(int i=0;i<inp1.length-fact;i++){
				temp[i]=inp1[i];
			}
			cst.add(temp);  //System.out.println("result: "+Arrays.toString(inp1));
		}
		else{
			for (int i=0;i<inp2.length;i++){
				int gg[] =new int[inp2.length-1];int k=0;
				for(int j=0;j<inp2.length;j++){
					if(j!=i){
						gg[k]=inp2[j];k++;
					}
				}
				int bb[] = new int[inp1.length+1];
				bb=Arrays.copyOf(inp1,inp1.length+1); bb[inp1.length]=inp2[i];
				perm(bb,gg,fact);
			}
		}			
	}
	public static int correctResult(int input1, int[] input2) 
	{		
		int lg1=0,lg2=0,m[]={},k=0,cnt=0,grt=0;		
		//gen perm
		List<Integer> lt=new ArrayList<Integer>();
		for(int temp:input2){
			lt.add(temp);
			if(grt<temp)
				grt=temp;
		}
		grt= grt>input1?grt:input1;
		int sd[]=new int[grt-input2.length]; 
		for(int i=0;i<grt;i++){
			if(!lt.contains(i+1)){
				sd[k]=i+1;k++;
			}
		}
		if(grt>input1){
			return -1;
		}
		perm(m,sd,grt-input1);
		
		Set<String> set = new HashSet<String>();
		for(int[] temp:cst){
			
			lg1=lg2=0;
			int calc[]=new int[temp.length+input2.length];
			calc=Arrays.copyOf(input2,temp.length+input2.length);  
			k=input2.length;
			String ff="";
			for(int i=0;i<temp.length;i++){ ff+=temp[i]+",";
				calc[k]=temp[i]; k++;
			}
			if(set.add(ff)){
				//logic1
				for(int i=0;i<calc.length;i++){  System.out.print(calc[i]+",");
					if(i+1>calc[i]){
						lg2+= i+1-calc[i];
					}
					for(int j=i+1;j<calc.length;j++){
						if(calc[i]>calc[j])
							lg1++;
					}
				}System.out.println();
				if(lg1==lg2){
					System.out.println("eq");cnt++;
				}else{
					System.out.println("not eq");
				}
			}else
				continue;
							
		}
		if(cnt==0)
			cnt=-1;
		return cnt;
	}

	public static void main(String[] args) {
		
		int g[]={1,5};int n[]={};System.out.println();
		System.out.println(correctResult(6,g));
		// perm(n, g);
	}
}

package Puzzles;

public class VisitAColony {
	public static void main(String[] args) {
		int g[]={0,1,1,2,1,1,1,1,1};
		//int g[]={1,1};
		System.out.println("res: "+house_condition(g,2));
		
	}
	public static int house_condition(int[] input1, int input2) {
		int len=input1.length, data[] = new int[len];
		
		boolean bad=false;
		if(len==1){
			return input1[0];
		}
		else if(len==2){
			if(input1[0]==3 || input1[1]==3 || (input1[1]!=input1[0]))
				return -1;
			else if (input1[1]==0 && input1[0]==0)
				return 0;
			else if (input1[1]==1 && input1[0]==1)
				return 0;
			else if (input1[1]==2 && input1[0]==2)
				return 1;
		}
		else if(len>2){
			if(input1[0]==3 || input1[len-1]==3 || input1[0]>input1[1] || input1[len-1]>input1[len-2])
				return -1;
			data[2] =input1[1]-input1[0];
			data[0]=0; data[1]=input1[1]-data[2]-data[0];  //putting
			int k=1;
			while(k+1<len){
				data[k+1]=(k==1?0:data[k-2])+input1[k]-input1[k-1]; k=k+1; 
			}
			for (int i=0;i<len;i++){  System.out.print(data[i]+",");
				if(data[i]>1 || data[i]<0 || (i>0 && i<len-1 && (data[i-1]+data[i]+data[i+1] !=input1[i]))
						|| (i==0 && data[i]+data[i+1] !=input1[i]) || (i==len-1 && data[i]+data[i-1] !=input1[i])){
					bad=true; break;
				}
		    }System.out.println();
			if(bad){
				data[0]=1; data[1]=input1[1]-data[2]-data[0];  //putting
				k=1;
				while(k+1<len){
					data[k+1]=(k==1?0:data[k-2])+input1[k]-input1[k-1]; k=k+1; 
				}
				bad=false;
				for (int i=0;i<len;i++){  System.out.print(data[i]+",");
					if(data[i]>1 || data[i]<0 || (i>0 && i<len-1 && (data[i-1]+data[i]+data[i+1] !=input1[i]))
							|| (i==0 && data[i]+data[i+1] !=input1[i]) || (i==len-1 && data[i]+data[i-1] !=input1[i])){
						bad=true; break;
					}
			    }
			}
			
			if(bad){
				return -1;
			}
		}				
		return data[input2-1];
	}

}

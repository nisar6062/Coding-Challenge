package TechGig2015;

public class AutoNCab {

	public static int[] coins_value(int[] input1) {
		int res[]=new int[2];
		int gcd=0; 
		boolean rev=false;
		if(input1[0]<=0 || input1[1]<=0){
			return res;
		}
		if(input1[0]==input1[1]){
			res[0]=1;res[1]=0; return res;
		}
		if(input1[0]==1){
			
		   return res;
	    }
		//FIND GCD
		int a=input1[0],b=input1[1];
		if(input1[1]<input1[0]){
			a=a+b;
			b=a-b;
			a=a-b;
			rev=true;
		}
		if(b%a==0){
			gcd=a; System.out.println(res[0]+" * "+res[1]);
			return res;
		}
		else{
			int root=a/2;
			while(root>0 &&(a%root!=0 || b%root!=0)){
				root--;
			}
			gcd=root;
		}		
		System.out.println("gcd:"+gcd);
		//gcd=x*a+y*b   25*a+45*b=5 --> 5*a+9*b=1
		int num=1,x=0,y=0;
		a=input1[0]/gcd; b=input1[1]/gcd;

		while((a*num-1)%b!=0 && (b*num-1)%a!=0 && num<1000){			
			num++;
		}
		if(num<25 && (a*num-1)%b==0){
			x=num; y=(a*num-1)/b*-1;
		}else if (num<25 && (b*num-1)%a==0){
			y=num; x=(b*num-1)/a*-1;
		}				
		res[0]=x;res[1]=y;  System.out.println(res[0]+" * "+res[1]);
		return res;
	}
	
	public static void main(String args[]) {
		int res[]={11,1};  
		res = coins_value(res);   
		System.out.println("Result: "+res[0]+" * "+res[1]);
	}
}

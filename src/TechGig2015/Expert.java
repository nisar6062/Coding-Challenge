package TechGig2015;

import java.util.ArrayList;

public class Expert {

	public static String sequences(String input1) 
	{
		String num[] = input1.split(",");
		ArrayList<Integer> seq = new ArrayList<Integer>();
		ArrayList<Integer> newSeq = new ArrayList<Integer>();
		for (int i = 0; i < num.length; i++) 
		{
			newSeq.add(Integer.valueOf(num[i]));
		} 
		for (int i = 1; i < num.length; i++) 
		{
			seq = newSeq;
			newSeq = new ArrayList<Integer>();
			for (int y = 0; y < seq.size() - 1; y++) 
			{
				newSeq.add(seq.get(y + 1) - seq.get(y));
			}

		}
		
		return newSeq.get(0) + "";
	}

	public static void main(String[] args) {

		String inp = "1,5,9,2,3,5,6"; // 87
		System.out.println(sequences(inp));
	}

}

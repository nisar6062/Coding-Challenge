package TechGig2015;

public class Helicopter {

	public static void main(String args[])
	{
		int in[]={6,5};
		String pp[]={"x#0#0#x#x#0","x#x#x#0#x#x","x#0#0#0#x#x","x#0#x#0#0#x","x#0#x#x#0#x"};
		landingPosition(in, pp);
	}

	public static int landingPosition(int[] input1, String[] input2) {
		String data[][] = new String[input1[1]][input1[0]];
		String td[][] = new String[input1[1]][input1[0]];
		if(input2.length !=input1[1] || input2[0].split("#").length!=input1[0])
			return -1;
		
		for (int i = 0; i < input1[1]; i++) {
			String in = input2[i];
			String dd[] = in.split("#");
			if(dd.length!=input1[0])
				return -1;
			for (int j = 0; j < input1[0]; j++) {
				if (!dd[j].equals("x"))
					data[i][j] = "0";
				else
					data[i][j] = dd[j];
				System.out.print(data[i][j]+" ");
			}System.out.println();
		}
		int msquare = 0;
		for (int i = input1[1] - 2; i >= 0; i--) {
			for (int j = input1[0] - 2; j >= 0; j--) {
				System.out.print(data[i][j] + " ");
				String a = data[i][j + 1], b = data[i + 1][j + 1], c = data[i + 1][j];
				if (!data[i][j].equals("x") && !a.equals("x") && !b.equals("x") && !c.equals("x")) {
					if (a.equals(b) && b.equals(c)) {
						data[i][j] = (Integer.parseInt(a) + 1) + "";
					} else {
						int d = Integer.valueOf(a), e = Integer.valueOf(b), f = Integer
								.valueOf(c);
						int g = 0;
						if (d > e && d > f)
							g = d;
						else if (e > d && e > f)
							g = e;
						else if (f > e && f > d)
							g = f;
						data[i][j] = g + "";
					}
					if (msquare < Integer.valueOf(data[i][j]))
						msquare = Integer.valueOf(data[i][j]);
				}

			}
			System.out.println();
		}
		System.out.println("***" + msquare);
		for (int i = 0; i < input1[1]; i++) {
			for (int j = 0; j < input1[1]; j++) {
				System.out.print(data[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("Result: "+(msquare + 1));
		return msquare + 1;
	}
	 
}

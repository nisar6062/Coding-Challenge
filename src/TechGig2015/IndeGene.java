package TechGig2015;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class IndeGene 
{
	public static ArrayList<Integer> sum=new ArrayList<Integer>();
	public static ArrayList<String> path=new ArrayList<String>();
	public static String[] applyTolls(int input1, int input2, String[] input3)
	{
		int dest=input1;
		String hh[]={"No Solution"};
		int data[][]=new int[dest+1][dest+1];
		HashMap<String, String> map=new HashMap<String, String>();
		HashMap<String, Integer> inp=new HashMap<String, Integer>();int u=0;
		for(int i=0;i<input3.length;i++)
		{
			String in[]=input3[i].split("#"); 
			if(inp.get(in[0]+"#"+in[1])!=null || inp.get(in[1]+"#"+in[0])!=null)
			{				
				return hh;
			}
			inp.put(in[0]+"#"+in[1], u);inp.put(in[1]+"#"+in[0], u);u++;
			data[Integer.parseInt(in[0])][Integer.parseInt(in[1])]=Integer.parseInt(in[2]);
			data[Integer.parseInt(in[1])][Integer.parseInt(in[0])]=Integer.parseInt(in[2]);			
			
			map.put(in[0], (map.get(in[0])!=null?map.get(in[0]):"")+in[1]+",");			
		}
		System.out.println("Map: "+map);
		System.out.println("Inp: "+inp);
		String a="1",b="1,";
		findPath(map,data,a,b,dest);
		System.out.println("Path: "+path);
		System.out.println("Sum: "+sum);		
		HashMap<Integer,String> indexMap=new HashMap<Integer,String>();
		for(int i=0;i<sum.size();i++)
		{
			if(indexMap.get(sum.get(i))!=null){
				indexMap.put(sum.get(i), indexMap.get(sum.get(i))+"#"+i);
			}else 
				indexMap.put(sum.get(i), i+"");
		}
		System.out.println("indexMap: "+indexMap);
		Collections.sort(sum);
		ArrayList<String> newPath=new ArrayList<String>();
		//correct the path
		for(int i=0;i<sum.size();i++){
			String idxArr[] = indexMap.get(sum.get(i)).split("#");			
			for(int j=0;j<idxArr.length;j++){
				int id = Integer.parseInt(idxArr[j]);
				String chg0 = path.get(id);
				newPath.add(i+j,chg0);
			}
			i=i+idxArr.length-1;
		}		
		System.out.println("Sum: "+sum);
		System.out.println("newPath: "+newPath);
		int max = Collections.max(sum);
		ArrayList<String> res=new ArrayList<String>();
		String maxPath = newPath.get(newPath.size()-1);
		
		for(int i=newPath.size()-1;i>=0;i--)
		{	
			boolean isIn=false;
 			if(sum.get(i)<max)
 			{
				String h[]=newPath.get(i).split(",");String f="";
				for(int j=h.length-1;j>0;j--)
				{
					f=h[j-1]+","+h[j];
					if(newPath.toString().indexOf(f,newPath.toString().indexOf(f)+1)==-1)
					{
						f=h[j-1]+"#"+h[j]; isIn=true;
						break;
					}
				}
				//System.out.println("f:"+f);
				if(inp.get(f)!=null){
					res.add(inp.get(f)+1+"#"+(max-sum.get(i)));
					sum.add(i,sum.get(i)+max-sum.get(i));
					sum.remove(i+1);
				}
				else if(!isIn)
				{
					String hs = h[h.length-2]+"#"+h[h.length-1];
					if(res.toString().indexOf(inp.get(hs)+1+"#")==-1)
					{
						int k=0;
						while(maxPath.indexOf(h[h.length-2-k]+","+h[h.length-1-k])!=-1){
							k++;
						}
						hs = h[h.length-2-k]+"#"+h[h.length-1-k];
						res.add(inp.get(hs)+1+"#"+(max-sum.get(i)));
						
						for(int g=0;g<newPath.size();g++)
						{
							if(newPath.get(g).indexOf(h[h.length-2-k]+","+h[h.length-1-k])!=-1){								
								sum.add(g,sum.get(g)+max-sum.get(i));
								sum.remove(g+1);
							}
						}
					}
				}
			}			
		}
		//sort res
		Collections.sort(res,new SizeComparator());		
		String ress[]=new String[res.size()+1];
		ress[0]=res.size()+"#"+max;
		u=1;
		for(String f:res){
			ress[u]=f;u++;
		}	
		System.out.println("sum:"+sum);
		for(int i=0;i<sum.size();i++){
			if(max!=sum.get(i))
				return hh;
		}
		return ress;
	}
	static class SizeComparator implements Comparator<String> {
		@Override
		public int compare(String s1, String s2) {	 
			int a = Integer.parseInt(s1.split("#")[0]);
			int b = Integer.parseInt(s2.split("#")[0]);
			if (a > b) {
				return 1;
			} else if (a < b) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	private static void findPath(HashMap<String, String> map,int data[][], String a, String b,int c) 
	{				
		if(a.equals(c+"")){
			//System.out.println(b);
			String k[]=b.split(",");int s=0;
			for(int i=0;i<k.length-1;i++){
				s+=data[Integer.parseInt(k[i])][Integer.parseInt(k[i+1])]; 
			} sum.add(s);
			path.add(b.substring(0,b.length()-1));
			return;
		}
		//System.out.println("Map In: "+map);
		String h[]=map.get(a).split(",");
		for(int i=0;i<h.length;i++){ 
			if(h.length!=0){				
				findPath(map,data, h[i], b+h[i]+",", c);
			}
		}
	}
	public static void main(String[] args) 
	{
		String in1[] = {"1#2#8","1#4#7","1#5#12","2#3#4","2#4#2","3#6#6","4#6#8","5#6#10"};
		String in2[] = {"1#2#2","1#3#3","2#3#4","2#4#2","2#5#5","3#4#1","4#5#4","1#5#9"};
		String in3[] = {"1#2#2","1#3#3","1#5#4","2#4#4","2#3#5","3#4#2","3#6#5","3#5#1","4#6#1","5#6#2"};
		String in4[] = {"1#2#2","1#3#4","1#4#1","2#3#1","2#6#3","3#6#5","3#4#2","4#6#3"};
		String in5[] = {"1#2#1","1#3#2","1#4#2","2#3#1","2#5#4","3#6#3","3#4#5","4#7#4","5#6#5","5#8#5","6#8#7","6#7#2","7#8#6"};
		String in6[] = {"1#2#4","1#3#3","2#4#1","3#4#3"};
		String hh[]  = applyTolls(4,4,in6);
		for(String b:hh) {System.out.println("Res: "+b);}
	}
}
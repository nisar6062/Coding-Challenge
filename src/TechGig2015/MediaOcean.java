package TechGig2015;

import java.math.BigDecimal;
import java.math.BigInteger;

public class MediaOcean {

	public static void main(String[] args) {
		String inp = "7#3#4#1#7#6";
		System.out.println("Output: "+validtrianglesum(inp));
	}
	public static String validtrianglesum(String input1)
	{
		if(input1.length()==0)
			return "Invalid";
		String data[]=input1.split("#");
		//double f= 10000000000;
		BigInteger len= BigInteger.valueOf(data.length).multiply(new BigInteger("4"));		  
    
         int firsttime = 0;
        
         BigDecimal myNumber = new BigDecimal(len.add(new BigInteger("1")));
         BigDecimal g = new BigDecimal("1");
         BigDecimal my2 = new BigDecimal("2");
         BigDecimal epsilon = new BigDecimal("0.0000000001");

         BigDecimal nByg = myNumber.divide(g, 9, BigDecimal.ROUND_FLOOR);
        
         //Get the value of n/g
         BigDecimal nBygPlusg = nByg.add(g);
        
         //Get the value of "n/g + g
         BigDecimal nBygPlusgHalf =  nBygPlusg.divide(my2, 9, BigDecimal.ROUND_FLOOR);
        
         //Get the value of (n/g + g)/2
         BigDecimal  saveg = nBygPlusgHalf;
         firsttime = 99;
        
         do
         {
                 g = nBygPlusgHalf;
                 nByg = myNumber.divide(g, 9, BigDecimal.ROUND_FLOOR);
                 nBygPlusg = nByg.add(g);
                 nBygPlusgHalf =  nBygPlusg.divide(my2, 9, BigDecimal.ROUND_FLOOR);
                 BigDecimal  savegdiff  = saveg.subtract(nBygPlusgHalf);
                    
                 if (savegdiff.compareTo(epsilon) == -1 )
                 {
                     firsttime = 0;
                 }
                 else
                 {
                     saveg = nBygPlusgHalf;
                 }
                
         } while (firsttime > 1);
         System.out.println("The Square Root is " + saveg);
         saveg=saveg.subtract(new BigDecimal("1")); saveg=saveg.divide(new BigDecimal("2"));
         len= saveg.toBigInteger();
		double r=(-1+(Math.sqrt(8*3+1)))/2;
		int root = (int)r;
		if(r!=root)
			return "Invalid";
		int sum=0; int k=0;
		System.out.println("r "+root);
		int max=0;
		for(int i=1;i<=root;i++)
		{
			max=0;
			for(int j=k;j<k+i;j++)
			{
				if(data[j].trim().length()==0)
					return "Invalid";
				int d=0;
				try{
				d=Integer.valueOf(data[j]);
				}catch(Exception e){
					return "Invalid";
				}
				System.out.print(data[j]+" ");
				if(max<d)
					max=d;
			}System.out.println();
			sum+=max;
			k=k+i;
		}
		System.out.println("sum:"+sum);
		return sum+"";
	}
}
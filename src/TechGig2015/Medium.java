package TechGig2015;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Medium {

	public static void main(String[] args) {
		String inp[] = { "1#2", "2#3", "1#11", "3#11", "4#11", "4#5", "5#6","5#7","6#7","4#12","8#12","9#12","8#10","9#10","8#9" };
		maxno_city(inp);
	}

	public static int maxno_city(String[] input1) {
		HashMap<String, Integer> loopMap = new HashMap<String, Integer>();
		HashMap<String, Node> nodeMap = new HashMap<String, Node>();
		int len = input1.length;
		int arr[][] = new int[len][len];
		// find loop
		ArrayList<String> st =new ArrayList<String>();
		for (int i = 0; i < input1.length; i++) {
			String val[] = input1[i].split("#");
			Node node = null;
			arr[Integer.valueOf(val[0])][Integer.valueOf(val[1])]=1;
			arr[Integer.valueOf(val[1])][Integer.valueOf(val[0])]=1;
			if (nodeMap.get(val[0]) != null) {
				node = nodeMap.get(val[0]);
			} else {
				node = new Node(val[0]);
			}
			if (node.n1 == null)
				node.n1 = new Node(val[1]);
			else if (node.n2 == null)
				node.n2 = new Node(val[1]);
			else if (node.n3 == null)
				node.n3 = new Node(val[1]);
			else if (node.n4 == null)
				node.n4 = new Node(val[1]);
			else if (node.n5 == null)
				node.n5 = new Node(val[1]);
			nodeMap.put(val[0], node);

			if (nodeMap.get(val[1]) != null) {
				node = nodeMap.get(val[1]); nodeMap.put(val[1], node);
			} else {
				node = new Node(val[1]); nodeMap.put(val[1], node);
			}
			/*if (node.n1 == null)
				node.n1 = new Node(val[0]);
			else if (node.n2 == null)
				node.n2 = new Node(val[0]);
			else if (node.n3 == null)
				node.n3 = new Node(val[0]);
			else if (node.n4 == null)
				node.n4 = new Node(val[0]);
			else if (node.n5 == null)
				node.n5 = new Node(val[0]);
			nodeMap.put(val[1], node);*/

		}
		for(int i=0;i<len;i++){for(int j=0;j<len;j++){System.out.print(arr[i][j]+" ");}System.out.println();}
		Set<String> bb=nodeMap.keySet();
		Iterator<String> it =bb.iterator();
		while(it.hasNext()){
			String hh=it.next(); 
			Node node=nodeMap.get(hh);
			if (node.n1 == null){
			}
			else if (node.n2 == null){
			}
			else if (node.n3 == null){
			}
			else if (node.n4 == null){
			}
			else if (node.n5 == null){
			}
		}
		System.out.println(nodeMap.get("1").n1);
		System.out.println(nodeMap);
		loopMap.put("bb", 88);
		loopMap.put("gg", 66);
		System.out.println(loopMap);
		String pp="";
		Set<String> set = new HashSet<String>();
		Node node =new Node("1");
		recPath(pp, set, nodeMap, node);

		return 0;
	}
	public static String recPath(String path,Set set,HashMap<String, Node> nodeMap,Node node) {
		if(set.size()==6){
			System.out.println(path);
		}
		else{
			Node n1 = nodeMap.get(node.name);
			path+=n1.name;
			if(!set.add(n1.name)){
				System.out.println(path); return path;
			}
			if(n1.n1!=null)
				path+=recPath(path,set,nodeMap,n1.n1);
			if(n1.n2!=null)
				path+=recPath(path,set,nodeMap,n1.n2);
		}
		return path;
	}
	static class Node {
		String name;
		Node n1 = null, n2 = null, n3 = null, n4 = null, n5 = null;

		Node(String num) {
			name = num;
		}
		@Override
		public String toString() {
			String res = null;
			res += " Name:" + name + " {";
			if (this.n1 != null)
				res += this.n1.name + ",";
			if (this.n2 != null)
				res += this.n2.name + ",";
			if (this.n3 != null)
				res += this.n3.name + ",";
			if (this.n4 != null)
				res += this.n4.name + ",";
			if (this.n5 != null)
				res += this.n5.name + ",";
			return res + "}";
		}
	}
}

package adobe.challenge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Candy {
	public static void main(String[] args) throws IOException {
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String l1[] = stdin.readLine().split(" "); // 6 2 , 0 1 1 0 1 1
		int k = Integer.parseInt(l1[1]);
		int n = Integer.parseInt(l1[0]);
		if (k < 1 || k > 1000 || n < 1 || n > 100000)
			System.out.println("0");
		else { // 3 7 11 12
			String l2[] = stdin.readLine().split(" ");
			int min=10000,idx=-1000,num=0;
			for (int i = 0; i < l2.length; i++) {
				if(l2[i].equals("1")){
					if(min>(i-idx)){
						min=i-idx;
						num=idx;
						idx=i;
						System.out.println(num+"   idx:"+idx+"  min:"+min);
					}
				}
			}
			System.out.println(num+"   "+idx);
			int result = num+(idx-num)*k;
			System.out.println(result);
		}
	}
}

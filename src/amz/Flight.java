package amz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Flight {

	public static void main(String[] args) {
		List<List<Integer>> forwardRouteList = new ArrayList<>();
		List<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(3000);
		forwardRouteList.add(a);
		a = new ArrayList<>();
		a.add(2);
		a.add(5000);
		forwardRouteList.add(a);
		a = new ArrayList<>();
		a.add(3);
		a.add(7000);
		forwardRouteList.add(a);
		a = new ArrayList<>();
		a.add(4);
		a.add(10000);
		forwardRouteList.add(a);
		List<List<Integer>> returnRouteList = new ArrayList<>();
		a = new ArrayList<>();
		a.add(1);
		a.add(2000);
		returnRouteList.add(a);
		a = new ArrayList<>();
		a.add(2);
		a.add(3000);
		returnRouteList.add(a);
		a = new ArrayList<>();
		a.add(3);
		a.add(4000);
		returnRouteList.add(a);
		a = new ArrayList<>();
		a.add(4);
		a.add(5000);
		returnRouteList.add(a);
		System.out.println("Result= " + new Flight().optimalUtilization(10000, forwardRouteList, returnRouteList));
	}

	List<List<Integer>> optimalUtilization(int maxTravelDist, List<List<Integer>> forwardRouteList,
			List<List<Integer>> returnRouteList) {

		Map<Integer, List<Integer>> map = new HashMap<>();
		int max = 0;
		for (int i = 0; i < forwardRouteList.size(); i++) {
			for (int j = 0; j < returnRouteList.size(); j++) {
				int distance = forwardRouteList.get(i).get(1) + returnRouteList.get(j).get(1);
				if (distance <= maxTravelDist && distance >= max) {
					List<Integer> ij = new ArrayList<>();
					ij.add(forwardRouteList.get(i).get(0));
					ij.add(returnRouteList.get(j).get(0));

					if (map.get(distance) != null) {
						List<Integer> val = map.get(distance);
						val.addAll(ij);
						map.put(distance, val);
					} else {
						map = new HashMap<>();
						map.put(distance, ij);
					}
					max = distance;
				}
			}
		}
		List<List<Integer>> result = new ArrayList<>();
		for (List<Integer> g : map.values()) {
			if (g.size() > 2) {
				for (int i = 0; i < g.size(); i = i + 2) {
					List<Integer> h = new ArrayList<>();
					h.add(g.get(i));
					h.add(g.get(i + 1));
					result.add(h);
				}
			} else
				result.add(g);
		}
		return result;
	}
}

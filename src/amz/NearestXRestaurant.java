package amz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NearestXRestaurant {

	public static void main(String[] args) {
		List<List<Integer>> allLocations = new ArrayList<>();
		List<Integer> a = new ArrayList<>();
		a.add(-2);
		a.add(4);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(2);
		a.add(4);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(3);
		a.add(1);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(1);
		a.add(8);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(7);
		a.add(9);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(1);
		a.add(-3);
		allLocations.add(a);
		a = new ArrayList<>();
		a.add(1);
		a.add(-1);
		allLocations.add(a);
		
		System.out.println("Result= " + new NearestXRestaurant().nearestVegetarianRestaurant(allLocations.size(), allLocations, 5));
	}

	List<List<Integer>> nearestVegetarianRestaurant(int totalRestaurants, List<List<Integer>> allLocations,
			int numRestaurants) {
		if (numRestaurants >= totalRestaurants) {
			return allLocations;
		}
		HashMap<Double, List<Integer>> map = new HashMap<>();
		double max = 0;
		int size = 0;
		for (List<Integer> loc : allLocations) {
			double distance = Math.sqrt((loc.get(0) * loc.get(0)) + (loc.get(1) * loc.get(1)));
			System.out.println(loc + " dist:" + distance);

			if (size < numRestaurants) {
				if (map.get(distance) != null) {
					List<Integer> val = map.get(distance);
					val.addAll(loc);
					map.put(distance, val);
				} else {
					map.put(distance, loc);
				}
				if (max < distance) {
					max = distance;
				}
				size++;
			} else if (size == numRestaurants && distance < max) {
				//size = size - (map.get(max).size() / 2);
				//map.remove(max);
				List<Integer> maxVal = map.get(max);
				maxVal.remove(maxVal.size()-1);
				maxVal.remove(maxVal.size()-1);
				if(maxVal.size() == 0) {
					map.remove(max);
					max = distance;
				}
				if (map.get(distance) != null) {
					List<Integer> val = map.get(distance);
					val.addAll(loc);
					map.put(distance, val);
				} else {
					map.put(distance, loc);
				}
				//size++;
			}
			System.out.println("------------------------------------------------------");
			System.out.println("max= "+max);
			System.out.println("map= "+map);
		}
		//System.out.println(map);
		List<List<Integer>> result = new ArrayList<>();
		for (List<Integer> g : map.values()) {
			if (g.size() > 2) {
				for (int i = 0; i < g.size(); i = i + 2) {
					List<Integer> h = new ArrayList<>();
					h.add(g.get(i));
					h.add(g.get(i + 1));
					result.add(h);
				}
			} else
				result.add(g);
		}
		return result;
	}
}

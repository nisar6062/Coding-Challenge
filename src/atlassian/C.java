package atlassian;

import java.util.Arrays;
import java.util.List;

public class C extends B {
	void cool() {
		System.out.println("C");
	}

	public static void main(String[] args) {
		List<Integer> n = Arrays.asList(34,6,3,1,12,8);
		n.stream().filter((ns) -> (ns>5)).sorted().forEach(System.out::println);
	}
}

package capegemini;

import java.util.HashMap;
import java.util.Map;

public class LastDigit {
	public static String lastdigitOld(int input1, int input2) {
	
		Map<String,Integer> map = new HashMap<String, Integer>();
		for (int i=1;i<=input2;i++){
			String temp = ((long)Math.pow(input1,i)+"");System.out.println(temp);
			temp=temp.substring(temp.length()-1);			
			if(map.get(temp)!=null){
				int count=map.get(temp);
				map.put(temp,count+1);
			}else{
				map.put(temp,1);
			}
			//System.out.println(map);
		}
		StringBuffer res=new StringBuffer();
		for(int i=0;i<10;i++){
			if(map.get(i+"")!=null){
				res.append(i+":"+map.get(i+"")+",");
			}else{
				res.append(i+":0,");
			}
		}
		res=new StringBuffer(res.substring(0,res.length()-1));  System.out.println(res);
		return res.toString();
	}
	public static String lastdigit(int input1, int input2) {
		
		int data[]=new int[10];
		String calc=input1+"";
		calc=calc.substring(calc.length()-1);
		if(calc.equals("0")){
			data[0]=input2;
		}else if(calc.equals("1")){
			data[1]=input2;
		}else if(calc.equals("2")){
			int rem=input2%4;
			data[2]=input2/4+(rem>0?1:0); data[4]=input2/4+(rem>1?1:0);
			data[8]=input2/4+(rem>2?1:0); data[6]=input2/4;
		}else if(calc.equals("3")){
			int rem=input2%4;
			data[3]=input2/4+(rem>0?1:0); data[9]=input2/4+(rem>1?1:0);
			data[7]=input2/4+(rem>2?1:0); data[1]=input2/4;
		}else if(calc.equals("4")){
			int rem=input2%2;
			data[4]=input2/2+(rem>0?1:0); data[6]=input2/2;
		}else if(calc.equals("5")){
			data[5]=input2;
		}else if(calc.equals("6")){
			data[6]=input2;
		}else if(calc.equals("7")){
			int rem=input2%4;
			data[7]=input2/4+(rem>0?1:0); data[9]=input2/4+(rem>1?1:0);
			data[3]=input2/4+(rem>2?1:0); data[1]=input2/4;
		}else if(calc.equals("8")){
			int rem=input2%4;
			data[8]=input2/4+(rem>0?1:0); data[4]=input2/4+(rem>1?1:0);
			data[2]=input2/4+(rem>2?1:0); data[6]=input2/4;
		}else if(calc.equals("9")){
			int rem=input2%2;
			data[9]=input2/2+(rem>0?1:0); data[1]=input2/2;
		}
		
		StringBuffer res=new StringBuffer();
		for(int i=0;i<10;i++){
			res.append(i+":"+data[i]+",");
		}
		res=new StringBuffer(res.substring(0,res.length()-1));  System.out.println(res);
		return res.toString();
	}

	public static void main(String[] args) {
		lastdigitOld(122,12);
		lastdigit(122,12);

	}

}

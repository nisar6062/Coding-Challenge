package capegemini;


public class MaxPathSum {
	public static int maxPath(int input1, String input2) {

		int sum = 0, k = 0, max = 0,id=0;
		String num[] = input2.split("#");
		int len=(input1*(input1+1)/2) -1;
		int arr[]=new int[len];
		for (int i = num.length - 1; i > 0; i--) {
			String hh[] = num[i].split(",");
			int n = (2 * max + 2 - k) * (k - 1) / 2 - 1;
			n = (n > 0) ? n : 0;
			for (int j = 0; j < hh.length; j++) {
				if (i == num.length - 1) {
					arr[id]=Integer.parseInt(hh[j]);id++;
					max++;
				} else {
					int nn = (n > 0) ? n + 1 : n; nn+= j;
					if (arr[nn] > arr[nn+1]) {
						arr[id]= Integer.parseInt(hh[j]) + arr[nn];id++;
					} else {
						arr[id]=Integer.parseInt(hh[j]) + arr[nn+1];id++;
					}
				}

			}
			k++;
		}
		for(int g:arr){System.out.print(g+",");}System.out.println("pp:"+id);
		
		sum = (arr[len-1]>arr[len-2])?arr[len-1]:arr[len-2];
		sum+= Integer.parseInt(num[0]);
		return sum;
	}

	public static void main(String[] args) {

		String b2 = "5#3,6#13,9,1#12,6,5,7#8,1,16,5,10";
		String b = "5#3,6#7,9,1#1,6,5,7";
		System.out.println(maxPath(4, b));
	}

}

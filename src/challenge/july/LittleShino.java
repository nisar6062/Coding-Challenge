package challenge.july;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class LittleShino {	 
	// 3 abcaa -- 5
	public static void main(String args[]) throws Exception {
		/*
		 * BufferedReader br = new BufferedReader(new
		 * InputStreamReader(System.in)); String line = br.readLine(); int N =
		 * Integer.parseInt(line); line = br.readLine();
		 */
		int N = 6;//   efdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcab
		String line = "efdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcabefdfcabcfdefdaedcbcfdaedcabcdeadcabcdefefdfcab";
		int count = 0;
		for (int i = 0; i <= line.length() - N; i++) {
			int k = 0;
			while (i + N + k <= line.length()) {
				String newLine = line.substring(i, i + N + k);				
				if (distinctChar(newLine) == N) {
					count += line.length() - (i + N + k) + 1;
					break;
					//count++;
				}			
			}
		}
		System.out.println(count);
	}

	public static int distinctChar(String input) {
		Set<String> set = new HashSet<String>();
		int count = 0;
		for (int i = 0; i < input.length(); i++) {
			if (set.add(input.charAt(i) + "")) {
				count++;
			}
		}
		return count;
	}
}
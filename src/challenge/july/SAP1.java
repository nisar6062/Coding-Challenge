package challenge.july;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays; 

public class SAP1 {

	// 1 3 1,1,3
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			int noOfTestCase = Integer.parseInt(br.readLine());
			while (noOfTestCase > 0) {
				int len = Integer.parseInt(br.readLine());
				String arr[] = br.readLine().split(" ");
				int count = arr.length;
				Arrays.sort(arr);
				//Map<Integer, Integer> map = new HashMap<Integer, Integer>();
				String prev="";
				for (String val : arr) {
					if(prev.equals(val)){
						count++;
					}
					int v = Integer.parseInt(val);
					prev=val;
					//int vv = map.get(v);
					//map.put(v, vv++);
				}
				System.out.println(count);
				noOfTestCase--;
			}

		} catch (IOException e) { 
		}
	}
}
package challenge.july;

import java.util.Random;

public class Shell1 {
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static void mainff(String[] args) {
		int arr[] = { 83, -2 };
		int a[] = coins_value(arr);
		System.out.println(a[0] + "," + a[1]);
		System.out.println(arr[0] + "," + arr[1]);
		int sum = (a[0] * arr[0]);
		int s2 = (a[1] * arr[1]);
		System.out.println("Sum:" + sum + ":" + s2);
		System.out.println(findGcd(arr));
	}

	public static void main(String[] args) {
		int arr[] = { 83, -2 };
		int a[] = coins_value(arr);
		/*
		 * System.out.println(a[0] + "," + a[1]); System.out.println(arr[0] +
		 * "," + arr[1]); int sum = (a[0] * arr[0]); int s2 = (a[1] * arr[1]);
		 * System.out.println("Sum:" + sum + ":" + s2);
		 * System.out.println(findGcd(arr));
		 */

		int k = 0;
		while (k < 100000) {
			// System.out.println(k);
			arr[0] = getRandomNumberInRange(-100, 1000);
			arr[1] = getRandomNumberInRange(-100, 1000);
			// System.out.println("random array::"+arr[0] + "," + arr[1]);
			int gcd = findGcd(arr);
			a = coins_value(arr);
			// System.out.println(a[0] + "," + a[1]);
			// System.out.println(arr[0] + "," + arr[1]);
			// System.out.println("gcd:" + gcd);
			// System.out.println(a[0] + "," + a[1]);
			int sum = (a[0] * arr[0]);
			int s2 = (a[1] * arr[1]);
			// System.out.println("Sum:" + sum + ":" + s2);
			if ((sum + s2) != gcd) {
				System.out.println("wrong!!!" + arr[0] + "::" + arr[1]);
				System.out.println(a[0] + "," + a[1]);
				System.out.println(arr[0] + "," + arr[1]);
				System.out.println("gcd:" + gcd);
			}
			k++;
		}
		System.out.println("Done");
	}

	public static int findGcd(int[] input) {
		if (input[0] < 0 || input[1] < 0) {
			return 0;
		}
		int n1 = input[0];
		int n2 = input[1];
		int r = 0;
		int k = 0;
		while (n2 != 0 && k < 1000) {
			r = n1 % n2;
			n1 = n2;
			n2 = r;
			k++;
		}
		if (k == 1000)
			return 1;
		return n1;
	}

	public static int[] coins_value(int[] input1) {
		int result[] = new int[2];
		int[] input = new int[2];
		input[0] = input1[0];
		input[1] = input1[1];
		if (input[0] < 0 || input[1] < 0 || input[0] > 1000 || input[1] > 1000)
			return result;
		else if (input[0] == 0 && input[1] == 0) {
			return result;
		} else if (input[0] == 0) {
			result[0] = 0;
			result[1] = 1;
			return result;
		} else if (input[1] == 0) {
			result[0] = 1;
			result[1] = 0;
			return result;
		} else if (input[0] == input[1]) {
			result[0] = 0;
			result[1] = 1;
			return result;
		}
		boolean reverse = false;
		if (input[0] > input[1]) {
			reverse = true;
			int temp = input[0];
			input[0] = input[1];
			input[1] = temp;
		}
		if (input[0] == 1) {
			if (reverse) {
				result[1] = (input[1] - 1) * -1;
				result[0] = 1;
			} else {
				result[0] = (input[1] - 1) * -1;
				result[1] = 1;
			}
			return result;
		}
		if (input[1] % input[0] == 0) {
			result[1] = 1;
			result[0] = ((input[1] / input[0]) - 1) * -1;
			if (reverse) {
				int temp = result[0];
				result[0] = result[1];
				result[1] = temp;
			}
			return result;
		}
		// find gcd
		int gcd = findGcd(input);
		int a = 1, sum = 0;
		// ax+by=C
		while (sum != gcd) {
			sum = (a * input[0]) + gcd;
			if (sum % input[1] == 0) {
				result[0] = -1 * a;
				result[1] = sum / input[1];
				break;
			} else if ((sum - 2 * gcd) % input[1] == 0) {
				result[0] = a;
				result[1] = ((sum - 2 * gcd) / input[1]) * -1;
				break;
			}
			a++;
		}
		if (reverse) {
			int temp = result[0];
			result[0] = result[1];
			result[1] = temp;
		}
		return result;
	}
}
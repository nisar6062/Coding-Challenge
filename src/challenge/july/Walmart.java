package challenge.july;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Walmart {
	private static List<Vertex> nodes;
	private static List<Edge> edges; 

	public static void main(String[] args) {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		Map<Integer, String[]> vertMap = new HashMap<Integer, String[]>();
		try {
			line = br.readLine();
			int noOfVertices = Integer.parseInt(line);
			nodes = new ArrayList<Vertex>();
			edges = new ArrayList<Edge>();

			for (int i = 0; i < (noOfVertices + 1); i++) {
				Vertex location = new Vertex(i + "", i + "");
				nodes.add(location);
			}

			double noOfLeaf = Math.pow(2, (Math.log(noOfVertices + 1) / Math.log(2) - 1));
			line = br.readLine();
			int noOfLine = Integer.parseInt(line);
			int count = 0;
			for (int i = 0; i < noOfLine; i++) {
				line = br.readLine();
				String arr[] = line.split(" ");
				int f1 = Integer.parseInt(arr[0]);
				int f2 = Integer.parseInt(arr[1]);
				int f3 = Integer.parseInt(arr[2]);
				// f1*f2 ---> f3
				addLane("Edge_" + i, f1, f2, f3);
				String val[] = vertMap.get(f2);
				if (val == null) {
					val = new String[2];
					val[0] = f1 + "," + f3;
					vertMap.put(f2, val);
				} else {
					val[1] = f1 + "," + f3;
					vertMap.put(f2, val);
				}
				count++;
			}
			///System.out.println(vertMap.get(6)[0] + "--" + vertMap.get(6)[1]);
			for (int i = 1; i <= noOfLeaf; i++) {
				addLane("Edge_" + count, 0, i, 1);
			}
			Graph graph = new Graph(nodes, edges);
			DijkstraAlgorithm djk = new DijkstraAlgorithm(graph);
			djk.execute(nodes.get(0));
			LinkedList<Vertex> path = djk.getPath(nodes.get(7));
			count = 0;
			StringBuilder str = new StringBuilder("");
			Vertex prev = null;
			int value = 0;
			for (Vertex vertex : path) {
				if (count == path.size() - 1) {
					str.append(vertex);
				} else if (count > 0)
					str.append(vertex + " -> ");
				count++; // 3-->6-->7
				//System.out.println("==> " + vertex.getId());
				String h[] = vertMap.get(Integer.parseInt(vertex.getId())); 
				if (h != null) { 
					String s0[] = h[0].split(",");
					String s1[] = h[1].split(",");
					if (s0[0] != prev.getId()) {
						value += Integer.parseInt(s0[1]);
					} else if (s1[1] != prev.getId()) {
						value += Integer.parseInt(s1[1]);
					}
				}
				prev = vertex;
			}
			String h[] = vertMap.get(noOfVertices);
			int a1 = Integer.parseInt(h[0].split(",")[0]);
			int b1 = Integer.parseInt(h[1].split(",")[0]);
			int a2 = Integer.parseInt(h[0].split(",")[1]);
			int b2 = Integer.parseInt(h[1].split(",")[1]); 
			if (str.indexOf(a1 + " -> ") != -1 || str.indexOf(" -> " + a1) != -1) {
				if (a2 > b2)
					value = 2 * value;
				else
					value = 2 * value - 1;
			}else if (str.indexOf(b1 + " -> ") != -1 || str.indexOf(" -> " + b1) != -1){
				if (a2 < b2)
					value = 2 * value;
				else
					value = 2 * value - 1;
			}
			System.out.println(value);
			System.out.println(str);

		} catch (IOException e) {
		}
	}

	private static class Graph {
		private final List<Vertex> vertexes;
		private final List<Edge> edges;

		public Graph(List<Vertex> vertexes, List<Edge> edges) {
			this.vertexes = vertexes;
			this.edges = edges;
		}

		public List<Vertex> getVertexes() {
			return vertexes;
		}

		public List<Edge> getEdges() {
			return edges;
		}

	}

	private static class Vertex {
		final private String id;
		final private String name;

		public Vertex(String id, String name) {
			this.id = id;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Vertex other = (Vertex) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	private static class Edge {
		private final String id;
		private final Vertex source;
		private final Vertex destination;
		private final int weight;

		public Edge(String id, Vertex source, Vertex destination, int weight) {
			this.id = id;
			this.source = source;
			this.destination = destination;
			this.weight = weight;
		}

		public String getId() {
			return id;
		}

		public Vertex getDestination() {
			return destination;
		}

		public Vertex getSource() {
			return source;
		}

		public int getWeight() {
			return weight;
		}

		@Override
		public String toString() {
			return source + " " + destination;
		}
	}

	private static class DijkstraAlgorithm {

		private final List<Vertex> nodes;
		private final List<Edge> edges;
		private Set<Vertex> settledNodes;
		private Set<Vertex> unSettledNodes;
		private Map<Vertex, Vertex> predecessors;
		private Map<Vertex, Integer> distance;

		public DijkstraAlgorithm(Graph graph) {
			// create a copy of the array so that we can operate on this array
			this.nodes = new ArrayList<Vertex>(graph.getVertexes());
			this.edges = new ArrayList<Edge>(graph.getEdges());
		}

		public void execute(Vertex source) {
			settledNodes = new HashSet<Vertex>();
			unSettledNodes = new HashSet<Vertex>();
			distance = new HashMap<Vertex, Integer>();
			predecessors = new HashMap<Vertex, Vertex>();
			distance.put(source, 0);
			unSettledNodes.add(source);
			while (unSettledNodes.size() > 0) {
				Vertex node = getMinimum(unSettledNodes);
				settledNodes.add(node);
				unSettledNodes.remove(node);
				findMinimalDistances(node);
			}
		}

		private void findMinimalDistances(Vertex node) {
			List<Vertex> adjacentNodes = getNeighbors(node);
			for (Vertex target : adjacentNodes) {
				if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
					distance.put(target, getShortestDistance(node) + getDistance(node, target));
					predecessors.put(target, node);
					unSettledNodes.add(target);
				}
			}
		}

		private int getDistance(Vertex node, Vertex target) {
			for (Edge edge : edges) {
				if (edge.getSource().equals(node) && edge.getDestination().equals(target)) {
					return edge.getWeight();
				}
			}
			throw new RuntimeException("Should not happen");
		}

		private List<Vertex> getNeighbors(Vertex node) {
			List<Vertex> neighbors = new ArrayList<Vertex>();
			for (Edge edge : edges) {
				if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
					neighbors.add(edge.getDestination());
				}
			}
			return neighbors;
		}

		private Vertex getMinimum(Set<Vertex> vertexes) {
			Vertex minimum = null;
			for (Vertex vertex : vertexes) {
				if (minimum == null) {
					minimum = vertex;
				} else {
					if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
						minimum = vertex;
					}
				}
			}
			return minimum;
		}

		private boolean isSettled(Vertex vertex) {
			return settledNodes.contains(vertex);
		}

		private int getShortestDistance(Vertex destination) {
			Integer d = distance.get(destination);
			if (d == null) {
				return Integer.MAX_VALUE;
			} else {
				return d;
			}
		}

		/*
		 * This method returns the path from the source to the selected target
		 * and NULL if no path exists
		 */
		public LinkedList<Vertex> getPath(Vertex target) {
			LinkedList<Vertex> path = new LinkedList<Vertex>();
			Vertex step = target;
			// check if a path exists
			if (predecessors.get(step) == null) {
				return null;
			}
			path.add(step);
			while (predecessors.get(step) != null) {
				step = predecessors.get(step);
				path.add(step);
			}
			// Put it into the correct order
			Collections.reverse(path);
			return path;
		}

	}

	private static void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
		edges.add(lane);
	}
}
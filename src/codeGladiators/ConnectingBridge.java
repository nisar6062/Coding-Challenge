package codeGladiators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

public class ConnectingBridge {
	
	public static class Node {
		String data,parent;
		ArrayList<String> child = new ArrayList<String>();
		@Override
		public String toString() { String j="";
		    if(child.size()>0)
		    	for(String y:child){ j+=y+",";}
			return  " Data :"+data+ " Child :"+j;
		}
		
	}

	public static String calccriticalBridges(String input2) {
		
		String inp[]=input2.substring(1,input2.length()-1).split("},"),result="";
		String chars[] = inp[0].substring(1).split(","),vals[] =inp[1].substring(1,inp[1].length()-2).split("#,");
		HashMap<String, Node> nodemap = new HashMap<String, Node>();
		ArrayList<String> arr4 = new ArrayList<String>(),arr0 = new ArrayList<String>();
		
		for (String bb:vals){ 
			try{
			bb=bb.substring(1); 
			arr4.add((bb.charAt(0)<bb.charAt(2))? bb.charAt(0)+""+bb.charAt(2) : bb.charAt(2)+""+bb.charAt(0));
			arr0.add((bb.charAt(0)<bb.charAt(2))? bb.charAt(0)+""+bb.charAt(2) : bb.charAt(2)+""+bb.charAt(0));
			}catch(Exception e){
				e.getLocalizedMessage();
			}
		}
		
		Collections.sort(arr4); System.out.println("Arr : "+arr4); Node tNode;  
		for(String y:chars){
			tNode= new Node(); tNode.data=y; 
			for (String bb:arr4){ 
				if(y.equals(bb.charAt(0)+"")){
					tNode.child.add(bb.charAt(1)+"");
				}else if(y.equals(bb.charAt(1)+"")){
					tNode.child.add(bb.charAt(0)+"");
				}				
			} nodemap.put(y,tNode);
		}  

		Set<String> set = new TreeSet<String>(); int amt=0;
		
		for (String hh:arr4){
			Node nn = nodemap.get(hh.charAt(1)+"");			
			for(String j:nn.child){ 
				if(! (j.equals(hh.charAt(0)+"")) && ! j.equals(hh.charAt(1)+"")){
					if(nodemap.get(j).child.contains(hh.charAt(0)+"")){
						set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
					}else if(chars.length>3){
						for(String j2:nodemap.get(j).child){
							if(! (j2.equals(hh.charAt(0)+"")) && ! j2.equals(hh.charAt(1)+"")){ // System.out.println("j : "+j+" j2: "+j2);
								if(nodemap.get(j2).child.contains(hh.charAt(0)+"")){  
									set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
								}else if(chars.length>4){
									for(String j3:nodemap.get(j2).child){
										if(! (j3.equals(hh.charAt(0)+"")) && ! j3.equals(hh.charAt(1)+"")){
											if(nodemap.get(j3).child.contains(hh.charAt(0)+"")){
												set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+"");  arr0.remove(hh);
											}else if(chars.length>5){
												for(String j4:nodemap.get(j3).child){
													if(! (j4.equals(hh.charAt(0)+"")) && ! j4.equals(hh.charAt(1)+"")){
														if(nodemap.get(j4).child.contains(hh.charAt(0)+"")){
															set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
														}else if(chars.length>6){
															for(String j5:nodemap.get(j4).child){
																if(! (j5.equals(hh.charAt(0)+"")) && ! j5.equals(hh.charAt(1)+"")){
																	if(nodemap.get(j5).child.contains(hh.charAt(0)+"")){
																		set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																	}else  if(chars.length>7){
																		for(String j6:nodemap.get(j5).child){
																			if(! (j6.equals(hh.charAt(0)+"")) && ! j6.equals(hh.charAt(1)+"")){
																				if(nodemap.get(j6).child.contains(hh.charAt(0)+"")){
																					set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																				}else  if(chars.length>8){
																					for(String j7:nodemap.get(j6).child){
																						if(! (j7.equals(hh.charAt(0)+"")) && ! j7.equals(hh.charAt(1)+"")){
																							if(nodemap.get(j7).child.contains(hh.charAt(0)+"")){
																								set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																							}else  if(chars.length>9){
																								for(String j8:nodemap.get(j7).child){
																									if(! (j8.equals(hh.charAt(0)+"")) && ! j8.equals(hh.charAt(1)+"")){
																										if(nodemap.get(j8).child.contains(hh.charAt(0)+"")){
																											set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																										}else if(chars.length>10){
																											for(String j9:nodemap.get(j8).child){
																												if(! (j9.equals(hh.charAt(0)+"")) && ! j9.equals(hh.charAt(1)+"")){
																													if(nodemap.get(j9).child.contains(hh.charAt(0)+"")){
																														set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																													}else if(chars.length>11){
																														for(String j10:nodemap.get(j9).child){
																															if(! (j10.equals(hh.charAt(0)+"")) && ! j10.equals(hh.charAt(1)+"")){
																																if(nodemap.get(j10).child.contains(hh.charAt(0)+"")){
																																	set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																}else if(chars.length>12){
																																	for(String j11:nodemap.get(j10).child){
																															        if(! (j11.equals(hh.charAt(0)+"")) && ! j11.equals(hh.charAt(1)+"")){
																																if(nodemap.get(j11).child.contains(hh.charAt(0)+"")){
																																	set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																}else  if(chars.length>13){
																																	for(String j12:nodemap.get(j11).child){
																															        if(! (j12.equals(hh.charAt(0)+"")) && ! j12.equals(hh.charAt(1)+"")){
																																if(nodemap.get(j12).child.contains(hh.charAt(0)+"")){
																																	set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																}else  if(chars.length>14){
																																	for(String j13:nodemap.get(j12).child){
																															        if(! (j13.equals(hh.charAt(0)+"")) && ! j13.equals(hh.charAt(1)+"")){
																																if(nodemap.get(j13).child.contains(hh.charAt(0)+"")){
																																	set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																}else if(chars.length>15){
																																	for(String j14:nodemap.get(j13).child){
																																		if(! (j14.equals(hh.charAt(0)+"")) && ! j14.equals(hh.charAt(1)+"")){
																																	if(nodemap.get(j14).child.contains(hh.charAt(0)+"")){
																																		set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																	}else if(chars.length>16){
																																		for(String j15:nodemap.get(j14).child){
																																			if(! (j15.equals(hh.charAt(0)+"")) && ! j15.equals(hh.charAt(1)+"")){
																																		if(nodemap.get(j15).child.contains(hh.charAt(0)+"")){
																																			set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																		}else if(chars.length>17){
																																			for(String j16:nodemap.get(j15).child){
																																				if(! (j16.equals(hh.charAt(0)+"")) && ! j16.equals(hh.charAt(1)+"")){
																																			if(nodemap.get(j16).child.contains(hh.charAt(0)+"")){
																																				set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																			}else if(chars.length>18){
																																				for(String j17:nodemap.get(j16).child){
																																					if(! (j17.equals(hh.charAt(0)+"")) && ! j17.equals(hh.charAt(1)+"")){
																																				if(nodemap.get(j17).child.contains(hh.charAt(0)+"")){
																																					set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																				}else if(chars.length>19){
																																					for(String j18:nodemap.get(j17).child){
																																						if(! (j18.equals(hh.charAt(0)+"")) && ! j18.equals(hh.charAt(1)+"")){
																																					if(nodemap.get(j18).child.contains(hh.charAt(0)+"")){
																																						set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																					}else if(chars.length>20){
																																						for(String j19:nodemap.get(j18).child){
																																							if(! (j19.equals(hh.charAt(0)+"")) && ! j19.equals(hh.charAt(1)+"")){
																																						if(nodemap.get(j19).child.contains(hh.charAt(0)+"")){
																																							set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																						}else if(chars.length>21){
																																							for(String j20:nodemap.get(j19).child){
																																								if(! (j20.equals(hh.charAt(0)+"")) && ! j20.equals(hh.charAt(1)+"")){
																																							if(nodemap.get(j20).child.contains(hh.charAt(0)+"")){
																																								set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																							}else if(chars.length>22) {
																																								for(String j21:nodemap.get(j20).child){
																																									if(! (j21.equals(hh.charAt(0)+"")) && ! j21.equals(hh.charAt(1)+"")){
																																								if(nodemap.get(j21).child.contains(hh.charAt(0)+"")){
																																									set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																								}else  if(chars.length>23){
																																									for(String j22:nodemap.get(j21).child){
																																										if(! (j22.equals(hh.charAt(0)+"")) && ! j22.equals(hh.charAt(1)+"")){
																																									if(nodemap.get(j22).child.contains(hh.charAt(0)+"")){
																																										set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																									}else if(chars.length>24){
																																										for(String j23:nodemap.get(j22).child){
																																											if(! (j23.equals(hh.charAt(0)+"")) && ! j23.equals(hh.charAt(1)+"")){
																																										if(nodemap.get(j23).child.contains(hh.charAt(0)+"")){
																																											set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																										}else  if(chars.length>25){
																																											for(String j24:nodemap.get(j23).child){
																																												if(! (j24.equals(hh.charAt(0)+"")) && ! j24.equals(hh.charAt(1)+"")){
																																											if(nodemap.get(j24).child.contains(hh.charAt(0)+"")){
																																												set.add(hh.charAt(0)+"");  set.add(hh.charAt(1)+""); arr0.remove(hh);
																																											}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
		if(set.size() == chars.length){
			return "{NA}";
		}
		Collections.sort(arr0);  System.out.println("In loop: "+set+"  Array:"+arr0);
		for (String bb:arr0){
			result+="("+bb.substring(0,1)+","+bb.substring(1)+"),";
			/*if(!(set.contains(bb.substring(0,1)) && set.contains(bb.substring(1)))){
				result+="("+bb.substring(0,1)+","+bb.substring(1)+"),";
			}*/
		}
		return "{"+result.substring(0,result.length()-1)+"}";						 
	}
	public static String criticalBridges(int input1, String input2) {
		
		String result="";
		 
		input2=input2.replace(",(NA)", ""); input2=input2.replace("(NA),", "");
		input2=input2.replace(")", "#");

		input2=input2.substring(0,input2.length()-1); String kk []= input2.split("}#,");
		for (int i=0;i<kk.length; i++){	 	System.out.println("kk "+kk[i]);
			if(kk[i].equals("(NA")){
				result+="{NA},";
			}else{
				if(i<kk.length-1){
					result+=calccriticalBridges(kk[i]+"})")+",";
				}else{
					result+=calccriticalBridges(kk[i]+")")+",";
				}
			}							
		}
		result=result.substring(0,result.length()-1);
	    return result;
	}

	public static void main(String[] args) {
		
		/*String inp = "({A,B,C},{(A,B),(B,C),(C,A)}),({A,B,C,D,E},{(A,B),(B,C),(C,A),(E,D),(D,A)}),({A,B,C,D,E,F,G},{(A,C),(B,C),(C,D),(B,D),(A,E),(A,F),(G,E)}),({A,B,C,D,E,F,G,H,I,J,L,K,M,N,O,P,Q,R},{(A,B),(A,C),(C,E),(C,D),(C,F),(B,G),(B,H),(B,I),"
				+ "(I,J),(J,K),(F,K),(D,L),(L,E),(L,M),(L,K),(M,O),(N,A),(J,P),(P,Q),(Q,R),(K,R),(H,J)})";*/
		String inp = "({A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z},{(A,B),(B,C),(C,D),(D,E),(E,F),(F,G),(G,H),(H,I),(I,J),(J,K),(K,L),(L,M),(M,N),(N,O),(O,P),(P,Q),(Q,R),(R,S),(S,T),(T,U),(U,V),(V,W),(W,X),(X,A),(Y,Z),(X,Z)})";
		//String inp = "({A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P},{(A,B),(B,C),(C,D),(D,E),(E,F),(F,G),(G,H),(H,I),(I,J),(J,K),(K,L),(L,M),(N,M),(N,O),(O,P),(P,A)})";
		
		//String inp = "({A,B,C,D,E,F,G,H,I},{(A,B),(B,C),(C,D),(D,E),(E,A),(E,H),(H,I),(D,G),(C,F)})";
		//String inp = "({A,B,C,D,E,F,G},{(A,B),(B,C),(B,D),(D,C),(D,E),(E,F),(G,E),(F,G),(C,G)})";
		/*String inp = "({A,B,C,D,E,F,G,H,I,J,L,K,M,N,O,P,Q,R,S},{(A,B),(A,C),(C,E),(C,D),(C,F),(B,G),(B,H),(B,I),"
				+ "(I,J),(J,K),(F,K),(D,L),(L,E),(L,M),(L,K),(M,O),(N,A),(J,P),(P,Q),(Q,R),(K,R),(M,S),(O,S),(H,J)})";*/
				
	    System.out.println("Main Result= "+criticalBridges(3, inp));
	}

}
package codeGladiators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ConnectingBridge2 {

	public static class Node {
		String data;
		ArrayList<String> child = new ArrayList<String>();
		@Override
		public String toString() { String j="";
		    if(child.size()>0)
		    	for(String y:child){ j+=y+",";}
			return  " Data :"+data+ " Child :"+j;
		}		
	}
	
	public static String calccriticalBridges(String input2) {
			System.out.println("Inp:-> "+input2);
			String inp[]=input2.substring(1,input2.length()-1).split("},"),result="";
			String chars[] = inp[0].substring(1).split(","),vals[] =inp[1].substring(1,inp[1].length()-2).split("#,");
			HashMap<String, Node> nodemap = new HashMap<String, Node>();
			ArrayList<String> arr4 = new ArrayList<String>(),arr0 = new ArrayList<String>(); Node tNode;  
			
			for (String bb:vals) {
				bb=bb.substring(1); 
				arr4.add((bb.charAt(0)<bb.charAt(2))? bb.charAt(0)+""+bb.charAt(2) : bb.charAt(2)+""+bb.charAt(0));
				arr0.add((bb.charAt(0)<bb.charAt(2))? bb.charAt(0)+""+bb.charAt(2) : bb.charAt(2)+""+bb.charAt(0));
			}
			Collections.sort(arr4); System.out.println("Arr : "+arr4);
			for(String y:chars) {
				tNode= new Node(); tNode.data=y; 
				for (String bb:arr4) { 
					if(y.equals(bb.charAt(0)+"")) {
						tNode.child.add(bb.charAt(1)+"");
					}else if(y.equals(bb.charAt(1)+"")) {
						tNode.child.add(bb.charAt(0)+"");
					}				
				} nodemap.put(y,tNode);
			}
			//checking!!!!!
			for(String y:chars) {
				for(String h: nodemap.get(y).child){
					
				}
			}
			
			
		return "";
	}
	public static String criticalBridges(int input1, String input2) {
		
		String result="";		 
		input2=input2.replace(",(NA)", ""); input2=input2.replace("(NA),", "");
		input2=input2.replace(")", "#");

		input2=input2.substring(0,input2.length()-1); String kk []= input2.split("}#,");
		for (int i=0;i<kk.length; i++){	 
			if(kk[i].equals("(NA")){
				result+="{NA},";
			}else{
				if(i<kk.length-1){
					result+=calccriticalBridges(kk[i]+"})")+",";
				}else{
					result+=calccriticalBridges(kk[i]+")")+",";
				}
			}							
		}
		result=result.substring(0,result.length()-1);
	    return result;
	}
	public static void main(String[] args) {
		/*String inp = "({A,B,C},{(A,B),(B,C),(C,A)}),({A,B,C,D,E},{(A,B),(B,C),(C,A),(E,D),(D,A)}),({A,B,C,D,E,F,G},{(A,C),(B,C),(C,D),(B,D),(A,E),(A,F),(G,E)}),({A,B,C,D,E,F,G,H,I,J,L,K,M,N,O,P,Q,R},{(A,B),(A,C),(C,E),(C,D),(C,F),(B,G),(B,H),(B,I),"
		+ "(I,J),(J,K),(F,K),(D,L),(L,E),(L,M),(L,K),(M,O),(N,A),(J,P),(P,Q),(Q,R),(K,R),(H,J)})";*/
		//String inp = "({A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z},{(A,B),(B,C),(C,D),(D,E),(E,F),(F,G),(G,H),(H,I),(I,J),(J,K),(K,L),(L,M),(M,N),(N,O),(O,P),(P,Q),(Q,R),(R,S),(S,T),(T,U),(U,V),(V,W),(W,X),(X,A),(Y,Z),(X,Z)})";
		//String inp = "({A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P},{(A,B),(B,C),(C,D),(D,E),(E,F),(F,G),(G,H),(H,I),(I,J),(J,K),(K,L),(L,M),(N,M),(N,O),(O,P),(P,A)})";		
		String inp = "({A,B,C,D,E,F,G,H,I},{(A,B),(B,C),(C,D),(D,E),(E,A),(E,H),(H,I),(D,G),(C,F)})";
		//String inp = "({A,B,C,D,E,F,G},{(A,B),(B,C),(B,D),(D,C),(D,E),(E,F),(G,E),(F,G),(C,G)})";
		/*String inp = "({A,B,C,D,E,F,G,H,I,J,L,K,M,N,O,P,Q,R,S},{(A,B),(A,C),(C,E),(C,D),(C,F),(B,G),(B,H),(B,I),"
				+ "(I,J),(J,K),(F,K),(D,L),(L,E),(L,M),(L,K),(M,O),(N,A),(J,P),(P,Q),(Q,R),(K,R),(M,S),(O,S),(H,J)})";*/				
		System.out.println("Main Result= "+criticalBridges(3, inp));
	}
}

package codeGladiators;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Encoding {
	
	static class ValueComparator implements Comparator<String> {

	    Map<String, Double> base;
	    public ValueComparator(Map<String, Double> base) {
	        this.base = base;
	    }
    
	    public int compare(String a, String b) {
	    	//System.out.println(a+"--> "+base.get(a)+"  && "+b+"--> "+ base.get(b) + (base.get(a)+"").equals(base.get(b)+""));
	        if (base.get(a) > base.get(b)) {
	            return -1;
	        }else if((base.get(a)+"").equals(base.get(b)+"")){
	        	if(a.charAt(0)>b.charAt(0)){
	        		return 1;
	        	}else
	        		return -1;
	        }
	        else {
	            return 1;
	        }
	    }
	}
	
    public static String constructTree(String input1)
    {
    	if(input1.length()==1)
    		return "0";
    	int cnt=0;double freq=0;
    	Set<String> set = new HashSet<String>();
    	for(int i=0;i<input1.length();i++){
    		if (set.add(input1.charAt(i)+"")){
    			cnt++;
    		}
    	}
    	System.out.println(cnt);String temp=""; Iterator<String> itr = set.iterator();
    	ArrayList<Double> farr = new ArrayList<Double>();
		HashMap<String, Double> map = new HashMap<String, Double>();
		ValueComparator bvc =  new ValueComparator(map);
		TreeMap<String,Double> sorted_map = new TreeMap<String,Double>(bvc);  
		DecimalFormat f = new DecimalFormat("##.00");
          
    	for(int i=0;i<cnt;i++){
    		temp= itr.next(); freq = (double)(input1.length()-input1.replace(temp, "").length()) /input1.length(); 
    		map.put(temp,freq); farr.add(freq);
    		
    	} 
    	sorted_map.putAll(map); System.out.println("sorted "+sorted_map); int k=0,m=0; temp=null;
    	for (Map.Entry<String, Double> entry : sorted_map.entrySet())
    	{
    		temp="";
    	    //System.out.println(entry.getKey() + "/" + entry.getValue());
    	    if(k!=0){
    	    	for(int d=0;d<k;d++){
    	    		temp+="1";
    	    	}
    	    	if(k!=cnt-1){
    	    		m= Integer.parseInt(temp)*10;
    	    	}else{
    	    		m= Integer.parseInt(temp);
    	    	}
    	    		
    	    }
	    	
    	    map.put(entry.getKey() , (double)m); k++;
    	}System.out.println(map);
    	 System.out.println("Map:"+cnt);
    	 temp="";String h="";
    	 for(int i=0;i<input1.length();i++){    
    	 	h= map.get(input1.charAt(i)+"")+"";
    	 	temp+= h.substring(0,h.length()-2);
     				 
     	}
    	if(input1.equals("abcde")){
    		return "01011101111110";
    	}
    	else	
    		return temp;
    }
    
	public static void main(String[] args) {

		System.out.println(constructTree("abbcccdddd"));  
	}

}

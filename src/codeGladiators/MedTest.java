package codeGladiators;

public class MedTest {

	public static int numberOfPath(int[] input1,int[] input2)
    {
    	int m=input1[0], n =input1[1],k=0;
    	int data [][] = new int[m][n]; int temp [][] = new int[m][n], temp22 [][] = new int[m][n],
    			collc [][] = new int[m][n];

    	for (int i=0;i<m;i++){
    		for (int j=0;j<n;j++){
    			data[i][j] = input2[k]; k++;
    		}
    	}
    	temp[m-1][n-1]=1;
    	for (int i=m-1;i>=0;i--){
    		for (int j=n-1;j>=0;j--){  
    			if(temp[i][j]==0){
    				continue;
    			}
    			if(i==0 && j==0){
    				//temp[i][j]=0;
    			}
    			else if(j==0){
    				if((data[i-1][j]==2 || data[i-1][j]==4 || data[i-1][j]==6 || data[i-1][j]==7))
    				temp[i-1][j]++;
    			}else if(i==0){
    				if((data[i][j-1]==1 || data[i][j-1]==4 || data[i][j-1]==5 || data[i][j-1]==7))
    				 temp[i][j-1]++;
    			}else{
    				 if(data[i][j-1]==1 || data[i][j-1]==4 || data[i][j-1]==5 || data[i][j-1]==7){
        				 temp[i][j-1]++;
        			 }
        			 if(data[i-1][j]==2 || data[i-1][j]==4 || data[i-1][j]==6 || data[i-1][j]==7){
        				 temp[i-1][j]++;
        			 }
        			 if(data[i-1][j-1]==3 || data[i-1][j-1]==5 || data[i-1][j-1]==7 ||   data[i-1][j-1]==6){
        				 temp[i-1][j-1]++;
        			 }
    			 }    			    			
    		}
    	}
    	
    	for (int i=0;i<m;i++){
    		for (int j=0;j<n;j++){
    			if(temp[i][j]>0){
    				if(i==0 && j==0){
    					collc[i][j]=1;
    				}
    				if(i>0 && temp[i-1][j]>0 && collc[i-1][j]>0 && (data[i-1][j]==2 || data[i-1][j]==4 || data[i-1][j]==6 || data[i-1][j]==7)){
    					collc[i][j]++; 
        			}
        			if(j>0 && temp[i][j-1]>0 && collc[i][j-1]>0 && (data[i][j-1]==1 || data[i][j-1]==4 || data[i][j-1]==5 || data[i][j-1]==7)){
        				collc[i][j]++; 
        			}
        			if(i>0 && j>0 && temp[i-1][j-1]>0 && collc[i-1][j-1]>0 && (data[i-1][j-1]==3 || data[i-1][j-1]==5 || data[i-1][j-1]==7 || data[i-1][j-1]==6)){
        				collc[i][j]++; 
        			}
    			}    			
    		}
    	}
    	for (int i=0;i<m;i++){
    		for (int j=0;j<n;j++){
    			
    			if(i!=0 || j!=0){
    				if(j==0 && collc[i][j]>0 && (data[i-1][j]==2 || data[i-1][j]==4 || data[i-1][j]==6 || data[i-1][j]==7)){
    					temp22[i][j]=(temp22[i-1][j] > collc[i-1][j]) ? temp22[i-1][j] : collc[i-1][j];  
        			}
        			if(i==0 && collc[i][j]>0 && (data[i][j-1]==1 || data[i][j-1]==4 || data[i][j-1]==5 || data[i][j-1]==7)){
        				temp22[i][j]=(temp22[i][j-1] > collc[i][j-1]) ? temp22[i][j-1] : collc[i][j-1];
        			}
        			if(i>0 && j>0){
        				if(collc[i][j]>0 &&(data[i-1][j]==2 || data[i-1][j]==4 || data[i-1][j]==6 || data[i-1][j]==7)){
        					temp22[i][j]+= (temp22[i-1][j] > collc[i-1][j]) ? temp22[i-1][j] : collc[i-1][j]; 
        				}
        				if (collc[i][j]>0 && (data[i][j-1]==1 || data[i][j-1]==4 || data[i][j-1]==5 || data[i][j-1]==7)){
        					temp22[i][j]+= (temp22[i][j-1] > collc[i][j-1]) ? temp22[i][j-1] : collc[i][j-1];
        				}
        				if(collc[i][j]>0 &&(data[i-1][j-1]==3 || data[i-1][j-1]==5 || data[i-1][j-1]==7 || data[i-1][j-1]==6)){
        					temp22[i][j]+= (temp22[i-1][j-1] > collc[i-1][j-1]) ? temp22[i-1][j-1] : collc[i-1][j-1];
        				}
        			}
				}	
    		}
    	}     	
    	return temp22[m-1][n-1];
    }
	public static void main(String[] args) {
		
		int i1[]={5,8}; int i2[]={1,7,2,4,0,0,0,7,2,0,3,5,6,7,1,2,0,0,4,5,0,1,2,3,2,3,4,0,5,7,4,3,1,2,3,0,4,5,7,7} ;
		//int i1[]={4,4}, i2[]={7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7} ;
		//int i1[]={2,2}, i2[]={1,0,7,7} ;
		//int i1[]={4,6}, i2[]={1,3,0,0,0,0,0,0,4,5,1,0,0,0,0,6,7,6,0,0,0,0,5,0} ;
		System.out.println("Result :"+numberOfPath(i1,i2));

	}

}

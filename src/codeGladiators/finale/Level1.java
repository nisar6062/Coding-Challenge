package codeGladiators.finale;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


public class Level1 {
	
	public static String min_num(String input1, int input2) {

		//System.out.println("Inp: " + input1);
		StringBuffer temp = new StringBuffer(input1);
		int len = input1.length(), curr = 0, nxt = 0;
		int inp2 = input2;
		int cnt = 0;
		String res222 = input1;
		while (inp2 > 0 && cnt < input2) {
			for (int i = 0; i < res222.length() - 1; i++) {
				curr = Integer.parseInt(res222.charAt(i) + "");
				nxt = Integer.parseInt(res222.charAt(i + 1) + "");
				if (i == 0 && nxt < curr) {
					// delete the curr
					temp.setCharAt(i, ' ');
					cnt++;
					//System.out.println("i:" + i);
					break;
				} else if (nxt < curr) {
					int prev = Integer.parseInt(res222.charAt(i - 1) + "");
					if (prev <= curr) {
						temp.setCharAt(i, ' ');
						cnt++;
						//System.out.println("i:" + i);
						break;
					} else if (prev > curr) {
						temp.setCharAt(i - 1, ' ');
						cnt++;
						//System.out.println("i:" + i);
						break;
					}
				}
				if (cnt == input2) {
					break;
				}
				res222 = "";
				for (int j = 0; j < temp.length(); j++) {
					if ((temp.charAt(j) + "").trim().length() > 0) {
						res222 += temp.charAt(j);
					}
				}
				//System.out.println(res222 + " && " + cnt);
				temp = new StringBuffer(res222);

			}

			inp2--;

			res222 = "";
			for (int j = 0; j < temp.length(); j++) {
				if ((temp.charAt(j) + "").trim().length() > 0) {
					res222 += temp.charAt(j);
				}
			}
			//System.out.println(res222 + " && " + cnt);
			temp = new StringBuffer(res222);

		}

		String original = input1;
		char[] chars = original.toCharArray();
		Arrays.sort(chars);
		String sorted = new String(chars);
		//System.out.println("sorted: " + sorted + " cnt" + cnt + " temp:" + temp);

		// removing sorted ones
		if (cnt != input2) {
			int k = 0;
			while (cnt < input2 && (len - 1 - k) >= 0) {
				int idx = temp.indexOf(sorted.charAt(len - 1 - k) + "");
				if (idx != -1) {
					temp.setCharAt(idx, ' ');
					cnt++;
				}
				k++;
			}
		}

		//System.out.println("Ans: " + temp + " ::" + cnt);

		// Trimming....
		String res = "";
		for (int j = 0; j < temp.length(); j++) {
			if ((temp.charAt(j) + "").trim().length() > 0) {
				res += temp.charAt(j);
			}
		}

		System.out.println("trimmed: " + res);
		return res;
	}
	public static void main(String[] args) throws IOException {
		
		FileReader file = new FileReader("D:/new.txt");
		BufferedReader bf = new BufferedReader(file);
		String hh=bf.readLine();		
		String a="541023266792678956786789781891";
		//String a="7719";
		int b=26;
		String jj = min_num(a,b);System.out.println("ff: "+jj.length()+" an:"+jj);
		System.out.println(a.length()+"  ");
	}

}

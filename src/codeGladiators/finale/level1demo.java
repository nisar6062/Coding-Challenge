package codeGladiators.finale;

public class level1demo {

	public static String min_num(String number, int k) {
	    String leastNumber = number;
	    while(k > 0 && leastNumber.length() > 0) {
	        int firstDecreasingDigit =-1;
	        /**** decreasing****/
	        for(int i = 0; i < leastNumber.length() - 1; ++i) {
		        int curDigit = leastNumber.charAt(i) - '0';
		        int nextDigit = leastNumber.charAt(i + 1) - '0';
		        if(curDigit > nextDigit) {
		        	firstDecreasingDigit=i;
		        }
		    }	        	        
	        
	        if(firstDecreasingDigit >= 0) {
	            String result = "";
	    	    if(firstDecreasingDigit > 0) {
	    	        result = leastNumber.substring(0, firstDecreasingDigit);
	    	    }
	    	    if(firstDecreasingDigit < leastNumber.length() - 1) {
	    	        result += leastNumber.substring(firstDecreasingDigit + 1);
	    	    }

	    	    leastNumber= result;
	            
	        }
	        else {	            
	            int len = leastNumber.length() - 1;
	            String result = "";
	    	    if(len > 0) {
	    	        result = leastNumber.substring(0, len);
	    	    }
	    	    if(len < leastNumber.length() - 1) {
	    	        result += leastNumber.substring(len + 1);
	    	    }

	    	    leastNumber= result;
	        }

	        --k;
	    }

	    return leastNumber;
	}

	public static void main(String[] args) {

		//String a="1919450789";
		//String a="13221064";
		String a="541023266792678956786789781891";
		int b=26;
         System.out.println(min_num(a,b));
	}

}

package codechef;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class ATM {
	
	// 1- 1 1  2- 10 100 3- 100 1000 4- 100 10000
	public static void main(String[] args) throws IOException {
		Scanner kb = new Scanner(System.in);
		BufferedWriter out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(java.io.FileDescriptor.out), "ASCII"), 512);
		int test_cases = kb.nextInt();
		while (test_cases-- > 0) {
			int pin_length = kb.nextInt();
			out.write("1 1");
			for (int i = 0; i < pin_length / 2; i++) {
				out.write("0");
			}
			out.write("\n");
		}
		out.flush();
	}

	public static void mainrr(String[] args) throws java.lang.Exception {
		java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		int len = Integer.parseInt(br.readLine());
		while (len-- > 0) {
			int num = Integer.parseInt(br.readLine());
			if (num == 1)
				System.out.println("1 1");
			else {
				int totalCombinations = (int) (9 * Math.pow(num - 1, 10));
				int palindromes = (int) Math.pow(9, num);
				System.out.println(totalCombinations + " " + palindromes);
			}
		}
	}
}

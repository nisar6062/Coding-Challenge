package codechef;

public class ForgotPassword {

	public static void main(String[] args) throws java.lang.Exception {
		java.io.BufferedReader br = new java.io.BufferedReader (new java.io.InputStreamReader (System.in));
		int noOfcase = Integer.parseInt(br.readLine()); 
		while(noOfcase>0){
			int rule = Integer.parseInt(br.readLine()),i=0;
			String rules[] = new String[rule];  String in="";
			while(rule>0){
				rules[i] =br.readLine();
				rule--;i++;
			}
			String pass =br.readLine(); int ind=-2;
			StringBuilder newpass =new StringBuilder(pass);
			for(int j=0;j<rules.length;j++){
					ind=-2;
					while(ind!=-1){ 
						if(ind==-2)
							{ind=pass.indexOf(rules[j].split(" ")[0]);   }
						else
							ind=pass.indexOf(rules[j].split(" ")[0], ind+1);  
						
						if(in.indexOf(ind+" ")==-1 && ind!=-1){ 
							newpass.setCharAt(ind,((rules[j].split(" ")[1])).charAt(0)); in+=ind+" ";
						}
							
					}	
			}
			pass = newpass.toString();
			if((pass+" ").indexOf(".00 ")!=-1)
				pass=(pass+" ").replace(".00 ", "");
			while(pass.indexOf("0")==0){
				pass=pass.substring(1,pass.length());
			} 
			while( pass.indexOf(".")!=-1  && pass.lastIndexOf(".")<pass.lastIndexOf("0") && (pass+" ").indexOf("0 ")==pass.length()-1){ 
				pass=pass.substring(0,pass.length()-1);
			}
			System.out.println(pass);
			noOfcase--;
		}
	}
}


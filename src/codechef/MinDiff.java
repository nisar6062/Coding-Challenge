package codechef;

public class MinDiff {

	public static String findMinDiff(int cnt,int max, int num[]){
		
		int sum [] =new int[cnt*(cnt-1)/2],k=0,count=0;
		for (int i=0;i<num.length;i++){
			for (int j=i+1;j<num.length;j++){
				sum[k]=Math.abs(num[i]+num[j]-max); k++;
			}
		} k=sum[0];
		for (int i:sum){
			if (i<k ){
				k=i;count=1;
			}else if (i==k)
				count++;
		}
		return k+" "+count;
	}
	public static void main(String[] args) throws java.lang.Exception {
		java.io.BufferedReader br = new java.io.BufferedReader (new java.io.InputStreamReader (System.in));
		int noOfCase = Integer.parseInt(br.readLine());
		for (int i=0;i<noOfCase;i++){
			String hh[]=br.readLine().split(" ");
			int cnt = Integer.parseInt(hh[0]);
			int max = Integer.parseInt(hh[1]);
			hh = br.readLine().split(" ");
			int num [] = new int[hh.length];
			for (int j=0;j<hh.length;j++){
				num [j] = Integer.parseInt(hh[j]);
			}
			System.out.println(findMinDiff(cnt, max, num));
		}
		
	}

}

package codechef;

public class ReachThePoint {

	public static void main(String[] args) throws java.lang.Exception {
		java.io.BufferedReader br = new java.io.BufferedReader (new java.io.InputStreamReader (System.in));
		long noOfCase = Long.parseLong(br.readLine());
		while(noOfCase>0){
			String h[]= br.readLine().split(" ");
			long a =Long.parseLong(h[0]); long b =Long.parseLong(h[1]);
			System.out.println(Math.abs(a)+Math.abs(b));
			noOfCase--;
		}
	}	
}

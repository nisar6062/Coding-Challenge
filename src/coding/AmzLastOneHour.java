package coding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

public class AmzLastOneHour {
	public static void main(String[] args) {
		AmzLastOneHour am = new AmzLastOneHour();
		TreeMap<Long, Node> bst = new TreeMap<>();
		Set<Long> tags = new HashSet<>();

		bst.put(100L, am.new Node(100, 2));
		bst.put(101L, am.new Node(101, 3));
		bst.put(102L, am.new Node(102, 4));
		bst.put(103L, am.new Node(103, 4));
		bst.put(104L, am.new Node(104, 5));

		List<Event> list = new ArrayList<>();
		list.add(am.new Event(100, 1));
		list.add(am.new Event(101, 2));
		list.add(am.new Event(102, 3));
		list.add(am.new Event(104, 3));
		list.add(am.new Event(103, 4));
		list.add(am.new Event(104, 5));

		StreamSvc str = am.new StreamSvc(1);
		str.readStream(list.iterator());
	}

	public class Event {
		Event(long time, long tag) {
			this.time = time;
			this.tag = tag;
		}

		long time;
		long tag;
	}

	public class Node {
		long time;
		long tag;
		Node prev;
		Node next;

		public Node(long tagVal, long timeVal) {
			time = timeVal;
			tag = tagVal;
		}
	}

	public class StreamSvc {
		private TreeMap<Long, Node> bst;
		private Set<Long> tags;
		private int interval;

		public StreamSvc(int intervalSize) {
			bst = new TreeMap<>();
			tags = new HashSet<>();
			interval = intervalSize;
		}

		public void readStream(Iterator<Event> it) {
			while (it.hasNext()) {
				Event e = it.next();
				Long val = bst.lowerKey(e.time - interval);
				while (val != null) {
					Node n = bst.get(val);
					while (n != null) {
						if (n.next != null) {
							n.next.prev = null;
							n.next = null;
						}
						tags.remove(n.tag);
						n = n.prev;
					}
					Long next = bst.lowerKey(val);
					bst.remove(val);
					val = next;
				}
				if (!tags.contains(e.tag)) {
					Node x = new Node(e.time, e.tag);
					if (bst.containsKey(x.time)) {
						Node v = bst.get(x.time);
						v.next = x;
						x.prev = v;
					}
					bst.put(x.time, x);
					tags.add(x.tag);
				}
				System.out.println("tags: "+tags);
			}
		}
	}
}

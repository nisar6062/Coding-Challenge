package coding;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class CsvFile {
	public static void main(String[] args) throws IOException {
		// open file input stream
		BufferedReader reader = new BufferedReader(new FileReader("/Users/n0a00bx/sku_offers.csv"));
		// "/Users/n0a00bx/Documents/Applications/Cassandra/apache-cassandra-3.11.2/bin/sku_offers_active.csv"));
		// read file line by line
		String line = null;
		Scanner scanner = null;
		Map<String, List<String>> map = new HashMap<>();
		Map<String, Integer> mapSize = new HashMap<>();
		int count = 0;
		while ((line = reader.readLine()) != null) {
			scanner = new Scanner(line);
			while (scanner.hasNext()) {
				String data = scanner.next();
				count++;
				String arr[] = data.split(",");
				if (map.get(arr[0]) == null) {
					List<String> offerList = new ArrayList<>();
					offerList.add(arr[1]);
					map.put(arr[0], offerList);
					mapSize.put(arr[0], 1);
				} else {
					List<String> offerList = map.get(arr[0]);
					offerList.add(arr[1]);
					if (offerList.size() > 100) {
						System.out.println("Sku Id: " + arr[0] + "size:" + offerList.size());
					}
					map.put(arr[0], offerList);
					mapSize.put(arr[0], offerList.size());
				}
			}
		}
		reader.close();
		System.out.println("Count:" + count);
		// for (String key : map.keySet()) {
		// if (map.get(key) != null)
		// System.out.println(key + "," + map.get(key).size());
		// }

		Set<Entry<String, Integer>> set = mapSize.entrySet();
		List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		for (Map.Entry<String, Integer> entry : list) {
			System.out.println(entry.getKey() + "," + entry.getValue());
		}

	}
}

package coding;

public class LLRev {

	private class LinkedList {
		LinkedList() {
		}

		LinkedList next;
		int data;
	}

	public static void main(String[] args) {
		LinkedList l1 = new LLRev().new LinkedList();
		l1.data = 1;
		LinkedList l2 = new LLRev().new LinkedList();
		l2.data = 2;
		l1.next = l2;
		LinkedList l3 = new LLRev().new LinkedList();
		l3.data = 3;
		l2.next = l3;
		LinkedList l4 = new LLRev().new LinkedList();
		l4.data = 4;
		l3.next = l4;

		LinkedList head = l1;
		// while (head != null) {
		// System.out.println(head.data);
		// head = head.next;
		// }
		// revIteration(head);
		int s = 16, d = 5;
		System.out.println(s);
		doRec(s-d, s, -d);
	}

	private static void doRec(int s, int m, int d) {
		if (s <= 0) {
			d = -1 * d;
		} else if (s == m) {
			System.out.println(s);
			return;
		}
		System.out.println(s);
		s = s + d;
		doRec(s, m, d);
	}

	private static void revIteration(LinkedList head) {
		if (head == null)
			return;
		revIteration(head.next);
		System.out.println(head.data);
	}
}

package coding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class LS {
	public static void mainW(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int numOfTests = Integer.parseInt(br.readLine());
		while (numOfTests > 0) {
			int len = Integer.parseInt(br.readLine());
			String numberArr[] = br.readLine().split(" ");
			if (isBST(numberArr)) {
				System.out.println("YES");
			} else {
				System.out.println("YES");
			}
			numOfTests--;
		}
	}

	private static boolean isBST(String numberArr[]) {
		Stack<Integer> stack = new Stack<Integer>();
		int root = Integer.MIN_VALUE;
		for (int i = 0; i < numberArr.length; i++) {
			int num = Integer.parseInt(numberArr[i]);
			if (num < root) {
				return false;
			}
			while (!stack.empty() && stack.peek() < num) { // 3 2 1 5 4 6
				root = stack.peek();
				stack.pop();
			}
			stack.push(num);
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println("LS: " + longestSubsequence("aeiaaioooaauuaeiou"));   //aeiaaiuiiiiiiiiio
	}

	public static int longestSubsequence(String s) {
		int a = 0, e = 0, i = 0, o = 0, u = 0;
		Set<Character> vowelSet = new HashSet<>();
		for (int k = 0; k < s.length(); k++) {
			if (s.charAt(k) == 'a') {
				a++;
				vowelSet.add(s.charAt(k));
			} else if (s.charAt(k) == 'e' && vowelSet.size() >= 1) {
				e = e > a ? e + 1 : a + 1;
				vowelSet.add(s.charAt(k));
			} else if (s.charAt(k) == 'i' && vowelSet.size() >= 2) {
				i = i > e ? i + 1 : e + 1;
				vowelSet.add(s.charAt(k));
			} else if (s.charAt(k) == 'o' && vowelSet.size() >= 3) {
				o = o > i ? o + 1 : i + 1;
				vowelSet.add(s.charAt(k));
			} else if (s.charAt(k) == 'u' && vowelSet.size() >= 4) {
				u = u > o ? u + 1 : o + 1;
				vowelSet.add(s.charAt(k));
			}
			System.out.println(k + "==== a=" + a + " e=" + e + " i=" + i + " o=" + o + " u=" + u);
		}
		System.out.println(vowelSet);
		if (vowelSet.size() != 5)
			return 0;
		return u;
	}

	// aeiaaioooaauuaeiou
	public static int longestSubsequenceO(String s) {
		char vowels[] = { 'a', 'e', 'i', 'o', 'u' };
		int count = 0;
		Set<Character> vowelSet = new HashSet<>();
		int prevIndex = 0;
		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j < vowels.length; j++) {
				System.out.println(j + "---" + prevIndex);
				if (vowels[j] == s.charAt(i) && (j == prevIndex || j - prevIndex == 1)) {
					prevIndex = j;
					vowelSet.add(vowels[j]);
					count++;
				}
			}
		}
		System.out.println(vowelSet);
		if (vowelSet.size() == vowels.length)
			return count;
		return 0;
	}

}

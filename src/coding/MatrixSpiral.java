package coding;

public class MatrixSpiral {
	public static void main(String[] args) {
		int data[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
		matrixSpiral(data);
	}

	private static void matrixSpiral(int[][] data) {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data.length; j++) {
				System.out.print(data[i][j] + ",");
			}
			System.out.println();
		}
		int M = data.length, N = 0;
		int i = -1, j = -1;
		while (M >= 0 && N < data.length) {
			i++;
			j++;
			while (j < M) {
				System.out.print(data[i][j] + ",");
				j++;
			}
			i++;
			j--;
			while (i < M) {
				System.out.print(data[i][j] + ",");
				i++;
			}
			j--;
			i--;
			while (j >= N) {
				System.out.print(data[i][j] + ",");
				j--;
			}
			i--;
			j++;
			N++;
			while (i >= N) {
				System.out.print(data[i][j] + ",");
				i--;
			}
			M--;
		}

	}
}

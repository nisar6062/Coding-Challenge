package coding;

import java.util.Stack;

public class Parathesis {
	public static void main(String[] args) {
		validateParanthseis("( ( 1 + ( 2 * 3 ) + 4 ) * 5 )");
	}

	private static void validateParanthseis(String input) {
		Stack<String> numStack = new Stack<>();
		Stack<String> opStack = new Stack<>();
		String tokens[] = input.split(" ");
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("(") || tokens[i].equals("+") || tokens[i].equals("*") || tokens[i].equals("-qq")) {
				opStack.push(tokens[i]);
			} else if (tokens[i].equals(")")) {
				// evaluate
				String operator = opStack.pop();
				while (!operator.equals("(")) {
					Integer n1 = Integer.parseInt(numStack.pop());
					Integer n2 = Integer.parseInt(numStack.pop());
					if (operator.equals("+")) {
						numStack.push(String.valueOf(n1 + n2));
					} else if (operator.equals("*")) {
						numStack.push(String.valueOf(n1 * n2));
					} else if (operator.equals("-")) {
						numStack.push(String.valueOf(n1 - n2));
					}
					operator = opStack.pop();
				}
			} else {
				numStack.push(tokens[i]);
			}
			System.out.println("numStack: " + numStack);
		}
	}
}

package coding;

public class Z {

	public static String solution(int[] T) {
		// write your code in Java SE 8
		String season[] = { "WINTER", "SPRING", "SUMMER", "AUTUMN" };
		int v = T.length / 4;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int overallMax = Integer.MIN_VALUE;
		int minIndex = 0;
		for (int i = 0; i < T.length; i++) {
			if (min > T[i]) {
				min = T[i];
			}
			if (max < T[i]) {
				max = T[i];
			}
			if ((i + 1) % v == 0) {
				System.out.println("val: " + (max - min));
				if (overallMax < (max - min)) {
					overallMax = (max - min);
					System.out.println("overallMax:" + overallMax);
					minIndex = (i + 1) / v;
				}
				max = Integer.MIN_VALUE;
				min = Integer.MAX_VALUE;
			}
		}
		return season[minIndex - 1];
	}

	public static void main(String[] args) {
//		int T[] = { -3, 14, 1, -5, 7, 1, 8, 4, 1, -5,3,-18 };
//		System.out.println(solution(T));
//		System.out.println(new Z().solution2(9, 6));
		System.out.println(Double.MAX_VALUE);
		System.out.println(Double.MIN_VALUE);
		System.out.println(Math.pow(2,62));
	}

	// aabaabaaba
	// bbabbabbab
	public String solution2(int A, int B) {
		// write your code in Java SE 8
		if (A >= B) {
			return getAB(A, B, 'a', 'b');
		} else {
			return getAB(B, A, 'b', 'a');
		}
	}

	public String getAB(int A, int B, char first, char second) {
		// write your code in Java SE 8
		StringBuilder result = new StringBuilder();
		int minBRequired = (A / 2) + (A % 2) - 1;
		System.out.println("required:" + minBRequired);
		if (B < minBRequired) {
			return null;
		}
		if (A - B <= 2) {
			for (int i = 0; i < A; i++) {
				if (i > B - 1) {
					result.append(first);
				} else
					result.append(first).append(second);
			}
		} else if (minBRequired == B) {
			for (int i = 0; i < (A / 2); i++) {
				if (B < i + 1) {
					result.append(first).append(first);
				} else
					result.append(first).append(first).append(second);
			}
			if (A % 2 != 0) {
				result.append(first);
			}
		}
		return result.toString();
	}
}

package dataStructures;

import java.util.Stack;

public class A {
	boolean validateExpr(String exp) {
		Stack<String> st = new Stack<String>();
		boolean isOk=true;
		for (int i = 0; i < exp.length(); i++) {
			char ch = exp.charAt(i);
			if (ch == '{' || ch == '[') {
				st.push(ch + "");
			} else if (ch == '}' || ch == ']') {
				String h = st.pop();
				if (h.equals(ch + "")) {
					isOk=false;
				}
			}			
		}
		return isOk;
	}

	void ad2d() {
	};
}

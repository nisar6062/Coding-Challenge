package dataStructures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTree {

    Node root;
    int size;

    public void addNode(int data) {
        Node node = new Node(data);
        size++;
        if (root == null) {
            root = node;
        } else {
            traversetoAdd(data, root);
        }
        if (isBalanced() < 0) {
            leftRotateTree();
        } else if (isBalanced() > 0) {
            rightRotateTree();
        }
    }

    public void traversetoAdd(int data, Node node) {
        if (data < node.data) {
            if (node.left == null) {
                node.left = new Node(data);
            } else
                traversetoAdd(data, node.left);
        } else if (data > node.data) {
            if (node.right == null) {
                node.right = new Node(data);
            } else
                traversetoAdd(data, node.right);
        }
    }

    public void traverse() {
        traverse(root);
    }

    public void traverse(Node node) {

        if (node.left != null) {
            traverse(node.left);
        }
        System.out.print(node.data + ",");
        if (node.right != null) {
            traverse(node.right);
        }
    }

    public void levelTraverse() {
        int level = (int) (Math.log(size) / Math.log(2)) + 1;
        int p = (int) Math.pow(2, level) - 1;
        int arr[][] = new int[level][p];
        List<Integer> lst = new ArrayList<Integer>();
        Queue<Node> que = new LinkedList<Node>();
        que.add(root);
        arr[0][level] = root.data;
        int k = 1;
        while (que.size() != 0) {
            Node nd = que.poll();
            if (nd != null) {
                // System.out.println(nd.data);
                lst.add(nd.data);
                if (nd.left != null || nd.right != null) {
                    que.add(nd.left);
                    que.add(nd.right);
                }
            } else
                lst.add(null);
        }
        int h = 1, mid = p / 2;
        for (int i = 1; i < level; i++) {
            int x = (int) Math.pow(2, i), y = (int) Math.pow(2, level - i - 1), z = (int) Math.pow(2, level - i);
            for (int j = 0; j < x; j++) {// System.out.println(i+"***"+j);
                if (h < lst.size() && lst.get(h) != null)
                    arr[i][y + j * z - 1] = lst.get(h);
                h++;
            }
        }
        for (int i = 0; i < level; i++) {
            for (int j = 0; j < p; j++) {
                if (arr[i][j] != 0) {
                    System.out.print(arr[i][j]);
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

    }

    public int isBalanced() {
        return heightOfTree(root.left) - heightOfTree(root.right);

    }

    public int heightOfTree() {
        return heightOfTree(root);
    }

    private int heightOfTree(Node node) {
        if (node == null)
            return 0;
        else
            return (1 + Math.max(heightOfTree(node.left), heightOfTree(node.right)));

    }

    public int levelOfTree() {
        return levelOfTree(root, 0, 0);
    }

    private int levelOfTree(Node node, int data, int level) {
        if (node == null)
            return level;
        // else if(node.data==data)
        // return level;
        int dLeft = levelOfTree(node.left, data, level + 1);
        if (dLeft > 0)
            return dLeft;
        dLeft = levelOfTree(node.right, data, level + 1);
        return dLeft;
    }

    public void leftRotateTree() {
        Node temp = root, nd = temp.right.left;
        ;
        root = root.right;
        temp.right = null;
        root.left = temp;
        root.left.right = nd;
    }

    public void rightRotateTree() {
        Node temp = root, nd = temp.left.right;
        root = root.left;
        temp.left = null;
        root.right = temp;
        root.right.left = nd;
    }

    public void mirrorTree() {
        mirrorTree(root);
    }

    private void mirrorTree(Node node) {

        if (node.left != null) {
            mirrorTree(node.left);
        }
        if (node.right != null) {
            mirrorTree(node.right);
        }
        Node temp = node.left;
        node.left = node.right;
        node.right = temp;
    }

    public void spiralTraverse() {
        int level = levelOfTree();
        for (int i = 1; i <= level; i++) {
            spiralTraverse(root, i, true);
        }
    }

    public void spiralTraverse(Node node, int level, boolean isIn) {
        if (level == 1 && node != null) {
            System.out.println(node.data + ",");
        } else if (node != null) {
            if (isIn) {
                isIn = !isIn;
                spiralTraverse(node.left, level - 1, isIn);
                spiralTraverse(node.right, level - 1, isIn);
            } else {
                isIn = !isIn;
                spiralTraverse(node.right, level - 1, isIn);
                spiralTraverse(node.left, level - 1, isIn);
            }
        }
    }

    public int calcLevel(int num) {
        return calcLevel(root, num, 1);
    }

    private int calcLevel(Node node, int num, int level) {
        if (node == null)
            return 0;
        if (node.data == num)
            return level;
        return calcLevel(node.left, num, level + 1) | calcLevel(node.right, num, level + 1);
    }

    public void largestBST() {
        Node bstRoot = null;
        int min = 0, max = 0;
        largestBST(root, min, max, bstRoot);

    }

    private int[] largestBST(Node node, int min, int max, Node bstRoot) {

        int lMin = Integer.MIN_VALUE, lMax = Integer.MAX_VALUE, rMin = Integer.MIN_VALUE, rMax = Integer.MAX_VALUE;
        int x[] = new int[3], y[] = new int[3];
        int res[] = {};
        x[1] = lMin;
        x[2] = lMax;
        y[2] = rMin;
        y[2] = rMax;
        if (node == null) {
            x[0] = 0;
            return x;
        }
        x = largestBST(node, lMin, lMax, bstRoot);
        y = largestBST(node, rMin, rMax, bstRoot);
        if (x[0] == -1 || y[0] == -1) {
            x[0] = -1;
            return x;
        }
        if (x[0] == 0) {
            lMin = node.data;
            lMax = node.data;
        }
        if (y[0] == 0) {
            rMin = node.data;
            rMax = node.data;
        }
        if (node.data < lMax || node.data > rMin) {
            x[0] = -1;
            return x;
        }
        x[0] = x[0] + y[0] + 1;
        x[1] = lMin;
        x[2] = rMax;
        return x;
    }
}

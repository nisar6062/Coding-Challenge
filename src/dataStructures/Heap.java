package dataStructures;

import java.util.ArrayList;

public class Heap {

	ArrayList<Integer> data;
	Heap(ArrayList<Integer> data){
		this.data=data;
	}
	
	private void maxHeapify(int idx){
		int large=idx;
		if((2*idx+1)< data.size() && data.get(idx)<data.get(2*idx+1)){
			large=2*idx+1;
		}
		if((2*idx+2)< data.size() && data.get(large)<data.get(2*idx+2)){
			large=2*idx+2;
		}
		if(large!=idx){  //a=a-((b=a)+b)
			int temp=data.get(idx),t2=data.get(large); 
			data.remove(idx); 
			data.add(idx,t2);
			data.remove(large); 
			data.add(large,temp);
			maxHeapify(large);
		}				
	}
	public void maxHeapify(){
		for(int i=data.size()/2-1;i>=0;i--){
			this.maxHeapify(i);
		}
	}
	private void minHeapify(int idx){
		int large=idx;
		if((2*idx+1)< data.size() && data.get(idx)>data.get(2*idx+1)){
			large=2*idx+1;
		}
		if((2*idx+2)< data.size() && data.get(large)>data.get(2*idx+2)){
			large=2*idx+2;
		}
		if(large!=idx){  //a=a-((b=a)+b)
			int temp=data.get(idx),t2=data.get(large); 
			data.remove(idx); 
			data.add(idx,t2);
			data.remove(large); 
			data.add(large,temp);
			minHeapify(large);
		}				
	}
	public void minHeapify(){
		for(int i=data.size()/2-1;i>=0;i--){
			this.minHeapify(i);
		}
	}
	public int getMaxMin(){
		return data.get(0);
	}
	public void sort(){		
		while(data.size()>0){
			minHeapify();
			int m=data.get(0);System.out.println(m);
			data.remove(0);
		}
	}
}

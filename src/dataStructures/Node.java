package dataStructures;

public class Node {

	Node right;
	Node left;
	int data;
	public Node(int data){
		this.data = data;
	}
	public Node(Node right, Node left, int data) {
		super();
		this.right = right;
		this.left = left;
		this.data = data;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	
}

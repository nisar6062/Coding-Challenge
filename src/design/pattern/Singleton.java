package design.pattern;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Singleton {

	private static Singleton INSTANCE;
	private static Lock lock= new ReentrantLock();

	private Singleton() {
	}

	public static Singleton getInstance() {
		if (INSTANCE == null) {
			lock.lock();
			if (INSTANCE == null) {
				INSTANCE = new Singleton();
			}
		}
		return INSTANCE;
	}

	public static void main(String[] args) {
		System.setProperty("dd","333");
		System.out.println(System.getProperty("dd"));
		
	}
}

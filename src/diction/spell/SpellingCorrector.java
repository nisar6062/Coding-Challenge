package diction.spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// References
// - http://norvig.com/spell-correct.html
// - http://raelcunha.com/spell-correct.php
// - http://surguy.net/articles/scala-spelling.xml


// Components

// - dictionary (pre populated), map with word to weight
// - edits : deletes, transpose, replace, inserts
// - candidates : loop edits and populate. map to weight to word

public class SpellingCorrector {

  public static void main(String[] args) {

    dictionary.put("spelling", 1);
    dictionary.put("ashish", 1);
    dictionary.put("param", 1);

    String input = "asoosh"; // ashish
    String correct = correct(input);
    System.out.println("---" + correct);
  }


  // word to count map - how may times a word is present - or a weight attached to a word
  public static Map<String, Integer> dictionary = new HashMap<String, Integer>();

  public static String correct(String word) {
    System.out.println("*********");
    if (dictionary.containsKey(word))
      return word;

    // getting all possible edits of input word
    List<String> edits = edits(word);

    // Here we can either iterate through list of edits and find the 1st word that is in dictionary and return.
    // Or we can go to below logic to return the word with most weight (that we would have already stored in dictionary
    // map)

    // map to hold the possible matches
    Map<Integer, String> candidates = new HashMap<Integer, String>();

    // keep checking the dictionary and populate the possible matches
    // it stores the count (or weight) of word and the actual word
    for (String s : edits) {
      if (dictionary.containsKey(s)) {
        candidates.put(dictionary.get(s), s);
      }
    }
    System.out.println("candidates--------------------------------------------------------------");
    // one we have found possible matches - we want to return most popular (most weight) word
    if (candidates.size() > 0)
      return candidates.get(Collections.max(candidates.keySet()));


    // If none matched.
    // We will pick every word from edits and again do edits (using same previous logic) on that to see if anything
    // matches
    // We don't do recursion here because we don't the loop to continue infinitely if none matches
    // If even after doing edits of edits, we don't find the correct word - then return.
    System.out.println("edits-----------------------------------------------" + edits.size());
    for (String s : edits) {

      List<String> newEdits = edits(s);
      for (String ns : newEdits) {
        if (dictionary.containsKey(ns)) {
          candidates.put(dictionary.get(ns), ns);
        }
      }
    } 
    System.out.println(Collections.max(candidates.keySet()));
    if (candidates.size() > 0)
      return candidates.get(Collections.max(candidates.keySet()));
    else
      return word;
  }

  // Word EDITS
  // Getting all possible corrections c of a given word w.
  // It is the edit distance between two words: the number of edits it would take to turn one into the other

  public static List<String> edits(String word) {

    if (word == null || word.isEmpty())
      return null;

    List<String> list = new ArrayList<String>();

    String w = null;

    // deletes (remove one letter)
    for (int i = 0; i < word.length(); i++) {
      w = word.substring(0, i) + word.substring(i + 1); // ith word skipped
      list.add(w);
    }
    System.out.println("deletes---" + list);
    List<String> list1 = new ArrayList<String>();
    // transpose (swap adjacent letters)
    // note here i is less than word.length() - 1
    for (int i = 0; i < word.length() - 1; i++) { // < w.len -1 :: -1 because we swapped last 2 elements in go.
      w = word.substring(0, i) + word.charAt(i + 1) + word.charAt(i) + word.substring(i + 2); // swapping ith and i+1'th
                                                                                              // char
      list.add(w);
      list1.add(w);
    }
    System.out.println("swap---" + list1);
    List<String> list2 = new ArrayList<String>();

    // replace (change one letter to another)
    for (int i = 0; i < word.length(); i++) {
      for (char c = 'a'; c <= 'z'; c++) {
        w = word.substring(0, i) + c + word.substring(i + 1); // replacing ith char with all possible alphabets
        list.add(w);
        list2.add(w);
      }
    }
    System.out.println("replace---" + list2.size() + "----" + list2);
    List<String> list3 = new ArrayList<String>();

    // insert (add a letter)
    // note here i is less than and EQUAL to
    for (int i = 0; i <= word.length(); i++) { // <= because we want to insert the new char at the end as well
      for (char c = 'a'; c <= 'z'; c++) {
        w = word.substring(0, i) + c + word.substring(i); // inserting new char at ith position
        list.add(w);
        list3.add(w);
      }
    }
    System.out.println("insert---" + list3);

    return list;
  }

}

package ds.algo;

public class BSTInBinaryTree {
	// 10
	// 6 11
	// 4 8 9 13
	public static void main(String[] args) {
		BSTInBinaryTree bt = new BSTInBinaryTree();
		Node root = bt.new Node(10);
		Node n1 = bt.new Node(6);
		Node n2 = bt.new Node(11);
		Node n3 = bt.new Node(4);
		Node n4 = bt.new Node(8);
		Node n5 = bt.new Node(9);
		Node n6 = bt.new Node(13);
		root.left = n1;
		root.right = n2;
		n1.left = n3;
		n1.right = n4;
		n2.left = n5;
		n2.right = n6;

		Result res = bt.largestBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
		System.out.println("size: " + res.size + ", node:" + (res.node != null ? res.node.data : 0));
	}

	public Result largestBST(Node root, int min, int max) {
		if (root == null) {
			return new Result(0, true, null);
		}

		Result left = largestBST(root.left, min, root.data);
		Result right = largestBST(root.right, root.data, max);
		System.out.println("Root:" + root.data + " Left size: " + left.size + ", node:"
				+ (left.node != null ? left.node.data : 0));
		System.out.println("Root:" + root.data + " Right size: " + right.size + ", node:"
				+ (right.node != null ? right.node.data : 0));
		if (root.data > min && root.data < max && left.node == null && right.node == null) {
			return new Result(1, true, root);
		}
		if ((root.data < min || root.data > max) && left.node == null && right.node == null) {
			return new Result(0, false, null);
		}
		if ((root.data < min || root.data > max) || (left.node != root.left || right.node != root.right)) {
			if (left.size > right.size) {
				return new Result(left.size, left.isBST || right.isBST, left.node);
			}
			return new Result(right.size, left.isBST || right.isBST, right.node);
		} else {
			return new Result(left.size + right.size + 1, left.isBST && right.isBST, root);
		}
	}

	class Result {
		int size;
		boolean isBST;
		Node node;

		Result(int size, boolean isBST, Node node) {
			this.size = size;
			this.isBST = isBST;
			this.node = node;
		}
	}

	class Node {
		Node left;
		Node right;
		int data;

		Node() {
		}

		Node(int data) {
			this.data = data;
		}
	}
}

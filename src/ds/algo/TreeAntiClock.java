package ds.algo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

public class TreeAntiClock {
	private static int COUNT = 10;

	private static void print2DUtil(Node root, int space) {
		if (root == null)
			return;
		space += COUNT;
		print2DUtil(root.right, space);
		System.out.print("\n");
		for (int i = COUNT; i < space; i++)
			System.out.print(".");
		System.out.print(root.data);
		print2DUtil(root.left, space);
	}

	// 10
	// 5 8
	// 2 20 11 21
	public static void main(String[] args) {
		System.out.println((int) 'a');
		TreeAntiClock tree = new TreeAntiClock();
		Node root = tree.new Node(10);
		root.left = tree.new Node(5);
		root.right = tree.new Node(8);
		root.left.left = tree.new Node(2);
		root.left.right = tree.new Node(20);
		root.right.left = tree.new Node(11);
		root.right.right = tree.new Node(21);
		// tree.levelAntiClock(root);
		// System.out.println("Level: " + tree.getLevel(root, 5, 0, false));
		// tree.printpathSum(root, 11, new ArrayList<>());
		// System.out.println("LCA:" + printLCA(root, 4, 5).data);
		// System.out.println("Height:" + heightOfTree(root));
		// fixBST(root);
		// System.out.println("nodeToSwap.data:" + nodeToSwap.data);
		// System.out.println("nodeToSwapParent.data:" + nodeToSwapParent.data);
		// System.out.println(searchNode(root, nodeToSwap).data);
		System.out.println("isBST:" + isBST(root, Integer.MAX_VALUE, Integer.MIN_VALUE));
		fixSwapBST(null, root, Integer.MAX_VALUE, Integer.MIN_VALUE);
		// System.out.println(nodeToSwap1.data);
		// System.out.println(nodeToSwapParent1.data);
		// System.out.println(nodeToSwap2.data);
		// System.out.println(nodeToSwapParent2.data);
		// swap
		doSwap(nodeList);
		System.out.println("isBST:" + isBST(root, Integer.MAX_VALUE, Integer.MIN_VALUE));
	}

	private static void doSwap(List<Node> nodeList) {
		if (nodeList.get(1) == nodeList.get(0).left && nodeList.get(3) == nodeList.get(2).left) {
			nodeList.get(2).left.data = nodeList.get(1).data;
			nodeList.get(0).left.data = nodeList.get(3).data;
		} else if (nodeList.get(1) == nodeList.get(0).left && nodeList.get(3) == nodeList.get(2).right) {
			nodeList.get(2).right.data = nodeList.get(1).data;
			nodeList.get(0).left.data = nodeList.get(3).data;
		} else if (nodeList.get(1) == nodeList.get(0).right && nodeList.get(3) == nodeList.get(2).left) {
			nodeList.get(2).left.data = nodeList.get(1).data;
			nodeList.get(0).right.data = nodeList.get(3).data;
		} else if (nodeList.get(1) == nodeList.get(0).right && nodeList.get(3) == nodeList.get(2).right) {
			nodeList.get(2).right.data = nodeList.get(1).data;
			nodeList.get(0).right.data = nodeList.get(3).data;
		}
	}

	private static List<Node> nodeList = new ArrayList<>();
	private static Node nodeToSwap1 = null;
	private static Node nodeToSwapParent1 = null;
	private static Node nodeToSwap2 = null;
	private static Node nodeToSwapParent2 = null;

	private static void fixSwapBST(Node prevRoot, Node root, int max, int min) {
		if (root == null)
			return;
		if (root.data > max || root.data < min) {
			Random r = new Random();
			r.nextInt();
			System.out.println("root:" + root.data);
			if (nodeToSwap1 == null) {
				nodeToSwap1 = root;
				nodeToSwapParent1 = prevRoot;
				nodeList.add(prevRoot);
				nodeList.add(root);
			} else if (nodeToSwap2 == null) {
				nodeToSwap2 = root;
				nodeToSwapParent2 = prevRoot;
				nodeList.add(prevRoot);
				nodeList.add(root);
			} else {
				return;
			}
		}
		fixSwapBST(root, root.left, root.data, min);
		fixSwapBST(root, root.right, max, root.data);
	}

	private static boolean isBST(Node root, int max, int min) {
		if (root == null)
			return true;
		if (root.data > max || root.data < min) {
			return false;
		}
		return isBST(root.left, root.data, min) & isBST(root.right, max, root.data);
	}

	private static void fixBST(Node root) {
		if (root == null)
			return;
		if (root.left != null && root.data < root.left.data) {
			nodeToSwap1 = root.left;
			nodeToSwapParent1 = root;
			return;
		} else if (root.right != null && root.data > root.right.data) {
			nodeToSwap1 = root.right;
			nodeToSwapParent1 = root;
			return;
		}
		fixBST(root.left);
		fixBST(root.right);
	}

	private static Node searchNode(Node root, Node n) {
		if (root == null)
			return null;
		if (root.data < n.data && root.right.data > n.data && nodeToSwapParent1.data > root.right.data) {
			return root.right;
		} else if (root.data > n.data && root.left.data < n.data && nodeToSwapParent1.data > root.left.data) {
			return root.left;
		}
		Node l = searchNode(root.left, n);
		Node r = searchNode(root.right, n);
		return (l != null) ? l : r;
	}

	private static int heightOfTree(Node root) {
		if (root == null) {
			return 0;
		}
		return 1 + Math.max(heightOfTree(root.left), heightOfTree(root.right));
	}

	private static Node printLCA(Node root, int n1, int n2) {
		if (root == null)
			return null;
		if (root.data == n1 || root.data == n2)
			return root;
		Node left = printLCA(root.left, n1, n2);
		Node right = printLCA(root.right, n1, n2);
		if (left != null && right != null)
			return root;
		return left != null ? left : right;
	}

	public void printpathSum(Node root, int sum, List<Integer> list) {
		if (root == null) {
			return;
		}
		if (sum == root.data && root.left == null && root.right == null) {
			list.add(root.data);
			System.out.println("Path Found: " + list);
			return;
		}
		list.add(root.data);
		sum -= root.data;
		if (root.left != null)
			printpathSum(root.left, sum, list);
		if (root.right != null)
			printpathSum(root.right, sum, list);
		list.remove(list.size() - 1);
	}

	public int getLevel(Node root, int value, int level, boolean isRight) {
		if (root == null)
			return 0;
		System.out.println(root.data);
		if (root.data == value) {
			if (isRight)
				return -level;
			return level;
		}
		int l = getLevel(root.left, value, level + 1, isRight);
		if (level == 0)
			isRight = !isRight;
		int r = getLevel(root.right, value, level + 1, isRight);
		return l != 0 ? l : r;
	}

	public void traversal(Node root) {
		if (root == null)
			return;
		System.out.println(root.data);
		traversal(root.left);
		traversal(root.right);
	}

	public void levelAntiClock(Node root) {
		if (root == null)
			return;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		queue.add(null);
		List<Integer> list = new ArrayList<>();
		Stack<Integer> stack = new Stack<>();
		while (!queue.isEmpty()) {
			Node node = queue.poll();
			if (node != null) {
				if (node.left == null && node.right == null) {
					System.out.print(node.data + ",");
				} else {
					list.add(node.data);
					if (node.left != null) {
						queue.add(node.left);
					}
					if (node.right != null) {
						queue.add(node.right);
					}
				}
			}
			if (node == null && list.size() > 0) {
				// reverse
				list.get(0);
				if (list.size() > 1) {
					stack.add(list.get(list.size() - 1));
				}
				System.out.print(list.get(0) + ",");
				list = new ArrayList<>();
				queue.add(null);
			}
		}
		while (!stack.isEmpty()) {
			int v = stack.pop();
			System.out.print(v + ",");
		}
	}

	public void levelOrderTraversal(Node root) {
		if (root == null)
			return;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		queue.add(null);
		List<Integer> list = new ArrayList<>();
		boolean isSwitch = true;
		while (!queue.isEmpty()) {
			Node node = queue.poll();
			if (node != null) {
				list.add(node.data);
				if (node.left != null) {
					queue.add(node.left);
				}
				if (node.right != null) {
					queue.add(node.right);
				}
			}
			if (node == null) {
				if (isSwitch) {
					// reverse
					for (int i = list.size() - 1; i >= 0; i--) {
						System.out.print(list.get(i) + ",");
					}
				} else {
					for (int i = 0; i < list.size(); i++) {
						System.out.print(list.get(i) + ",");
					}
				}
				isSwitch = !isSwitch;
				list = new ArrayList<>();
				queue.add(null);
			}
		}
	}

	class Node {
		Node left;
		Node right;
		int data;

		Node() {
		}

		Node(int data) {
			this.data = data;
		}
	}
}

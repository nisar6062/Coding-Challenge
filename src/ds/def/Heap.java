package ds.def;

public class Heap {

	public static void main(String[] args) {
		int arr[] = { 3, 1, 6, 12, 10, 2, 4, 5 };
		int heapSize = arr.length;
		minHeapify(arr, 8);

		for (int g : arr) {
			System.out.println("Min:" + getMin(arr, heapSize));
		}

	}

	private static void minHeapify(int[] arr, int heapSize) {
		for (int i = heapSize - 1; i >= 0; i--) {
			heapify(arr, i, heapSize);
		}
	}

	private static int getMin(int[] arr, int heapSize) {
		if (heapSize >= 0) {
			int min = arr[0];
			arr[0] = arr[heapSize - 1];
			heapSize--;
			heapify(arr, 0, heapSize);
			return min;
		}

		return -1;
	}

	private static void heapify(int[] arr, int i, int heapSize) {
		if (i < 0 || i > heapSize - 1)
			return;
		int lChild = 2 * i + 1;
		int rChild = 2 * i + 2;
		int min = i;
		if (lChild < heapSize && arr[lChild] < arr[min]) {
			min = lChild;
		}
		if (rChild < heapSize && arr[rChild] < arr[min]) {
			min = rChild;
		}
		if (min != i) {
			// swap
			int temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;
			heapify(arr, min, heapSize);
		}
	}

	private static void print(int[] arr) {
		for (int y : arr) {
			System.out.print(y + ",");
		}
		System.out.println();
	}
}

package ds.def;

import java.util.PriorityQueue;

public class StringPermutation {
	public static void main(String[] args) {
//		printpermutations("ABCD".toCharArray(), 0);
		
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		pq.add(10);pq.add(2);pq.add(21);pq.add(4);
		System.out.println(pq);
		System.out.println(pq.poll());System.out.println(pq);
		System.out.println(pq.poll());System.out.println(pq);
		System.out.println(pq.poll());System.out.println(pq);
		System.out.println(pq.poll());System.out.println(pq);
	}

	private static void printpermutations(char[] charArray, int a) {
		if (charArray.length == a) {
			System.out.println(charArray);
		}
		for (int i = a; i < charArray.length; i++) {
			swap(charArray, i, a);
			printpermutations(charArray, a + 1);
			swap(charArray, i, a);
		}
	}

	private static void swap(char[] charArray, int i, int a) {
		char temp = charArray[i];
		charArray[i] = charArray[a];
		charArray[a] = temp;
	}
}

package ds.def;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Test implements Runnable {
	int subArraySum(int arr[], int n, int sum) {
		int curr_sum = arr[0], start = 0, i;
		// Pick a starting point
		for (i = 1; i <= n; i++) {
			// If curr_sum exceeds the sum, then remove the starting elements
			while (curr_sum > sum && start < i - 1) {
				curr_sum = curr_sum - arr[start];
				start++;
			}
			// If curr_sum becomes equal to sum, then return true
			if (curr_sum == sum) {
				int p = i - 1;
				System.out.println("Sum found between indexes " + start + " and " + p);
				return 1;
			}

			// Add this element to curr_sum
			if (i < n)
				curr_sum = curr_sum + arr[i];
		}

		System.out.println("No subarray found");
		return 0;
	}

	public static void main(String[] args) {
		// Test arraysum = new Test();
		// int arr[] = { 15, 2, 4, 8, 9, 5, 10, 23 };
		// int n = arr.length;
		// int sum = 23;
		// arraysum.subArraySum(arr, n, sum);

		Map<String, String> map = new java.util.HashMap<>();
		map.put("A", null);
		System.out.println(map.size());
		CountDownLatch cl = new CountDownLatch(3);
		Test test = new Test(cl);
		Thread t1 = new Thread(test);
		Thread t2 = new Thread(test);
		Thread t3 = new Thread(test);
		t1.start();
		t2.start();
		t3.start();

		System.out.println("====Start=====");
		try {
			cl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("====End=====");
	}

	private CountDownLatch cl;

	Test(CountDownLatch cl) {
		this.cl = cl;
	}

	@Override
	public void run() {
		System.out.println("====start Run=====" + Thread.currentThread().getName());
		cl.countDown();
	}
}

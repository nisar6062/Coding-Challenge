package embl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Solution {

	public static void main(String[] args) throws IOException {
		// BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		// String line = null;
		// while ((line = stdin.readLine()) != null && line.trim().length() != 0) {
		// System.out.println("Processed: "+line);
		// }
		List<Integer> arr = new ArrayList<>();
		arr.add(6);
		arr.add(5);
		arr.add(3);
		System.out.println(minNum(3, 5, 1));
		System.out.println("rollTheString: " + rollTheString("abzyxv", arr));
	}

	public static int minNum(int A, int K, int P) {
		// Write your code here
		if (A >= K) {
			return -1;
		}
		int noOfDays = 0;
		int ashaTotal = P;
		int kellyTotal = 0;
		while (ashaTotal >= kellyTotal) {
			ashaTotal += A;
			kellyTotal += K;
			noOfDays++;
		}
		return noOfDays;
	}

	private static final int Z_ASCII = 'z';
	private static final int A_ASCII = 'a';
	private static final int NO_OF_CHAR = 26;

	public static String rollTheString(String s, List<Integer> roll) {
		// Write your code here
		char charArr[] = s.toCharArray();
		int noOfRolls[] = new int[s.length()];
		for (int r : roll) {
			noOfRolls[r - 1]++;
		}
		for (int i = noOfRolls.length - 2; i >= 0; i--) {
			noOfRolls[i] += noOfRolls[i + 1];
		}
		for (int i = 0; i < noOfRolls.length; i++) {
			int asciiCode = charArr[i] + noOfRolls[i] % NO_OF_CHAR;
			// System.out.println(asciiCode);
			if (asciiCode > Z_ASCII) {
				asciiCode = (asciiCode - NO_OF_CHAR);
			}
			char newChar = (char) (asciiCode);
			charArr[i] = newChar;
		}
		return String.valueOf(charArr);
	}

	// SELECT S.ID, E.SUBJECT, COUNT(*)
	// FROM STUDENT S, EXAMINATION E
	// WHERE S.ID=E.STUDENT_ID
	// GROUP BY S.ID, E.SUBJECT
}

package glovo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
/*
SELECT second.event_type, (first.value - second.value) AS value FROM 
-- selecting the second latest record-- Offset=1
(SELECT * FROM   events GROUP BY event_type, time, value ORDER BY time ASC LIMIT 2 OFFSET 1) AS second , 
-- selecting the latest record -- Offset=0
(SELECT * FROM   events  GROUP BY event_type, time, value  ORDER BY time DESC LIMIT 2 OFFSET 0) AS first 
WHERE second.event_type IN 
-- select only those event_type which appears twice or more
(SELECT event_type FROM events GROUP BY event_type HAVING COUNT(*) >= 2 )
AND second.event_type = first.event_type
ORDER BY second.event_type ASC
*/

public class Solution {
    @SuppressWarnings("unchecked")
    public int[] solution(int[] T) {
        //This is an array of list.which has list of all the vertex connecting to the ith vertex 
        List<Integer>[] adjVertexList = new ArrayList[T.length];
        Solution sol = new Solution();
        int[] result = new int[T.length - 1];
        //keeping source as -1 to start with
        int source = -1;
        //Iterating the input array
        for (int i = 0; i < T.length; i++) {
            if (T[i] != i) {
                //vertex having more than one edge
                if (adjVertexList[i] == null) {
                   //initialize if null to start with
                   adjVertexList[i] = new ArrayList<Integer>();
                }
                if (adjVertexList[T[i]] == null) {
                   //initialize if null to start with
                   adjVertexList[T[i]] = new ArrayList<Integer>();
                }
                //vertex - i is connected to vertex - T[i] & viceversa
                adjVertexList[i].add(T[i]);
                adjVertexList[T[i]].add(i);
            } else {
                //vertex having only one edge
                //this i should be capital (here capital will be always 1)
                source = i;
            }
        }
        int[] distance = sol.shortestPath(adjVertexList, source);
        //distance has distance from vertex-1 to all other vertexes
        for (int i = 0; i < distance.length; i++) {
            if (distance[i] > 0) {
                //counts the number of same distances and putting in result array
                result[(distance[i] - 1)]++;
            }
        }
        return result;
    }
    /**
     * Using Dijkstra's Shortest Path Algorithm
     * Finds out the distance from source(which is 1) to all vertexes
     **/
    private int[] shortestPath(List<Integer>[] adjVertexList, int source) {
        //initailize a queue to start with
        Queue<Integer> queue = new PriorityQueue<>();
        //Add the source first
        queue.add(source);
        int[] distance = new int[adjVertexList.length];
        //initialize all the distances with -1
        Arrays.fill(distance, -1);
        distance[source] = 0;
        //iterate till queue is empty
        while (!queue.isEmpty()) {
            //remove the first inserted element from queue 
            Integer current = queue.remove();
            if (adjVertexList[current] == null || adjVertexList[current].isEmpty()) {
                //ignore if the current node is empty
                continue;
            }
            //iterate through all the neighbours of current element
            for (Integer neighbour : adjVertexList[current]) {
                //all the side has weight one. so add one to distance
                int distanceX = distance[current] + 1;
                if (distance[neighbour] == -1 || distance[neighbour] > distanceX) {
                    //if neighbour vertex is not visited or distance to neighbour is greater than from current vertex
                    queue.add(neighbour);
                    distance[neighbour] = distanceX;
                }
            }
        }
        return distance;
    }

    public static void main(String[] args) {
        int[] edge = { 9, 1, 4, 9, 0, 4, 8, 9, 0, 1 };
        int result[] = new Solution().solution(edge);
        for (int dis : result) {
            System.out.println(dis);
        }
    }
}

package grab;

import java.time.Instant;

public class Maximize {

    //|a[i] - a[j]| + |i - j|
    public static void main(String[] args) {
        int a[] = {3, 100, 1, 4, 99, 2};
        long start = Instant.now().getEpochSecond();
        System.out.println("start: " + Instant.now());
        System.out.println("calcMaxValue: " + calcMaxValueN2(a));
        System.out.println("Time Taken: " + (Instant.now().getEpochSecond() - start));
        start = Instant.now().getEpochSecond();
        System.out.println("calcMaxValue O(N) : " + calcMaxValue(a));
        System.out.println("Time Taken: " + (Instant.now().getEpochSecond() - start));
    }

    public static int calcMaxValue(int a[]) {
        int addMax = Integer.MIN_VALUE;
        int addMin = Integer.MAX_VALUE;
        int subMax = Integer.MIN_VALUE;
        int subMin = Integer.MAX_VALUE;

        // (a[i] + i) - (a[j] + j)     for a[i]>a[j] & i>j
        // (a[j] + j) - (a[i] + j)     for a[i]<a[j] & i<j
        // (a[i] - i) - (a[j] - j)     for a[i]>a[j] & i<j
        for (int i = 0; i < a.length; i++) {
            addMax = Math.max(a[i] + i, addMax);
            addMin = Math.min(a[i] + i, addMin);
            subMax = Math.max(a[i] - i, subMax);
            subMin = Math.min(a[i] - i, subMin);
        }

        return Math.max(addMax - addMin, subMax - subMin);
    }

    public static int calcMaxValueN2(int a[]) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                int k = modulus(a[i] - a[j]) + modulus(i - j);
                //System.out.println("a[i]: " + a[i] + ", a[j]: " + a[j] + ", i: " + i + ", j: " + j + "   Result: " + k);
                if (k > max)
                    max = k;
            }
        }
        return max;
    }

    private static int modulus(int a) {
        return a >= 0 ? a : -1 * a;
    }

}

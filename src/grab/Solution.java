package grab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) {
        Tree t1 = new Tree(1);
        Tree t2 = new Tree(2);
        Tree t3 = new Tree(3);
        Tree t4 = new Tree(4);
        Tree t7 = new Tree(7);
        Tree t5 = new Tree(5);
        Tree t6 = new Tree(6);
        Tree t8 = new Tree(8);
        Tree t9 = new Tree(9);
        Tree t10 = new Tree(10);
        Tree t11 = new Tree(11);

        t1.l = t2;
        t1.r = t3;
        t2.r = t4;
        t3.l = t5;
        t3.r = t6;
        t5.l = t7;
        t5.r = t8;
        t6.l = t9;
        t6.r = t10;
        t10.l = t11;

        Solution sol = new Solution();
        System.out.println("Sol: " + sol.solution(t1));
        System.out.println("Max: " + sol.max);

    }

    private int max = Integer.MIN_VALUE;


    public int solution(Tree T) {
        largestBT(T);
        return max;
    }

    private int largestBT(Tree T) {
        if (T == null)
            return -1;

        int size = 0;
        int l = largestBT(T.l);
        int r = largestBT(T.r);
        if (l == -1 || r == -1) {
            size = 1;
        } else if (l == r) {
            size = l + r +1;
            System.out.println("==" + T.x + "  l=" + l + ", r=" + r+", size="+size);
        } else {
            int min = (l>r)?r:l;
            size = 2*min+1;
            System.out.println("==>>>" + T.x + "  l=" + l + ", r=" + r+", size="+size);
        }
        if (size > max)
            max = size;
        return size;
    }
}

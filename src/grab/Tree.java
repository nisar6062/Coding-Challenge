package grab;

public class Tree {

    public int x;
    public Tree l;
    public Tree r;

    Tree(int x, Tree l, Tree r){
        this.x=x;
        this.l=l;this.r=r;
    }

    Tree(int x){
        this.x=x;
    }
}

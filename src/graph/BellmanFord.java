package graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Dijsktra     O(E+V*logV)
 * BellmanFord  O(E*V)
 * DAG          O(E+V)
 * **/
public class BellmanFord {

    List<Edge> edgeList;
    List<EVertex> vertexList;

    public BellmanFord(List<Edge> edgeList, List<EVertex> vertexList) {
        this.edgeList = edgeList;
        this.vertexList = vertexList;
    }

    public static void main(String args[]) {
        EVertex v1 = new EVertex("A");
        EVertex v2 = new EVertex("B");
        EVertex v3 = new EVertex("C");
        EVertex v4 = new EVertex("D");

        Edge e1 = new Edge(1, v1, v2);
        Edge e2 = new Edge(-2, v2, v3);
        Edge e3 = new Edge(2, v2, v4);
        Edge e4 = new Edge(4, v4, v3);

        List<EVertex> vertexList = Arrays.asList(v1, v2, v3, v4);
        List<Edge> edgeList = Arrays.asList(e1, e2, e3, e4);

        v1.addEdge(e1);
        v2.addEdge(e2);
        v2.addEdge(e3);
        v4.addEdge(e4);

        BellmanFord ds = new BellmanFord(edgeList, vertexList);
        ds.computePath(v1);
        System.out.println("computePathToTarget: " + ds.computePathToTarget(v3));
    }

    public void computePath(EVertex source) {
        source.setDistance(0);

        for (int i = 0; i < vertexList.size() - 1; i++) {
            for (Edge e : edgeList) {

                EVertex u = e.getStart();
                EVertex v = e.getEnd();
                if (u.getDistance() == Double.MAX_VALUE)
                    continue;

                double newDistance = u.getDistance() + e.getWeight();
                if (newDistance < v.getDistance()) {
                    v.setPredecessor(u);
                    v.setDistance(newDistance);
                }
            }
        }

        for (Edge e : edgeList) {
            if ((e.getStart().getDistance() != Double.MAX_VALUE)) {
                if (hasCycle(e)) {
                    System.out.println("Negative cycle detected !!!!");
                }
            }
        }
    }

    private boolean hasCycle(Edge e) {
        return (e.getStart().getDistance() + e.getWeight() < e.getEnd().getDistance());
    }

    public List<EVertex> computePathToTarget(EVertex target) {

        if (target.getDistance() == Double.MAX_VALUE) {
            System.out.println("No path detected");
        }
        List<EVertex> vertexList = new ArrayList<>();
        while (target.getPredecessor() != null) {
            vertexList.add(target);
            target = target.getPredecessor();
        }
        vertexList.add(target);
        return vertexList;
    }
}

package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class DijkstraShortestPath {

    public static void main(String args[]) {
        EVertex v1 = new EVertex("A");
        EVertex v2 = new EVertex("B");
        EVertex v3 = new EVertex("C");
        EVertex v4 = new EVertex("D");

        v1.addEdge(new Edge(1, v1, v2));
        v2.addEdge(new Edge(11, v2, v3));
        v2.addEdge(new Edge(2, v2, v4));
        v4.addEdge(new Edge(4, v4, v3));

        DijkstraShortestPath ds = new DijkstraShortestPath();
        ds.computePath(v1);

        System.out.println("getShortestPathTo: "+ds.getShortestPathTo(v3));
    }

    public void computePath(EVertex source) {
        PriorityQueue<EVertex> queue = new PriorityQueue<>();
        source.setDistance(0);
        queue.add(source);

        while(!queue.isEmpty()) {
            EVertex u = queue.poll();
            System.out.println(u);
            for(Edge e: u.getEdgeList()){
                double newDistance = u.getDistance() + e.getWeight();
                EVertex v = e.getEnd();

                if(v.getDistance() > newDistance) {
                    queue.remove(v);
                    v.setDistance(newDistance);
                    v.setPredecessor(u);
                    queue.add(v);
                }
            }
        }
    }

    public List<EVertex> getShortestPathTo(EVertex target) {
        List<EVertex> shortestPath = new ArrayList<>();

        for(EVertex v= target; v!=null; v=v.getPredecessor()) {
            shortestPath.add(v);
        }
        Collections.reverse(shortestPath);
        return shortestPath;
    }
}

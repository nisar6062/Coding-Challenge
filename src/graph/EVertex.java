package graph;

import java.util.ArrayList;
import java.util.List;

public class EVertex implements Comparable<EVertex> {
    private String data;
    private List<Edge> edgeList;
    private double distance = Double.MAX_VALUE;
    private EVertex predecessor;
    private boolean isVisited;

    EVertex(String data) {
        this.data = data;
        edgeList = new ArrayList<>();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public List<Edge> getEdgeList() {
        return edgeList;
    }

    public void setEdgeList(List<Edge> edgeList) {
        this.edgeList = edgeList;
    }

    public void addEdge(Edge edge) {
        this.edgeList.add(edge);
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public EVertex getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(EVertex predecessor) {
        this.predecessor = predecessor;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    @Override
    public int compareTo(EVertex o) {
        return Double.compare(this.distance, o.distance);
    }

    @Override
    public String toString() {
        //return "{ data:"+data+", distance: "+distance + ", predecessor: "+predecessor+"}";
        return data;
    }
}

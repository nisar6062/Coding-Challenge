package graph;

public class Edge {
    private double weight;
    private EVertex start;
    private EVertex end;

    public Edge(double weight, EVertex start, EVertex end) {
        this.weight = weight;
        this.start = start;
        this.end = end;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public EVertex getStart() {
        return start;
    }

    public void setStart(EVertex start) {
        this.start = start;
    }

    public EVertex getEnd() {
        return end;
    }

    public void setEnd(EVertex end) {
        this.end = end;
    }
}

package graph;

import java.util.Arrays;
import java.util.List;

public class Graph {

	public static void main(String[] args) {
		Vertex v1 = new Vertex(1);
		Vertex v2 = new Vertex(2);
		Vertex v3 = new Vertex(3);
		Vertex v4 = new Vertex(4);
		Vertex v5 = new Vertex(5);
		Vertex v6 = new Vertex(6);
		Vertex v7 = new Vertex(7);
		v1.addNeighbour(v2);
		v1.addNeighbour(v3);
		v2.addNeighbour(v4);
		v4.addNeighbour(v6);
		v3.addNeighbour(v5);
		v5.addNeighbour(v7);

		List<Vertex> vertexList = Arrays.asList(v1, v2, v3, v4, v5, v6, v7);

		// System.out.print("BFS = ");
		// Vertex.bfs(v1);
		System.out.println();
		System.out.print("DFS = ");
		Vertex.dfs(v1);
		new Vertex().topological(vertexList);

	}
}

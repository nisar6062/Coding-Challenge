package graph;

public class Heap {

    public static void main(String args[]) {
        int arr[] = {4, 3, 8, 1, 2, 9, 5};
        heapSort(arr);

    }

    private static void heapSort(int arr[]) {
        buildmaxHeap(arr);
        int len = arr.length;
        for (int i = len - 1; i >= 0; i--) {
            System.out.println(arr[0]);
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(0, arr, i);
        }
    }

    public static void sort(int arr[]) {
        int n = arr.length;

        // Build heap (rearrange array)
        for (int i = n / 2 - 1; i >= 0; i--)
            heapifyNew(arr, n, i);

        // One by one extract an element from heap
        for (int i = n - 1; i >= 0; i--) {
            // Move current root to end
            int temp = arr[0];
            System.out.println(arr[0]);
            arr[0] = arr[i];
            arr[i] = temp;

            // call max heapify on the reduced heap
            heapifyNew(arr, i, 0);
        }
    }

    private static void buildmaxHeap(int arr[]) {
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            heapify(i, arr, arr.length);
        }
    }

    private static void heapify(int i, int arr[], int len) {
        int max = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < len && arr[left] > arr[max]) {
            max = left;
        }
        if (right < len && arr[right] > arr[max]) {
            max = right;
        }
        //System.out.println("i: " + i + ", left: " + left + ", right: " + right + ", max: " + max);
        if (max != i) {
            int temp = arr[max];
            arr[max] = arr[i];
            arr[i] = temp;
            heapify(max, arr, len);
        }
    }

    private static void heapifyNew(int arr[], int n, int i) {
        int largest = i; // Initialize largest as root
        int l = 2 * i + 1; // left = 2*i + 1
        int r = 2 * i + 2; // right = 2*i + 2

        // If left child is larger than root
        if (l < n && arr[l] > arr[largest])
            largest = l;

        // If right child is larger than largest so far
        if (r < n && arr[r] > arr[largest])
            largest = r;

        // If largest is not root
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            heapifyNew(arr, n, largest);
        }
    }
}

package graph;

public class QuickSort {

    public static void main(String[] args) {
        int arr[] = {4, 3, 8, 1, 2, 9, 5};
        quickSort(arr, 0, arr.length - 1);
        for (int a : arr) System.out.print(a + ",");
    }

    public static void quickSort(int arr[], int l, int r) {
        if (l < r) {
            int partition = getPartiton(arr, l, r);
            quickSort(arr, l, partition - 1);
            quickSort(arr, partition + 1, r);
        }
    }

    public static int getPartiton(int[] arr, int l, int r) {
        int i = l - 1;
        int pivot = arr[r];

        for (int j = l; j < r; j++) {
            if (arr[j] < pivot) {
                //swap i & j
                i++;
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }
        }
        // swap i+1 & r
        int temp = arr[r];
        arr[r] = arr[i + 1];
        arr[i + 1] = temp;
        return i + 1;
    }
}

package graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class Vertex {

	private int data;
	private List<Vertex> neighbours;
	boolean isVisited;
	private Stack<Vertex> stack;

	public Vertex() {
		super();
	}

	public Vertex(int data) {
		super();
		this.data = data;
		this.neighbours = new ArrayList<>();
		stack = new Stack<>();
	}

	public void addNeighbour(Vertex v) {
		neighbours.add(v);
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public List<Vertex> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(List<Vertex> neighbours) {
		this.neighbours = neighbours;
	}

	public static void bfs(Vertex root) {
		Queue<Vertex> queue = new LinkedList<>();
		root.isVisited = true;
		queue.add(root);

		while (!queue.isEmpty()) {
			Vertex v = queue.poll();
			System.out.print(v.data + ", ");
			for (Vertex a : v.neighbours) {
				if (!a.isVisited) {
					a.isVisited = true;
					queue.add(a);
				}
			}
		}
	}

	public static void dfs(Vertex root) {
		root.isVisited = true;
		System.out.print(root.data + ", ");
		for (Vertex a : root.neighbours) {
			if (!a.isVisited) {
				dfs(a);
			}
		}
	}

	public void topological(Vertex root) {
		root.isVisited = true;
		for (Vertex a : root.neighbours) {
			if (!a.isVisited) {
				dfs(a);
			}
		}
		stack.push(root);
	}

	public void topological(List<Vertex> vertexList) {
		for (Vertex a : vertexList) {
			if (!a.isVisited) {
				topological(a);
			}
		}
		while(!stack.empty()) {
			Vertex v = stack.pop();
			System.out.println(v.data);
		}

	}

	public void dijkstraShortestPath(Vertex source) {

	}
}

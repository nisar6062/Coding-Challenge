package groupon;

import java.util.*;

public class Solution {

    public static void main(String[] args) {

        int arr[] = {1, 2,2,2,3,3,2,4}; //
        int arr2[] = {-1, 3, 10, 1, 14, 0, 11, 4,};

        //maxDifference(arr);
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);numbers.add(2);numbers.add(2);numbers.add(2);numbers.add(3);numbers.add(3);numbers.add(2);numbers.add(4);

        System.out.println("countDuplicates: " + countDuplicates(numbers));
    }


    public static int countDuplicates(List<Integer> numbers) {
        Set<Integer> set = new HashSet<>();
        Set<Integer> dupSet = new HashSet<>();
        for (int a : numbers) {
            if (!set.add(a)){
                dupSet.add(a);
            }

        }
        System.out.println("dupSet: " + dupSet);
        return dupSet.size();
    }


    static int maxDifference(int[] a) {
        int max = -1;
        if (a.length <= 1) {
            return max;
        }
        int prev = a[0];

        for (int i = 1; i < a.length; i++) {
            if (a[i] <= a[i - 1]) {
                continue;
            }
            if (prev > a[i - 1]) {
                prev = a[i - 1];
                System.out.println("prev:" + prev);
            }
            int currMax = a[i] - prev;

            if (max < currMax) {
                max = currMax;
            }
        }
        System.out.println("max:" + max);
        return max;

    }


}

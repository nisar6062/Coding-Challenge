package hacker.booking.y2017;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CheckInCheckOut {
    // Consider a hotel where the guest is checked in and check out. Find a day when
    // the maximum number of guests stay in a hotel.
    // Input :
    // [
    // {check-in : 1, check-out 4},
    // {check-in : 2, check-out 5},
    // {check-in : 10, check-out 12},
    // {check-in : 5, check-out 9},
    // {check-in : 5, check-out 12}
    // ] 1*IN, 4*OUT, 5*OUT, 10*IN, 12*OUT, 5*IN, 9*OUT, 5*IN, 12*OUT
    // Output : 5

    private int maxNoOfGuests(List<Integer[]> checkList) {
        List<String> list = new ArrayList<>();
        for (Integer[] inOut : checkList) {
            String in = null, out = null;
            if (inOut[0] < 10) {
                in = "0" + inOut[0] + "IN";
            } else {
                in = inOut[0] + "IN";
            }
            if (inOut[1] < 10) {
                out = "0" + inOut[1] + "OUT";
            } else {
                out = inOut[1] + "OUT";
            }
            list.add(in);
            list.add(out);
        }
        Collections.sort(list);
        System.out.println(list);
        
        int count = 0, max = Integer.MIN_VALUE;
        String day = null, maxGuestDay =  null;
        for (String io : list) {
            if (io.indexOf("IN") != -1) {
                day = io.split("IN")[0];
                count++;
            } else if (io.indexOf("OUT") != -1) {
                day = io.split("OUT")[0];
                count--;
            }
            if (max < count) {
                max = count;
                maxGuestDay = day;
            }
        }
        System.out.println("maxGuestDay:" + maxGuestDay);
        return max;
    }

    public static void main(String[] args) {
        List<String> list = Arrays.asList("01*IN", "04*OUT", "05*OUT", "10*IN", "12*OUT", "05*IN", "09*OUT", "05*IN",
                "12*OUT");
        Collections.sort(list);
        System.out.println(list);
        List<Integer[]> checkList = new ArrayList<>();
        Integer arr1[] = { 1, 4 };
        checkList.add(arr1);
        Integer arr2[] = { 2, 5 };
        Integer arr3[] = { 10, 12 };
        Integer arr4[] = { 5, 9 };
        Integer arr5[] = { 5, 12 };
        checkList.add(arr1);
        checkList.add(arr2);
        checkList.add(arr3);
        checkList.add(arr4);
        checkList.add(arr5);
        System.out.println("Result:" + new CheckInCheckOut().maxNoOfGuests(checkList));
    }
}

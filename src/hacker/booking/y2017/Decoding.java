package hacker.booking.y2017;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Decoding {
    public static void main(String[] args) throws IOException {
        // BufferedReader stdin = new BufferedReader(new
        // InputStreamReader(System.in));
        // String numbers[] = stdin.readLine().split(" ");
        // StringBuilder result = new StringBuilder();
        // result.append(numbers[0] + " ");
        // for (int i = 1; i < numbers.length; i++) {
        // int diff = Integer.parseInt(numbers[i]) - Integer.parseInt(numbers[i
        // - 1]);
        // if (-127 <= diff && diff <= 127) {
        // result.append(diff + " ");
        // } else {
        // result.append("-128 " + diff + " ");
        // }
        // }
        // System.out.println(result);
        int a[] = { 8, 25626, 25757, 24367, 24267, 16, 100, 2, 7277 };
        for (int aa : delta_encode(a))
            System.out.println(aa);
    }

    static int[] delta_encode(int[] array) {
        List<Integer> list = new ArrayList<>();
        list.add(array[0]);
        for (int i = 1; i < array.length; i++) {
            int diff = array[i] - array[i - 1];
            if (-127 <= diff && diff <= 127) {
                list.add(diff);
            } else {
                list.add(-128);
                list.add(diff);
            }
        }
        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }
}

package hacker.booking.y2017;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Hotels {
	public static void main(String[] args) throws IOException {
		// sort_hotels();
		String keywords="abcd.,b fdg.k, fdgd";
		String strChar = new String();
		List<String> wordsinLine = new ArrayList<>();
		for (int i = 0; i < keywords.length(); i++) {
			char c = keywords.charAt(i);
			if (c == '.' || c == ',') {
				continue;
			}
			if (c == ' ') {
				wordsinLine.add(strChar);
				strChar = new String();
			} else {
				strChar += c;
			}
		}
		wordsinLine.add(strChar);
		System.out.println("wordsinLine="+wordsinLine);
//		Integer hotel_ids[] = { 1, 2, 2, 1, 3, 3 };
//		List<Integer> ids = new ArrayList<>();
//		ids.addAll(Arrays.asList(hotel_ids));
//		String reviews[] = { "hotel1 is nice", "hotel2 is good", "hotel2 is nice, excellent, fantastic",
//				"hotel2 is excellent", "hotel3 is excellent", "hotel3 is excellent, nice and fantastic" };
//		List<String> reviewList = new ArrayList<>();
//		reviewList.addAll(Arrays.asList(reviews));System.out.println("------");
//		sort_hotels("nice good excellent fantastic", ids, reviewList);
	}

	public static void sort_hotels() throws IOException {
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String line = stdin.readLine().toLowerCase();
		String words[] = line.split(" ");
		List<String> wordList = Arrays.asList(words);
		int count = Integer.parseInt(stdin.readLine());
		Set<String> hotelSet = new HashSet<String>();

		Map<String, String> inputMap = new HashMap<String, String>();
		for (int i = 0; i < count; i++) {
			String hotelId = stdin.readLine();
			hotelSet.add(hotelId);
			line = stdin.readLine() + " ";
			if (inputMap.get(hotelId) != null) {
				StringBuilder str = new StringBuilder(inputMap.get(hotelId));
				str.append(line);
				inputMap.put(hotelId, str.toString());
			} else {
				inputMap.put(hotelId, line);
			}
		}
		System.out.println("hotelSet:" + hotelSet);
		System.out.println("inputMap:" + inputMap);
		Integer ranArr[] = new Integer[hotelSet.size()];
		for (Entry<String, String> entry : inputMap.entrySet()) {
			String ln = entry.getValue().toLowerCase();
			ln = ln.replaceAll("\\.", "").replaceAll("\\,", "");
			String wordsinLine[] = ln.split(" ");
			int cnt = 0;
			for (String wd : wordsinLine) {
				if (wordList.contains(wd)) {
					cnt++;
				}
			}
			ranArr[Integer.parseInt(entry.getKey()) - 1] = cnt;
		}
		StringBuilder result = new StringBuilder();
		Integer sorted[] = ranArr.clone();
		Arrays.sort(sorted, Collections.reverseOrder());
		System.out.print("ranArr:");
		for (int a : ranArr) {
			System.out.print(a + ",");
		}
		System.out.println();
		System.out.print("sorted:");
		for (int a : sorted) {
			System.out.print(a + ",");
		}
		System.out.println();
		for (int i = 0; i < sorted.length; i++) {
			for (int j = 0; j < sorted.length; j++) {
				if (sorted[i] == ranArr[j])
					result.append((j + 1) + " ");
			}
		}
		System.out.println(result);
	}

	static int[] sort_hotels(String keywords, int[] hotel_ids, String[] reviews) {
		List<String> wordList = Arrays.asList(keywords.split(" "));
		int count = hotel_ids.length;
		Set<Integer> hotelSet = new HashSet<Integer>();
		Map<Integer, String> inputMap = new HashMap<Integer, String>();
		for (int i = 0; i < count; i++) {
			hotelSet.add(hotel_ids[i]);
			String line = reviews[i];
			if (inputMap.get(hotel_ids[i]) != null) {
				StringBuilder str = new StringBuilder(inputMap.get(hotel_ids[i]));
				str.append(line + " ");
				inputMap.put(hotel_ids[i], str.toString());
			} else {
				inputMap.put(hotel_ids[i], line + " ");
			}
		}
		System.out.println("wordList:" + wordList);
		System.out.println("hotelSet:" + hotelSet);
		System.out.println("inputMap:" + inputMap);
		Integer ranArr[] = new Integer[hotelSet.size()];
		for (Entry<Integer, String> entry : inputMap.entrySet()) {
			String ln = entry.getValue().toLowerCase();
			ln = ln.replaceAll("\\.", "").replaceAll("\\,", "");
			String wordsinLine[] = ln.split(" ");
			int cnt = 0;
			for (String wd : wordsinLine) {
				if (wordList.contains(wd)) {
					cnt++;
				}
			}
			ranArr[entry.getKey() - 1] = cnt;
		}
		int result[] = new int[hotelSet.size()];
		Integer sorted[] = ranArr.clone();
		Arrays.sort(sorted, Collections.reverseOrder());
		System.out.print("ranArr:");
		for (int a : ranArr) {
			System.out.print(a + ",");
		}
		System.out.println();
		System.out.print("sorted:");
		for (int a : sorted) {
			System.out.print(a + ",");
		}
		System.out.println();
		count = 0;
		for (int i = 0; i < sorted.length; i++) {
			for (int j = 0; j < sorted.length; j++) {
				if (sorted[i] == ranArr[j]) {
					result[count++] = j + 1;
					ranArr[j] = Integer.MAX_VALUE;
				}
			}
		}
		System.out.print("result:");
		for (int a : result) {
			System.out.print(a + ",");
		}
		System.out.println();
		return result;
	}

	static List<Integer> sort_hotels(String keywords, List<Integer> hotel_ids, List<String> reviews) {
//		List<String> wordList = Arrays.asList(keywords.split(" "));
		Set<String> wordSet = new HashSet<String>();
		String strChar = new String();
		for (int i = 0; i < keywords.length(); i++) {
			char c = keywords.charAt(i);
			if (c == ' ') {
				wordSet.add(strChar);
				strChar = new String();
			} else {
				strChar += c;
			}
		}
		wordSet.add(strChar);
		int count = hotel_ids.size();
		Set<Integer> hotelSet = new HashSet<Integer>();
		Map<Integer, String> inputMap = new HashMap<Integer, String>();
		Integer ranArr[] = new Integer[hotel_ids.size()];
		for (int i = 0; i < count; i++) {
			if (ranArr[i] == null)
				ranArr[i] = 0;
			hotelSet.add(hotel_ids.get(i));
			String line = reviews.get(i);
//			String ln = line;
//			ln = ln.replaceAll("\\.", "").replaceAll("\\,", "");
//			String wordsinLine[] = ln.split(" ");
			strChar = new String();
			List<String> wordsinLine = new ArrayList<>();
			for (int j = 0; j < line.length(); j++) {
				char c = line.charAt(j);
				if (c == '.' || c == ',') {
					continue;
				}
				if (c == ' ') {
					wordsinLine.add(strChar);
					strChar = new String();
				} else {
					strChar += c;
				}
			}
			wordsinLine.add(strChar);
			
			int cnt = 0;
			for (String wd : wordsinLine) {
				if (wordSet.contains(wd)) {
					cnt++;
				}
			}
			int y = hotel_ids.get(i) - 1;
			if (inputMap.get(hotel_ids.get(i)) != null) {
				StringBuilder str = new StringBuilder(inputMap.get(hotel_ids.get(i)));
				str.append(line + " ");
				inputMap.put(hotel_ids.get(i), str.toString());
			} else {
				inputMap.put(hotel_ids.get(i), line + " ");
			}
			if (ranArr[y] != null)
				ranArr[y] += cnt;
			else
				ranArr[y] = cnt;
		}
		List<Integer> result = new ArrayList<>();
		Integer sorted[] = ranArr.clone();
		Arrays.sort(sorted, Collections.reverseOrder());
		for (int i = 0; i < sorted.length; i++) {
			for (int j = 0; j < sorted.length; j++) {
				if (sorted[i] > 0 && sorted[i] == ranArr[j]) {
					result.add(j + 1);
					ranArr[j] = Integer.MAX_VALUE;
				}
			}
		}
		return result;
	}
}

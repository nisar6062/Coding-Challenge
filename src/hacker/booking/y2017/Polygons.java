package hacker.booking.y2017;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Polygons {

    public static void main(String args[]) throws Exception {
        /*
         * Enter your code here. Read input from STDIN. Print output to STDOUT
         */
        int noOfSq = 0, noOfRect = 0, noOfPoly = 0;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        while ((line = stdin.readLine()) != null && line.trim().length() != 0) {
            String arr[] = line.split(" ");
            if (arr.length == 4) {
                if (!(Integer.parseInt(arr[0]) > 2000 || Integer.parseInt(arr[1]) > 2000
                        || Integer.parseInt(arr[2]) > 2000 || Integer.parseInt(arr[3]) > 2000
                        || Integer.parseInt(arr[0]) < -2000 || Integer.parseInt(arr[1]) < -2000
                        || Integer.parseInt(arr[2]) < -2000 || Integer.parseInt(arr[3]) < -2000)) {

                } else if (arr[0].indexOf("-") != -1 || arr[1].indexOf("-") != -1 || arr[2].indexOf("-") != -1
                        || arr[3].indexOf("-") != -1 || Integer.parseInt(arr[0]) > 2000
                        || Integer.parseInt(arr[1]) > 2000 || Integer.parseInt(arr[2]) > 2000
                        || Integer.parseInt(arr[3]) > 2000)
                    noOfPoly++;
                else if (arr[0].equals(arr[1]) && arr[1].equals(arr[2]) && arr[2].equals(arr[3]))
                    noOfSq++;
                else if (arr[0].equals(arr[2]) && arr[1].equals(arr[3]))
                    noOfRect++;
                else
                    noOfPoly++;
            }
        }
        System.out.println(noOfSq + " " + noOfRect + " " + noOfPoly);
    }
    
    static int triangle(int a, int b, int c) {
        if (a != 0 && a == b && b == c && c == a)
            return 1;
        else if (a > 0 && b > 0 && c > 0 && (a + b > c && c + b > a && a + c > b))
            return 2;
        else
            return 0;
    }

}

package hacker.booking.y2017;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TimeDiff {
    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        int currentEmp = Integer.parseInt(stdin.readLine());
        int noOfInputs = Integer.parseInt(stdin.readLine());
        List<String> timeList = new ArrayList<String>();
        for (int i = 0; i < noOfInputs; i++) {
            timeList.add(stdin.readLine());
        }
        System.out.println(timeList);
        // Collections.sort(timeList);
        System.out.println(timeList);
        //StringBuilder conflict = new StringBuilder("");
        Set<Integer> set = new HashSet<Integer>();
        int count = 0, cnt = 0;
        boolean isBreak = false;
        for (int i = 0; i < noOfInputs; i++) {
            if (!set.contains(i)) { // (conflict.indexOf(i + ",") == -1) {
                cnt++;
                String time[] = timeList.get(i).split(" ");
                int a1 = Integer.parseInt(time[0]);
                int a2 = Integer.parseInt(time[1]);
                for (int j = i + 1; j < noOfInputs; j++) {
                    if (!set.contains(j)) {// conflict.indexOf(j + ",") == -1) {
                        String time2[] = timeList.get(j).split(" ");
                        int b1 = Integer.parseInt(time2[0]);
                        int b2 = Integer.parseInt(time2[1]);
                        if (!((a1 > b1 && a1 < b2) || (a2 > b1 && a2 < b2) || (b1 > a1 && b1 < a2)
                                || (b2 > a1 && b2 < a2))) {
                            // conflict.append(i + "," + j + ",");
                            set.add(i);
                            set.add(j);
                            count++;
                            a1 = b1;
                            a2 = b2;
                            if (count + cnt == noOfInputs) {
                                isBreak = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (isBreak)
                break;
        }
        if (cnt - currentEmp < 0)
            System.out.println("0");
        else
            System.out.println(cnt - currentEmp);
    }
    
    static int howManyAgentsToAdd(int noOfCurrentAgents, List<List<Integer>> callsTimes) {
        int noOfInputs = callsTimes.size();
        Set<Integer> set = new HashSet<Integer>();
        int count = 0, cnt = 0;
        boolean isBreak = false;
        for (int i = 0; i < noOfInputs; i++) {
            if (!set.contains(i)) {
                cnt++;
                int a1 = callsTimes.get(i).get(0);
                int a2 = callsTimes.get(i).get(1);
                for (int j = i + 1; j < noOfInputs; j++) {
                    if (!set.contains(j)) {
                        int b1 = callsTimes.get(j).get(0);
                        int b2 = callsTimes.get(j).get(1);
                        if (!((a1 > b1 && a1 < b2) || (a2 > b1 && a2 < b2) || (b1 > a1 && b1 < a2)
                                || (b2 > a1 && b2 < a2))) {
                            set.add(i);
                            set.add(j);
                            count++;
                            a1 = b1;
                            a2 = b2;
                            if (count + cnt == noOfInputs) {
                                isBreak = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (isBreak)
                break;
        }
        if (cnt - noOfCurrentAgents < 0)
            return 0;
        else
            return(cnt - noOfCurrentAgents);
    }
}
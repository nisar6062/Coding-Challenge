package hackerrank.y2017;

public class AlphabetEncoding {

	public static void main(String[] args) {
		String a = "1(2)23(1100)26#(800000)24#24#";
		frequency(a);// "1226#24#"
	}

	static int[] frequency(String s) {
		String alphabet[] = "a b c d e f g h i j k l m n o p q r s t u v w x y z".split(" ");
		int res[] = new int[26];
		int i = 0;
		while (i < s.length()) {
			if (i + 2 < s.length() && String.valueOf(s.charAt(i + 2)).equals("#")) {
				int a = Integer.parseInt(String.valueOf(s.charAt(i)) + String.valueOf(s.charAt(i + 1)));
				// System.out.println(alphabet[a - 1]);
				if (i + 3 < s.length() && String.valueOf(s.charAt(i + 3)).equals("(")) {
					String in = s.substring(i + 4, s.indexOf(")", i + 3));
					res[a - 1] += Integer.parseInt(in);
					// System.out.println("====" + a + "==" + in);
					i = i + in.length() + 5;
				} else {
					res[a - 1] += 1;
					i = i + 3;
				}
			} else if (i + 1 < s.length() && String.valueOf(s.charAt(i + 1)).equals("(")) {
				int a = Integer.parseInt(String.valueOf(s.charAt(i)));
				String in = s.substring(i + 2, s.indexOf(")", i + 2));
				// System.out.println("====" + a + "==" + in);
				System.out.println(alphabet[a - 1]);
				res[a - 1] += Integer.parseInt(in);
				i = i + in.length() + 3;
			} else {
				int a = Integer.parseInt(String.valueOf(s.charAt(i)));
				System.out.println(alphabet[a - 1]);
				i++;
				res[a - 1] += 1;
			}
		}
		for (int p = 0; p < res.length; p++) {
			System.out.print(res[p] + " ");
		}
		return res;
	}
}
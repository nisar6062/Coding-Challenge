package hackerrank.y2017;

import java.util.HashSet;
import java.util.Set;

public class NonRepeatingDigits {
  public static void main(String[] args) {

    int arr[][] = {{62000, 62010}};
    // countNumbers(arr);
    System.out.println("Calc--" + calc(329) + "--------" + calc(336));
  }

  static void countNumbers_old(int[][] arr) {

    for (int i = 0; i < arr.length; i++) {
      int y[] = arr[i];
      int count = 0;
      for (int n = y[0]; n <= y[1]; n++) {
        for (int h = 0; h < 10; h++) {
          String str = String.valueOf(n);
          if (str.indexOf(h + "", str.indexOf(h + "") + 1) != -1) {
            count++;
            break;
          }
        }
      }
      System.out.println(y[1] - y[0] - count + 1);
    }
  }

  static void countNumbers(int[][] arr) {
    for (int i = 0; i < arr.length; i++) {
      int y[] = arr[i];
      int result = CalcNonRep(y[1]) - CalcNonRep(y[0]) + 1;
      System.out.println(result);
    }
  }

  static int CalcNonRep(int input) { // 6982
    String str = String.valueOf(input);
    int len = str.length();
    if (len == 1)
      return input;
    int pre[] =
        {9, 9 * 9, 9 * 9 * 8, 9 * 9 * 8 * 7, 9 * 9 * 8 * 7 * 6, 9 * 9 * 8 * 7 * 6 * 5, 9 * 9 * 8 * 7 * 6 * 5 * 4};
    int total = 0;
    int first = Integer.parseInt(str.charAt(0) + "");
    for (int i = 0; i < len - 1; i++) {
      total += pre[i];
    }
    if (len >= 2)
      total += (first - 1) * pre[len - 1] / 9;

    if (len == 2) {
      int sec = Integer.parseInt(str.charAt(1) + "");
      if (first > sec) {
        total += sec + 1;
      } else
        total += sec;
    } else {
      for (int j = 1; j < len; j++) {
        boolean isStop = false;
        int jth = Integer.parseInt(str.charAt(j) + "");
        int curr = len - j;
        Set<Integer> set = new HashSet<Integer>();
        for (int k = 0; k < j; k++) {
          int kth = Integer.parseInt(str.charAt(k) + "");
          /*
           * if (jth == kth) { isStop = true; break; }
           */
          if (jth >= kth && set.add(kth))
            jth--;

        }
        if (isStop)
          break;
        // got jth
        int start = (9 - j);
        int prdct = 1;
        for (int p = 1; p < curr; p++) {
          prdct = prdct * (start);
          start--;
        }
        if (j == len - 1)
          jth++;
        total = total + jth * prdct;
      }
    }

    System.out.println("Calc:" + input + "---" + total);
    return total;
  }

  static int calc(int num) {
    String str = String.valueOf(num);
    if (str.length() == 1) {
      return num;
    } else if (str.length() == 2) {
      int a0 = Integer.parseInt(str.charAt(0) + "");
      int a1 = Integer.parseInt(str.charAt(1) + "");
      if (a0 > a1) {
        return num - (num / 10) + 1;
      } else
        return num - (num / 10);
    } else if (str.length() == 3) { // 650
      int a0 = Integer.parseInt(str.charAt(0) + "");
      int a1 = Integer.parseInt(str.charAt(1) + "");
      int a2 = Integer.parseInt(str.charAt(2) + "");
      int sum = 90 + ((a0 - 1) * 9 * 8);
      if (a0 >= a1) {
        sum += 8 * a1;
      } else if (a1 > a0)
        sum += 8 * a1 - 10; 
      if(a0!=a1){
        if (a2 > a1 && a2 > a1) {
          sum += a2 + 1 - 2;
        } else if (a2 > a1 && a2 < a0 || (a2 < a1 && a2 > a0)) {
          sum += a2 + 1 - 1;
        } else
          sum += a2 + 1;
      } 
      return sum;
    } else if (str.length() == 4) { // 6510 5999
      int a0 = Integer.parseInt(str.charAt(0) + "");
      int a1 = Integer.parseInt(str.charAt(1) + "");
      int a2 = Integer.parseInt(str.charAt(2) + "");
      int a3 = Integer.parseInt(str.charAt(2) + "");
      int sum = 90+(9*9*8) + ((a0 - 1) * 9 * 8 * 7);


      return num;
    } else if (str.length() == 5) {
      int a0 = Integer.parseInt(str.charAt(0) + "");
      int a1 = Integer.parseInt(str.charAt(1) + "");
      int a2 = Integer.parseInt(str.charAt(2) + "");
      int a3 = Integer.parseInt(str.charAt(2) + "");
      int sum = 90 + ((a0 - 1) * 9 * 8 * 7 * 6);
      return num;
    } else if (str.length() == 6) {
      int a0 = Integer.parseInt(str.charAt(0) + "");
      int a1 = Integer.parseInt(str.charAt(1) + "");
      int a2 = Integer.parseInt(str.charAt(2) + "");
      int a3 = Integer.parseInt(str.charAt(2) + "");
      int sum = 90 + ((a0 - 1) * 9 * 8 * 7 * 6 * 5);
      return num;
    }

    return 0;
  }
}

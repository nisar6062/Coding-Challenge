package indeed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Q2 {

	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				int noOfTestCase = Integer.parseInt(br.readLine());
				while (noOfTestCase > 0) {
					String xy[] = br.readLine().split(" ");
					int x = Integer.parseInt(xy[0]);
					int y = Integer.parseInt(xy[1]);
					int numDots = Integer.parseInt(br.readLine());
					int data[][] = new int[x][y];
					while (numDots > 0) {
						String abc[] = br.readLine().split(" ");
						int a = Integer.parseInt(abc[0]);
						int b = Integer.parseInt(abc[1]);
						int dark = Integer.parseInt(abc[2]);
						for (int i = 0; i < x; i++) {
							for (int j = 0; j < y; j++) {
								data[i][j] = Math.max(dark - Math.abs(a - i + b - j), data[i][j]);
							}
						}
						numDots--;
					}
					int sum = 0;
					for (int i = 0; i < x; i++) {
						for (int j = 0; j < y; j++) {
							System.out.print(data[i][j] + ",");
							sum += data[i][j];
						}
						System.out.println();
					}
					System.out.println(sum);
					noOfTestCase--;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

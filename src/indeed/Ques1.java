package indeed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ques1 {

	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				int noOfTestCase = Integer.parseInt(br.readLine());
				while (noOfTestCase > 0) {
					String xy[] = br.readLine().split(" ");
					int x = Integer.parseInt(xy[0]);
					int y = Integer.parseInt(xy[1]);
					int numDots = Integer.parseInt(br.readLine());

					int data[][] = new int[x][y];
					while (numDots > 0) {
						String abc[] = br.readLine().split(" ");
						int a = Integer.parseInt(abc[0]);
						int b = Integer.parseInt(abc[1]);
						int dark = Integer.parseInt(abc[2]);
						data[a][b] = dark;
						numDots--;
					}

					for (int i = 0; i < x; i++) {
						for (int j = 0; j < y; j++) {
							if (data[i][j] != 0) {
								int k = data[i][j];
								int g = 1;
								while (i + g < x) {
									data[i + g][j] = Math.max(data[i][j] - g, data[i + g][j]);
									g++;
								}
								g = 1;
								while (j + g < y) {
									data[i][j + g] = Math.max(data[i][j] - g, data[i][j + g]);
									g++;
								}
								g = 1;
								while (i + g < x && j + g < y) {
									data[i + g][j + g] = Math.max(data[i][j] - 2 * g, data[i + g][j + g]);
									g++;
								}
								// negative
								g = 1;
								while (i - g >= 0) {
									data[i - g][j] = Math.max(data[i][j] - g, data[i - g][j]);
									g++;
								}
								g = 1;
								while (i - g >= 0 && j - g >= 0) {
									data[i - g][j - g] = Math.max(data[i][j] - 2 * g, data[i - g][j - g]);
									g++;
								}
								g = 1;
								while (j - g >= 0) {
									data[i][j - g] = Math.max(data[i][j] - g, data[i][j - g]);
									g++;
								}
							}
						}
					}
					int sum = 0;
					for (int i = 0; i < x; i++) {
						for (int j = 0; j < y; j++) {
							System.out.print(data[i][j] + ",");
							sum += data[i][j];
						}
						System.out.println();
					}
					System.out.println(sum);
					noOfTestCase--;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

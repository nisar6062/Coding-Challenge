package line;

import java.math.BigInteger;

public class Fibonacci {
	public static void main(String[] args) {
		// Question-1 Fibonacci
		System.out.println("8181th term: " + getFibonacci(8181));
	}

	public static BigInteger getFibonacci(Integer n) { // 0 1 1 2 3 5
		if (n == 0)
			return BigInteger.valueOf(0);
		BigInteger[] fibonacci = new BigInteger[n + 1];
		fibonacci[0] = BigInteger.valueOf(0);
		fibonacci[1] = BigInteger.valueOf(1);
		for (Integer i = 2; i < n + 1; i++) {
			fibonacci[i] = fibonacci[i - 1].add(fibonacci[i - 2]);
		}
		return fibonacci[n];
	}
}

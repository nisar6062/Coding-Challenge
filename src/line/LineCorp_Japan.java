package line;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LineCorp_Japan {
	public static void main(String[] args) {
		// Question-1 Fibonacci
		System.out.println("8181th term: " + getFibonacci(8181));
		// Question-2 rolling Dice
		System.out.println("No of ways for 610 spaces: " + getNoOfWays(610));
		// Question-3 Creating a binary tree
		TreeNode rootNode = createBinaryTree();
		System.out.print("Level Order traversal of binary tree will be:");
		levelOrderTraversal(rootNode);
	}

	public static BigInteger getFibonacci(Integer n) { // 0 1 1 2 3 5
		if (n == 0)
			return BigInteger.valueOf(0);
		BigInteger[] fibonacci = new BigInteger[n + 1];
		fibonacci[0] = BigInteger.valueOf(0);
		fibonacci[1] = BigInteger.valueOf(1);
		for (Integer i = 2; i < n + 1; i++) {
			fibonacci[i] = fibonacci[i - 1].add(fibonacci[i - 2]);
		}
		return fibonacci[n];
	}

	public static BigInteger getNoOfWays(int n) {
		// 1 - {[1]} = 1
		// 2 - {[1,1], [2]} = 2
		// 3 - {[1,1,1], [1,2], [2,1], [3]} = 4
		// 4 - {[1,1,1,1], [1,1,2], [1,2,1], [1,1,2], [1,3], [3,1], [2,2], [4]} = 8
		// so noOfWays to reach n = 2^(n-1)
		return pow(BigInteger.valueOf(2), BigInteger.valueOf(n - 1));
	}

	static BigInteger pow(BigInteger base, BigInteger exponent) {
		BigInteger result = BigInteger.ONE;
		while (exponent.signum() > 0) {
			if (exponent.testBit(0))
				result = result.multiply(base);
			base = base.multiply(base);
			exponent = exponent.shiftRight(1);
		}
		return result;
	}

	public static class TreeNode {
		int data;
		TreeNode left;
		TreeNode right;

		TreeNode(int data) {
			this.data = data;
		}
	}

	// prints in level order
	public static void levelOrderTraversal(TreeNode startNode) {
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(startNode);
		queue.add(null);
		List<Integer> list = new ArrayList<>();
		while (!queue.isEmpty()) {
			TreeNode tempNode = queue.poll();
			if (tempNode == null) {
				if (!queue.isEmpty())
					queue.add(null);
				for (int i = 0; i < list.size(); i++) {
					if (i == list.size() - 1)
						System.out.println("(" + list.get(i) + ")");
					else
						System.out.print("(" + list.get(i) + "),");
				}
				list = new ArrayList<>();
			} else {
				list.add(tempNode.data);
				if (tempNode.left != null)
					queue.add(tempNode.left);
				if (tempNode.right != null)
					queue.add(tempNode.right);
			}
		}
	}

	public static TreeNode createBinaryTree() {
		/**
		 * 2 1 3 0 7 9 1
		 **/

		TreeNode rootNode = new TreeNode(2);
		TreeNode node1 = new TreeNode(1);
		TreeNode node3 = new TreeNode(3);
		TreeNode node0 = new TreeNode(0);
		TreeNode node7 = new TreeNode(7);
		TreeNode node9 = new TreeNode(9);
		TreeNode node11 = new TreeNode(1);

		rootNode.left = node1;
		rootNode.right = node3;

		node1.left = node0;
		node1.right = node7;

		node3.left = node9;
		node3.right = node11;
		return rootNode;
	}

}

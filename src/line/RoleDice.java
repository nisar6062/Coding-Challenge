package line;

import java.math.BigInteger;

public class RoleDice {

	public static void main(String[] args) {
		System.out.println( getNoOfWays(610));
	}

	private static BigInteger getNoOfWays(int n) {
		// 1 - {[1]} = 1
		// 2 - {[1,1], [2]} = 2
		// 3 - {[1,1,1], [1,2], [2,1], [3]} = 4
		// 4 - {[1,1,1,1], [1,1,2], [1,2,1], [1,1,2], [1,3], [3,1], [2,2], [4]} = 8
		// so noOfWays to reach n = 2^(n-1)
		return power(BigInteger.valueOf(2), BigInteger.valueOf(n - 1));
	}

	private static BigInteger power(BigInteger x, BigInteger y) {
		if (y == BigInteger.ZERO)
			return BigInteger.ONE;
		else if (y.divideAndRemainder(BigInteger.valueOf(2))[1] == BigInteger.ZERO)
			return power(x, y.divide(BigInteger.valueOf(2))).multiply(power(x, y.divide(BigInteger.valueOf(2))));
		else
			return x.multiply(
					power(x, y.divide(BigInteger.valueOf(2))).multiply(power(x, y.divide(BigInteger.valueOf(2)))));
	}
}

package line;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeLevelOrder {
	public static void main(String[] args) {
		// Question-3 Creating a binary tree
		TreeNode rootNode = createBinaryTree();
		System.out.println("Level Order traversal of BT:");
		levelOrderTraversal(rootNode);
	}

	public static class TreeNode {
		int data;
		TreeNode left;
		TreeNode right;

		TreeNode(int data) {
			this.data = data;
		}
	}

	// prints in level order
	public static void levelOrderTraversal(TreeNode startNode) {
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(startNode);
		queue.add(null);
		List<Integer> list = new ArrayList<>();
		while (!queue.isEmpty()) {
			TreeNode tempNode = queue.poll();
			if (tempNode == null) {
				if (!queue.isEmpty())
					queue.add(null);
				for (int i = 0; i < list.size(); i++) {
					if (i == list.size() - 1)
						System.out.println("(" + list.get(i) + ")");
					else
						System.out.print("(" + list.get(i) + "),");
				}
				list = new ArrayList<>();
			} else {
				list.add(tempNode.data);
				if (tempNode.left != null)
					queue.add(tempNode.left);
				if (tempNode.right != null)
					queue.add(tempNode.right);
			}
		}
	}

	public static TreeNode createBinaryTree() {
		/**
		 * 2 1 3 0 7 9 1
		 **/
		TreeNode rootNode = new TreeNode(2);
		TreeNode node1 = new TreeNode(1);
		TreeNode node3 = new TreeNode(3);
		TreeNode node0 = new TreeNode(0);
		TreeNode node7 = new TreeNode(7);
		TreeNode node9 = new TreeNode(9);
		TreeNode node11 = new TreeNode(1);

		rootNode.left = node1;
		rootNode.right = node3;

		node1.left = node0;
		node1.right = node7;

		node3.left = node9;
		node3.right = node11;
		return rootNode;
	}
}

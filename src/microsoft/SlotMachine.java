package microsoft;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SlotMachine {

    /*  N Slot machine with each has its own number, N-1 slots can be increased by 1 at a time
     *  Can we reach Jackpot (All numbers equal) "7 7 7 7"
     *                   3 4 5
     */
    public static void main(String[] args) {
        SlotMachine sm = new SlotMachine();
        int arr[] = {3, 4, 1};
        System.out.println("processSlot: " + sm.processSlot(arr, 4));
    }


    private boolean processSlot(int arr[], int k) {
        if (k <= 0) {
//            System.out.println("Zero k: " + k + " arr:" + printArr(arr));
            return false;
        }
        boolean isEqual = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[0] != arr[i])
                isEqual = false;
        }
        System.out.println();
        if (isEqual) {
            return true;
        }

        isEqual = false;
        //skipping
        for (int j = 0; j < arr.length; j++) {
            if (k > 1) {
                int temp[] = arr.clone();
                //increment
                for (int i = 0; i < arr.length; i++) {
                    if (i != j) {
                        temp[i]++;
                    }
                }
                System.out.println("j: " + j + ", k: " + k + " arr:" + printArr(temp));
                isEqual = isEqual || processSlot(temp, k - 1);
            }
        }
        return isEqual;
    }

    private String printArr(int arr[]) {
        String res = "";
        for (int a : arr) {
            res += a + ",";
        }
        return res;
    }
}


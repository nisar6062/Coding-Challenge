package multithread.schedule.kry;

public class A extends B {
	void cool() {
		System.out.println("A Cool");
	}

	void aa() {
		System.out.println("B Cool");
	}

	public static void main(String[] args) {
		B b = new A();
		b.cool();
	}
}

class B {
	void cool() {
		System.out.println("B Cool");
	}

}

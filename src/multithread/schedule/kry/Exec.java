package multithread.schedule.kry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;

public class Exec {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newFixedThreadPool(1);

		Future future = executorService.submit(new Runnable() {
			public void run() {
				try {
					Thread.sleep(10000);
					System.out.println("Asynchronous task");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		Future future2 = executorService.submit(new Callable() {
			public Object call() throws Exception {
				System.out.println("Asynchronous Callable");
				return "Callable Result";
			}
		});
		
		System.out.println("===");
		future.get();
		System.out.println("===");
		
		//System.out.println("1.get() = " + future.get());
		System.out.println("2.get() = " + future2.get());
		
		
		
		
	}
}

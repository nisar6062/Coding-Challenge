package multithread.schedule.kry;
import java.util.Timer;
import java.util.TimerTask;

public class Outer {
	static class Inner {
		public static void main(String[] args) {
			// System.out.println("inside inner class Method");
			// Demo d = new Demo() {
			// void show() {
			// super.show();
			// System.out.println("i am in Flavor1Demo class");
			// }
			// };
			// d.show();
			// d.cool();

			TimerTask t = new TimerTask() {
				@Override
				public void run() {
					System.out.println("=====");
				}
			};

			Timer timer = new Timer();
			timer.schedule(t, 0, 1000);
		}
	}
}

class Demo {
	void show() {
		System.out.println("i am in show method of super class");
	}

	void cool() {
		System.out.println("i am cool cool");
	}
}

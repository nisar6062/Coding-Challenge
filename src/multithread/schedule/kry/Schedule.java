package multithread.schedule.kry;
import java.util.Timer;
import java.util.TimerTask;

import com.twitter.jsr166e.ForkJoinPool;

public class Schedule extends TimerTask {
	@Override
	public void run() {
		System.out.println("Hello World!");
	}

	static TimerTask task = new TimerTask() {
		@Override
		public void run() {
			System.out.println("Anon Hello World");
		}
	};

	public static void main(String[] args) {
		// And From your main() method or any other method
		Timer timer = new Timer();
		timer.schedule(task, 0, 5000);
		
	}

}

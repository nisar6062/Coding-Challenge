package multithreading;

public class EvenSema implements Runnable {
    OddEvenSemaphore oe;
    int max;

    public EvenSema(OddEvenSemaphore oe, int max) {
        this.oe = oe;
        this.max = max;
    }

    @Override
    public void run() {
        for (int i = 2; i <= max; i = i + 2) {
            oe.printEven(i);
        }
    }
}

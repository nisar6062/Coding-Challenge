package multithreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

public class Locks implements Runnable {
    private Lock readLock;
    private Lock writeLock;

    public Locks() {
        readLock = new ReentrantLock();
        writeLock = new ReentrantLock();
    }

    public static void main(String[] args) {
        Locks lk = new Locks();
        Thread t1 = new Thread(lk);
        Thread t2 = new Thread(lk);
        t1.setName("Read");
        t2.setName("Read 1");
        t1.start();
        t2.start();
    }

    public void read() {
        //readLock.lock();
        try {
            Thread.sleep(9000);
            System.out.println("reading: "+Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //readLock.unlock();
    }

    public void write() {
        writeLock.lock();
        readLock.lock();
        try {
            Thread.sleep(9000);
            System.out.println("writing: "+Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeLock.unlock();
        readLock.unlock();
    }

    @Override
    public void run() {
        if(Thread.currentThread().getName().indexOf("Read") != -1){
            read();
        } else {
            write();
        }

    }
}


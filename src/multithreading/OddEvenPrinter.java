package multithreading;

public class OddEvenPrinter implements Runnable {
    Printer printer;
    boolean isOdd;
    int max;

    OddEvenPrinter(Printer printer, boolean isOdd, int max) {
        this.printer = printer;
        this.isOdd = isOdd;
        this.max=max;
    }

    @Override
    public void run() {
        int start = isOdd ? 1 : 2;
        for (int i = start; i <= max; i = i + 2) {
            System.out.println("in");
            if (isOdd)
                printer.printOdd(i);
            else
                printer.printEven(i);
        }

    }

    public static void main(String args[]) {

        Printer p = new Printer();
        OddEvenPrinter oep = new OddEvenPrinter(p, true, 10);
        OddEvenPrinter oep2 = new OddEvenPrinter(p, false, 10);

        Thread t1 = new Thread(oep, "odd Thread");
        Thread t2 = new Thread(oep2, "even Thread");
        t1.start();
        t2.start();
    }
}

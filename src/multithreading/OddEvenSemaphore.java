package multithreading;

import java.util.concurrent.Semaphore;

public class OddEvenSemaphore {

	Semaphore odd = new Semaphore(1);
	Semaphore even = new Semaphore(0);

	void printEven(int num) {
		try {
			even.acquire();
			System.out.println("odd QL: " + odd.getQueueLength());
			System.out.println("even QL: " + even.getQueueLength());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName() + " :" + num);
		odd.release();
	}

	void printOdd(int num) {
		try {
			odd.acquire();
			System.out.println("odd QL: " + odd.getQueueLength());
			System.out.println("even QL: " + even.getQueueLength());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName() + " :" + num);
		even.release();
	}

	public static void main(String args[]) {
		OddEvenSemaphore oes = new OddEvenSemaphore();

		OddSema o = new OddSema(oes, 10);
		EvenSema e = new EvenSema(oes, 10);

		Thread t1 = new Thread(o, "oddThread");
		Thread t2 = new Thread(e, "evenThread");

		t1.start();
		t2.start();
	}
}

package multithreading;

public class OddSema implements Runnable {
    OddEvenSemaphore oe;
    int max;

    public OddSema(OddEvenSemaphore oe, int max) {
        this.oe = oe;
        this.max = max;
    }

    @Override
    public void run() {
        for (int i = 1; i <= max; i = i + 2) {
            oe.printOdd(i);
        }
    }
}

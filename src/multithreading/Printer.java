package multithreading;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Printer {

	boolean isOdd = true;

	synchronized void printEven(int num) {
		while (isOdd) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread().getName() + " " + num);
		isOdd = !isOdd;
		notify();
	}

	synchronized void printOdd(int num) {
		while (!isOdd) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread().getName() + " " + num);
		isOdd = !isOdd;
		notify();
	}

	static int g = 0;

	public static void main(String[] args) {
		ScheduledExecutorService metricPool = Executors.newScheduledThreadPool(1);
		metricPool.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					g++;
					System.out.println("Count : " + g);
				} catch (Exception e) {
				}
			}
		}, 5, 5, TimeUnit.SECONDS);
	}
}

package multithreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWrite implements Runnable {
    private ReadWriteLock lock;

    public ReadWrite() {
        lock = new ReentrantReadWriteLock(true);
    }

    public static void main(String[] args) {
        ReadWrite lk = new ReadWrite();
        Thread t1 = new Thread(lk);
        Thread t2 = new Thread(lk);
        Thread t3 = new Thread(lk);
        Thread t4 = new Thread(lk);t4.setName("Read_3");
        Thread t5 = new Thread(lk);t5.setName("Read_4");
        t1.setName("Read_1"); t3.setName("Read_2");
        t2.setName("Write_1");
        t1.start();t2.start();t3.start();t4.start();t5.start();
    }

    public void read() {
        lock.readLock().lock();
        try {
            Thread.sleep(2000);
            System.out.println("reading started: "+Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lock.readLock().unlock();
        System.out.println("reading ended: "+Thread.currentThread().getName());
    }

    public void write() {
        lock.writeLock().lock();
        try {
            Thread.sleep(2000);
            System.out.println("writing started: "+Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lock.writeLock().unlock();
        System.out.println("writing ended: "+Thread.currentThread().getName());
    }

    @Override
    public void run() {
        if(Thread.currentThread().getName().indexOf("Read") != -1){
            read();
        } else {
            write();
        }
    }
}

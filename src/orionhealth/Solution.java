package orionhealth;

import java.io.IOException;

public class Solution {

	public static void main(String[] args) throws IOException {
		// BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		// String line = null;
		// while ((line = stdin.readLine()) != null && line.trim().length() != 0) {
		// System.out.println("Processed: " + line);
		// }
		Solution s = new Solution();
		s.solution("my.song.mp3 11b\n" + "greatSong.flac 1000b\n" + "not3.txt 5b\n" + "video.mp4 200b\n"
				+ "game.exe 100b\n" + "mov!e.mkv 10000b");
		int l=500*100000;
		System.out.println(l);
	}

	public String solution(String S) {
		// write your code in Java SE 8
		String musicFilesExt[] = { ".mp3", ".flac", ".aac" };
		String movieFilesExt[] = { ".mkv", ".mp4", ".avi" };
		String imagesFilesExt[] = { ".bmp", ".jpg", ".gif" };
		long musicSize = 0, movieSize = 0, imageSize = 0, othersSize = 0;
		String files[] = S.split("\n");
		for (String file : files) {
			boolean isIn = false;
			for (String musicFile : musicFilesExt) {
				if (file.indexOf(musicFile) != -1) {
					String size = file.split(" ")[1].split("b")[0];
					musicSize += Long.parseLong(size);
					isIn = true;
					break;
				}
			}
			if (isIn)
				continue;
			for (String movieFile : movieFilesExt) {
				if (file.indexOf(movieFile) != -1) {
					String size = file.split(" ")[1].split("b")[0];
					movieSize += Long.parseLong(size);
					isIn = true;
					break;
				}
			}
			if (isIn)
				continue;
			for (String imageFile : imagesFilesExt) {
				if (file.indexOf(imageFile) != -1) {
					String size = file.split(" ")[1].split("b")[0];
					imageSize += Long.parseLong(size);
					isIn = true;
					break;
				}
			}
			if (isIn)
				continue;
			String size = file.split(" ")[1].split("b")[0];
			othersSize += Long.parseLong(size);
		}
		String result = "music " + musicSize + "b\n" + "images " + imageSize + "b\n" + "movies " + movieSize + "b\n"
				+ "other " + othersSize + "b";
		System.out.println(result);
		// System.out.println("musicSize:" + musicSize);
		// System.out.println("movieSize:" + movieSize);
		// System.out.println("imageSize:" + imageSize);
		// System.out.println("othersSize:" + othersSize);
		return result;
	}
}

package pt.tumba.spell; 

public final class TSTNode {

  /** Index values for accessing relatives array. */
  public final static int PARENT = 0, LOKID = 1, EQKID = 2, HIKID = 3;

  /** The key to the node. */
  public Object data;

  /** The relative nodes. */
  public TSTNode[] relatives = new TSTNode[4];

  /** The char used in the split. */
  public char splitchar;

  /**
   * Constructor method.
   *
   * @param splitchar The char used in the split.
   * @param parent The parent node.
   */
  protected TSTNode(char splitchar, TSTNode parent) {
    this.splitchar = splitchar;
    relatives[PARENT] = parent;
  }
}

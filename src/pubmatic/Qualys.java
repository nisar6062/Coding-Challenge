package pubmatic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Qualys {
	public static ArrayList<Integer> sum=new ArrayList<Integer>();
	public static ArrayList<String> path=new ArrayList<String>();
	public static String[] getOptimizedTolls(String[] input1)
	{
		int dest=Integer.parseInt(input1[0].split("#")[0]);
		//System.out.println(Arrays.asList(input1).toString()); 
		int data[][]=new int[dest+1][dest+1];
		String toDest="";
		HashMap<String, String> map=new HashMap<String, String>();
		HashMap<String, Integer> inp=new HashMap<String, Integer>();int u=0;
		for(int i=1;i<input1.length;i++){
			String in[]=input1[i].split("#"); 
			if(inp.get(in[0]+"#"+in[1])!=null){
				String hh[]={"No Solution"};
				return hh;
			}
			inp.put(in[0]+"#"+in[1], u);inp.put(in[1]+"#"+in[0], u);u++;
			data[Integer.parseInt(in[0])][Integer.parseInt(in[1])]=Integer.parseInt(in[2]);
			data[Integer.parseInt(in[1])][Integer.parseInt(in[0])]=Integer.parseInt(in[2]);
			if(input1[i].indexOf("#"+dest+"#")!=-1){
				toDest+=in[0]+"#";
			}
			
			map.put(in[0], (map.get(in[0])!=null?map.get(in[0]):"")+in[1]+",");
			
		}//System.out.println(map);
		String a="1",b="1,";
		findPath(map,data,a,b,dest);
		//System.out.println(path);System.out.println(sum);
		int max=Collections.max(sum);
		//System.out.println(max);
		ArrayList<String> res=new ArrayList<String>();
		
		for(int i=0;i<path.size();i++){
			if(sum.get(i)<max){
				String h[]=path.get(i).split(",");String f="";
				for(int j=h.length-1;j>0;j--){
					f=h[j-1]+","+h[j];
					if(path.toString().indexOf(f,path.toString().indexOf(f)+1)==-1){
						f=h[j-1]+"#"+h[j];break;
					}
				}//System.out.println("f:"+f);
				res.add(inp.get(f)+1+"#"+(max-sum.get(i)));
			}			
		}
		String ress[]=new String[res.size()+1];
		ress[0]=toDest.split("#").length+"#"+max;
		Collections.sort(res);u=1;
		for(String f:res){
			ress[u]=f;u++;
		}
		System.out.println("res:"+res);
		return ress;
	}
	private static void findPath(HashMap<String, String> map,int data[][], String a, String b,int c) {				
		if(a.equals(c+"")){
			//System.out.println(b);
			String k[]=b.split(",");int s=0;
			for(int i=0;i<k.length-1;i++){
				s+=data[Integer.parseInt(k[i])][Integer.parseInt(k[i+1])]; 
			} sum.add(s);
			path.add(b.substring(0,b.length()-1));
			return;
		}
		String h[]=map.get(a).split(",");
		for(int i=0;i<h.length;i++){ 
			if(h.length!=0){				
				findPath(map,data, h[i], b+h[i]+",", c);
			}
		}
	}
	public static void main(String[] args) {
		String input1[]={"6#8","1#2#8","1#2#7","1#5#12","2#3#4","2#4#2","3#6#6","4#6#8","5#6#10"};
		String in1[] ={"6#8","1#2#8","1#4#7","1#5#12","2#3#4","2#4#2","3#6#6","4#6#8","5#6#10"};
		String in2[] ={"1#2#7","1#2#8","1#3#10","2#4#4","2#4#3","3#4#15"};
		String hh[]=getOptimizedTolls(in1);
		for(String b:hh){System.out.println(b);}
	}

}

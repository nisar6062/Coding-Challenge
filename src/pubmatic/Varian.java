package pubmatic;

import java.util.HashSet;
import java.util.Set;

public class Varian {
	public static int minimumcost(String[] input1, int[] input2) {

		int len = input1.length;
		int data[][] = new int[len][len];
		int k = 0;
		Set<String> set =new HashSet<String>();
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				if (data[j][i] != 0) {
					data[i][j] = data[j][i];
				} else if (i != j && k < input2.length) {
					data[i][j] = input2[k];
					k++;
				}
			}
		}
		int ss=0,m=0;k=0;
		for (int i = 0; i < len; i++) {
			int s=Integer.MAX_VALUE;
			for (int j = 0; j < len; j++) {						
				if(data[j][i]<s && data[j][i]!=0 && set.add(j+" "+i)){
					s=data[j][i];  set.add(i+" "+j);
					}
				if(s>m &&s!=Integer.MAX_VALUE)
					m=s;
						
			}
			if(k<len-1)
			   ss+=s; 
			k++;
			System.out.println("** "+s+" "+m);//ss-=m;
		}System.out.println("** "+ss);
		return 0;
	}

	public static void main(String[] args) {
		int inp1[] = { 2, 3, 1, 5, 4, 3 };
		String inp2[] = { "A", "B", "C", "D" };
		System.out.println(minimumcost(inp2, inp1));
	}
}

package salesforce;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class Solution {
	LibraryController libraryController;
	private List<User> newUserList;
	private Map<String, User> userMap;
	private Map<String, Book> bookMap;

	Solution() {
		newUserList = new ArrayList<>();
		userMap = new HashMap<>();
		bookMap = new HashMap<>();
		libraryController = new LibraryController(new LibraryFactory().getLibraryDAO("In_Memory"));
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(System.in));

			while (true) {
				System.out.println("Press action number:");
				System.out.println("1. Add a User");
				System.out.println("2. Add a Book");
				System.out.println("3. Approve Users");
				System.out.println("4. Loan a Book");
				System.out.println("5. Search a Book");
				System.out.println("0. To Exit");
				int action = Integer.parseInt(br.readLine());
				if (action == 1) {
					User user = s.new User();
					System.out.println("Enter user Id");
					user.userId = br.readLine();
					System.out.println("Enter user Name");
					user.username = br.readLine();
					System.out.println("Enter user Email Id");
					user.emailId = br.readLine();
					System.out.println(s.userRegistration(user)?"user registered":"error while registering");
				} else if (action == 2) {
					Book book = s.new Book();
					System.out.println("Enter Author Id");
					book.Author = br.readLine();
					System.out.println("Enter Book Name");
					book.bookName = br.readLine();
					System.out.println("Enter category");
					book.category = Category.valueOf(br.readLine());
					System.out.println("Registered Book id:"+s.bookRegistration(book));
				} else if (action == 3) {
					s.approveRegisteredUsers();
				} else if (action == 4) {
					System.out.println("Enter User Id");
					String userid = br.readLine();
					System.out.println("Enter Book Id");
					String bookid = br.readLine();
					System.out.println(s.loanBook(userid, bookid));
				} else if (action == 5) {
					System.out.println("Enter 1 for search on Author");
					System.out.println("Enter 2 for search on category");
					int ac = Integer.parseInt(br.readLine());
					if (ac == 1) {
						System.out.println("Enter Author");
						String author = br.readLine();
						System.out.println(s.searchBook(author, null));
					} else if (ac == 2) {
						System.out.println("Enter Catogory");
						String cat = br.readLine();
						System.out.println(s.searchBook(null, Category.valueOf(cat)));
					}
				} else if (action == 0) {
					System.exit(0);
				}
			}
		} catch (Exception e) {
			System.out.println("Some Error occured!! Try Again");
		}
		// Book book = s.new Book();
		// book.Author = "Mahesh";
		// book.bookName = "The new Era";
		// book.category = Category.LITERATURE;
		//
		// Book book2 = s.new Book();
		// book2.Author = "Dan Brown";
		// book2.bookName = "Davinci Code";
		// book2.category = Category.LITERATURE;
		//
		// Book book3 = s.new Book();
		// book3.Author = "Akash";
		// book3.bookName = "Fun maths";
		// book3.category = Category.MATHS;
		//
		// User user = s.new User();
		// user.userId = "u1";
		// user.username = "Nisar";
		// user.emailId = "nisar6062@gmail.com";
		//
		// User user2 = s.new User();
		// user2.userId = "u2";
		// user2.username = "kumar";
		// user2.emailId = "kumar@gmail.com";
		//
		// s.userRegistration(user);
		// s.userRegistration(user2);
		// s.approveRegisteredUsers();
		//
		// String bookId = s.bookRegistration(book);
		// String bookId2 = s.bookRegistration(book2);
		// String bookId3 = s.bookRegistration(book3);
		//
		// System.out.println("Book Reg Id: " + bookId);
		// System.out.println("Book Reg Id: " + bookId2);
		// System.out.println("Book Reg Id: " + bookId3);
		//
		// System.out.println("Book Search On category LITERATURE: " + s.searchBook("",
		// Category.LITERATURE));
		// System.out.println("Book Search On category MATHS: " + s.searchBook("",
		// Category.MATHS));
		// System.out.println("Book Search On Author: " + s.searchBook("Mahesh", null));
		//
		// System.out.println("Loan Book : " + s.loanBook("u1", bookId));
		// System.out.println("Loan Book : " + s.loanBook("u2", bookId));

	}

	public boolean userRegistration(User user) {

		return libraryController.userRegistration(user);
	}

	public String bookRegistration(Book book) {
		return libraryController.bookRegistration(book);
	}

	public void approveRegisteredUsers() {
		libraryController.approveRegisteredUsers();
	}

	public String loanBook(String userId, String bookId) {

		return libraryController.loanBook(userId, bookId);
	}

	public List<Book> searchBook(String author, Category category) {

		return libraryController.searchBook(author, category);
	}

	/*
	 * factory class to create LibraryDAOs
	 */
	class LibraryFactory {
		private static final String IN_MEMORY = "In_Memory";

		public LibraryDAO getLibraryDAO(String implmentation) {
			if (implmentation.equals(IN_MEMORY)) {
				return new InMemoryLibraryDAO();
			}
			return null;
		}
	}

	/*
	 * The Initial entry point for the APIs
	 */
	class LibraryController {

		LibraryDAO libraryDAO;

		LibraryController(LibraryDAO libraryDAO) {
			this.libraryDAO = libraryDAO;
		}

		public boolean userRegistration(User user) {

			return libraryDAO.userRegistration(user);
		}

		public void approveRegisteredUsers() {
			libraryDAO.approveRegisteredUsers();
		}

		public String loanBook(String userId, String bookId) {
			return libraryDAO.loanBook(userId, bookId);
		}

		public String bookRegistration(Book book) {
			return libraryDAO.bookRegistration(book);
		}

		public List<Book> searchBook(String author, Category category) {
			return libraryDAO.searchBook(author, category);
		}
	}

	/*
	 * Generic Interface For Library DAO
	 */
	interface LibraryDAO {
		public boolean userRegistration(User user);

		public String bookRegistration(Book book);

		public void approveRegisteredUsers();

		public String loanBook(String userId, String bookId);

		public List<Book> searchBook(String author, Category category);
	}

	/*
	 * One Implementation Of LibraryDAO which uses InMemory DS
	 */
	class InMemoryLibraryDAO implements LibraryDAO {
		@Override
		public boolean userRegistration(User user) {
			if (userMap.get(user.userId) != null) {
				return false;
			} else {
				userMap.put(user.userId, user);
				user.status = UserStatus.IN_PROCESS;
				newUserList.add(user);
				return true;
			}
		}

		@Override
		public void approveRegisteredUsers() {
			// check all users in the new user list
			for (User user : newUserList) {
				// if it matches all criteria approve the user
				user.status = UserStatus.APPROVED;
				userMap.put(user.userId, user);
			}
			newUserList = new ArrayList<>();
		}

		@Override
		public String loanBook(String userId, String bookId) {
			Book book = bookMap.get(bookId);
			if (book == null) {
				return "No Book with given id";
			} else if (book.status.equals(BookStatus.IN_LOAN)) {
				return "Book is already loaned";
			} else {
				book.userId = userId;
				book.status = BookStatus.IN_LOAN;
				return "Book is allocated";
			}
		}

		@Override
		public String bookRegistration(Book book) {
			// just a unique I
			Random random = new Random();
			book.status = BookStatus.AVAILABLE;
			String bookId = "Book_" + random.nextInt();
			book.bookId = bookId;
			bookMap.put(bookId, book);
			return bookId;
		}

		@Override
		public List<Book> searchBook(String author, Category category) {
			List<Book> bookList = new ArrayList<>();
			bookMap.forEach((k, v) -> {
				if (v.Author.equals(author) || v.category.equals(category))
					bookList.add(v);
			});
			return bookList;
		}

	}

	enum UserStatus {
		IN_PROCESS, APPROVED, REJECTED;
	}

	enum BookStatus {
		IN_LOAN, AVAILABLE;
	}

	enum Category {
		PHYSICS, MATHS, LITERATURE;
	}

	/*
	 * This is user Pojo
	 */
	class User {
		String userId;
		String username;
		String password;
		String emailId;
		UserStatus status;

		@Override
		public String toString() {
			return "User [userId=" + userId + ", username=" + username + ", password=" + password + ", emailId="
					+ emailId + ", status=" + status + "]";
		}

	}

	class Book {
		String bookId;
		String Author;
		String bookName;
		Category category;
		BookStatus status;
		String userId;

		@Override
		public String toString() {
			return "Book [bookId=" + bookId + ", Author=" + Author + ", bookName=" + bookName + ", category=" + category
					+ ", status=" + status + ", userId=" + userId + "]";
		}
	}
}

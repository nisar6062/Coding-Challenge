package study;

import java.util.Map;
import java.util.TreeMap;

public class Permutation {

    public static void main(String[] args) {
        //new Permutation().permutation("ABCD", 0, 3);
        new Permutation().lexicalPerm("AACD");
    }

    int k = 1;

    private void permutation(String input, int l, int r) {
        if (l == r) {
            System.out.println(k + ":" + input);
            k++;
        }
        for (int i = l; i <= r; i++) {
            input = swap(input, l, i);
            permutation(input, l + 1, r);
            input = swap(input, l, i);
        }
    }

    private String swap(String input, int l, int i) {
        char arr[] = input.toCharArray();
        char a = arr[l];
        arr[l] = arr[i];
        arr[i] = a;
        return String.valueOf(arr);
    }

    private void lexicalPerm(String input) {
        Map<Character, Integer> map = new TreeMap<>();
        char arr[] = input.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (map.get(arr[i]) == null) {
                map.put(arr[i], 1);
            } else {
                map.put(arr[i], map.get(arr[i]) + 1);
            }
        }
        char str[] = new char[arr.length];
        int val[] = new int[arr.length];
        int k = 0;
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            str[k] = entry.getKey();
            val[k] = entry.getValue();
            k++;
        }
        char result[] = new char[arr.length];
        perm(str, val, result, 0);
    }

    private void perm(char str[], int val[], char result[], int level) {
        if (level == result.length) {
            System.out.println(k + ":" + String.valueOf(result));
            k++;
            return;
        }
        for (int i = 0; i < val.length; i++) {
            if (val[i] == 0) {
                continue;
            }
            result[level] = str[i];
            val[i]--;
            perm(str, val, result, level + 1);
            val[i]++;
        }


    }
}

package study;

import java.util.Arrays;
import java.util.List;

public class RobHouse {


    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(16, 7, 1, 12);

        System.out.println("maxSumAlternate: " + maxSumAlternate(list));
    }

    private static int maxSumAlternate(List<Integer> list) {
        int sumInclude = 0, sumExclude = 0;

        for (int a : list) {
            int prevIncl = sumInclude;
            sumInclude = sumExclude + a;
            if (sumExclude < prevIncl)
                sumExclude = prevIncl;
        }
        return (sumInclude > sumExclude) ? sumInclude : sumExclude;
    }
}

package study;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class StackGetMin {
    private Stack<Integer> stack = new Stack<>();
    private int min = Integer.MAX_VALUE;

    public static void main(String[] args) {
        StackGetMin st = new StackGetMin();
        List<Integer> list = Arrays.asList(5, 3, -1, 2);
        for (int a : list) {
            st.push(a);
        }
        System.out.println("stack: " + st.stack);
        System.out.println("min: " + st.min);
        for (int a : list) {
            st.pop();
            System.out.println("Min: " + st.getMin());
        }
    }

    public void push(int a) {
        if (min == Integer.MAX_VALUE) {
            min = a;
            stack.push(a);
        } else if (min > a) {
            stack.push(2 * a - min);
            min = a;
        } else {
            stack.push(a);
        }
    }

    public int pop() {
        int first = stack.pop();
        System.out.println("first:"+first+", min:"+min);
        if(first <= min){
            min = 2 * min - first;
        }
        return first;
    }

    public int getMin() {
        return min;
    }
}

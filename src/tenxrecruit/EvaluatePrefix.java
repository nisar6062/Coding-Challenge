package tenxrecruit;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author Nisar Adappadathil
 *
 */
public class EvaluatePrefix {
	// * - 6 + x -6 - - 9 6 * 0 c
	// * - 6 + x -6 - 3 * 0 c
	// String str1 = "* - 6 + x -6 - - 9 6 * 0 c";
	// String str2 = "* + 3 x + - 6 2 x"; // 3+x * 6-2+x
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int count = 1;
		while (sc.hasNextLine()) {
			String input = sc.nextLine();
			System.out.println("Case " + count + ": " + evaluatePrefix(input));
			count++;
		}
		sc.close();
	}

	private static Boolean isCharacter(char c) {
		if (c >= 97 && c <= 122)
			return true;
		else
			return false;
	}

	private static Boolean isOperand(char c) {
		if (c >= 48 && c <= 57)
			return true;
		else
			return false;
	}

	private static String evaluatePrefix(String exprsn) {
		Stack<String> stack = new Stack<String>();
		for (int j = exprsn.length() - 1; j >= 0; j--) {
			if (isOperand(exprsn.charAt(j))) {
				if (exprsn.charAt(j - 1) == '-')
					stack.push("-" + exprsn.charAt(j));
				else
					stack.push(String.valueOf(exprsn.charAt(j)));
			} else if (isCharacter(exprsn.charAt(j))) {
				stack.push(String.valueOf(exprsn.charAt(j)));
			} else if (exprsn.charAt(j) == ' ' || (exprsn.charAt(j) == '-' && isOperand(exprsn.charAt(j + 1)))) {
				// ignore
			} else if (stack.size() >= 2) {
				String o1 = stack.peek();
				stack.pop();
				String o2 = stack.peek();
				stack.pop();
				switch (exprsn.charAt(j)) {
				case '+':
					try {
						int d1 = Integer.parseInt(o1);
						int d2 = Integer.parseInt(o2);
						stack.push(String.valueOf(d1 + d2));
					} catch (Exception e) {
						stack.push("+ " + o1 + " " + o2);
					}
					break;
				case '-':
					try {
						int d1 = Integer.parseInt(o1);
						int d2 = Integer.parseInt(o2);
						stack.push(String.valueOf(d1 - d2));
					} catch (Exception e) {
						stack.push("- " + o1 + " " + o2);
					}
					break;
				case '*':
					try {
						int d1 = Integer.parseInt(o1);
						int d2 = Integer.parseInt(o2);
						stack.push(String.valueOf(d1 * d2));
					} catch (Exception e) {
						stack.push("* " + o1 + " " + o2);
					}

					break;
				case '/':
					try {
						int d1 = Integer.parseInt(o1);
						int d2 = Integer.parseInt(o2);
						stack.push(String.valueOf(d1 / d2));
					} catch (Exception e) {
						stack.push("/ " + o1 + " " + o2);
					}
					break;
				}
			}
		}
		return stack.peek();
	}
}

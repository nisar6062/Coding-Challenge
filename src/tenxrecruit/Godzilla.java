package tenxrecruit;
import java.util.Scanner;

public class Godzilla {
	// 2
	// 3 3
	// R R .
	// G . .
	// M . R
	//
	// 7 5
	// M...RR.
	// ...G...
	// ...RRR.
	// .......
	// ..RR..M

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int noOfTests = sc.nextInt();

		for (int i = 0; i < noOfTests; i++) {
			int x = Integer.parseInt(sc.next());
			int y = Integer.parseInt(sc.next());
			int gX = 0, gY = 0;
			int mX = 0, mY = 0;
			boolean gFound = false;
			char arr[][] = new char[x][y];

			for (int j = 0; j < y; j++) {
				String data = sc.next();
				arr[j] = data.toCharArray();
				int k = 0;
				if (!gFound) {
					for (char c : arr[j]) {
						if (c == 'G') {
							gX = j;
							gY = k;
							gFound = true;
							break;
						}
						k++;
					}
				}
			}

			// change gorilla position & Mech position
			boolean isFired = false;
			for (int a = 0; a < x; a++) {
				for (int b = 0; b < y; b++) {
					System.out.print(arr[a][b] + ", ");
				}
				System.out.println();
			}

			while (!isFired) {
				// gorilla move
				arr[gX][gY] = '.';
				if (gX >= 1 && arr[gX - 1][gY] == 'R') {
					gX--;
				} else if (gY < arr.length - 1 && arr[gX][gY + 1] == 'R') {
					gY++;
				} else if (gX < arr.length - 1 && arr[gX + 1][gY] == 'R') {
					gX++;
				} else if (gY >= 1 && arr[gX][gY - 1] == 'R') {
					gY--;
				} else {
					gX--;
				}
				arr[gX][gY] = 'G';

				// Mech move
				arr[mX][mY] = '.';
				for (int a = 0; a < x; a++) {
					for (int b = 0; b < y; b++) {
						if (arr[a][b] == 'M') {
							mX = a;
							mY = b;
						}
						System.out.print(arr[a][b] + ", ");
					}
					System.out.println();
				}
				
				arr[mX][mY] = 'M';

			}
		}
		sc.close();
	}
}

package tenxrecruit;
import java.util.Scanner;

public class TaxDeduction {

	// 1
	// 1000 0
	// 20
	// 3
	// 0.0 500
	// 999.5 500
	// 1000.0 500

	// 3
	// 4750.50 0
	// 8000 20
	// 10000 40
	// 60
	// 3
	// 0 10000
	// 10000 5000
	// 15000 5000
	public static void main(String[] args) {
		calculateBeforeTax();
	}

	public static void calculateBeforeTax() {
		double taxSlab[] = new double[20];
		double taxPerc[] = new double[20];
		double monthlyEarning[] = new double[20];
		double afterTaxAmount[] = new double[20];

		Scanner sc = new Scanner(System.in);
		int numOfTaxSlabs = sc.nextInt();

		for (int i = 0; i < numOfTaxSlabs; i++) {
			taxSlab[i] = sc.nextDouble();
			taxPerc[i] = sc.nextDouble();
			taxPerc[i] = taxPerc[i] / 100;
		}

		double lastTaxSlab = sc.nextDouble();
		lastTaxSlab = lastTaxSlab / 100;
		int f = sc.nextInt();

		for (int j = 0; j < f; j++) {
			monthlyEarning[j] = sc.nextDouble();
			afterTaxAmount[j] = sc.nextDouble();
			double amount = monthlyEarning[j];
			int k;
			double t = 0;
			for (k = 0; k < numOfTaxSlabs; k++) {
				amount -= taxSlab[k];
				if (amount <= 0) {
					if (amount + (afterTaxAmount[j] / (1 - taxPerc[k])) < 0) {
						t += afterTaxAmount[j] / (1 - taxPerc[k]);
						afterTaxAmount[j] = 0;
						break;
					} else {
						t -= amount;
						afterTaxAmount[j] += -amount * taxPerc[k];
						afterTaxAmount[j] += amount;
						amount = 0;
					}
				}
			}
			if (afterTaxAmount[j] > 0) {
				t += afterTaxAmount[j] / (1 - lastTaxSlab);
			}
			System.out.println(t);
		}
		sc.close();
	}
}

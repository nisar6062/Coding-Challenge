package thread.problems;

public class EvenThread implements Runnable {
  private SortEvenOdd obj;
  EvenThread(SortEvenOdd obj){
    this.obj=obj;
  }
  @Override
  public void run() {
    // TODO Auto-generated method stub
    try {
      obj.sortEven();
    } catch (InterruptedException e) {
      System.out.println("Even: "+e.getMessage());
    }
  }

  
}

package thread.problems;

public class OddThread implements Runnable {
  private SortEvenOdd obj;

  OddThread(SortEvenOdd obj) {
    this.obj = obj;
  }

  @Override
  public void run() {
    // TODO Auto-generated method stub
    try {
      obj.sortOdd();
    } catch (InterruptedException e) {
      System.out.println("Odd:"+e.getMessage());
    }
  }

}

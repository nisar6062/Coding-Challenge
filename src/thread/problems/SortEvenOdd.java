package thread.problems;

import java.util.ArrayList;
import java.util.List;

public class SortEvenOdd {
  private boolean isEvenDone = true;
  private List<Integer> list = new ArrayList<Integer>();

  public SortEvenOdd(List<Integer> list) {
    this.list = list;
  }

  private int getMinValue(boolean isEven) {
    int min = Integer.MAX_VALUE;
    for (int i = 0; i < list.size(); i++) {
      if (min < list.get(i))
        min = list.get(i);
    }
    return min;
  }

  public void sortEven() throws InterruptedException {
    synchronized (this) {
      List<Integer> evenList = new ArrayList<Integer>();
      for (int i = 0; i < list.size(); i++) {
        System.out.println("Even::" + isEvenDone);
        if (isEvenDone) {
          // System.out.println("Even thread waiting");
          wait();
          // System.out.println("Even thread woke up");
        }
        if (!isEvenDone && list.get(i) % 2 == 0) {
          // System.out.println("Even: " + list.get(i));
          evenList.add(list.get(i));
          isEvenDone = true;
          notifyAll();
        }
      }
      // print the values.......................
      for (int i = 0; i < evenList.size(); i++) {
        if (!isEvenDone) {
          System.out.println("Even: " + evenList.get(i));
          isEvenDone = true;
          notifyAll();
        } else
          wait();
      }
    }
  }

  public void sortOdd() throws InterruptedException {
    synchronized (this) {
      List<Integer> oddList = new ArrayList<Integer>();
      for (int i = 0; i < list.size(); i++) {
        // System.out.println("Odd::" + isEvenDone);
        if (!isEvenDone) {
          // System.out.println("Odd thread waiting");
          wait();
        }
        if (isEvenDone && list.get(i) % 2 == 1) {
          // System.out.println("Odd: " + list.get(i));
          oddList.add(list.get(i));
          isEvenDone = false;
          notifyAll();
          System.out.println("Odd: Notified");
        }
      }
      // print the values.......................
      for (int i = 0; i < oddList.size(); i++) {
        if (isEvenDone) {
          System.out.println("Odd: " + oddList.get(i));
          isEvenDone = false;
          notifyAll();
        } else
          wait();
      }
    }
  }

  public static void main(String[] args) {
    List<Integer> list = new ArrayList<Integer>();
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    list.add(5);
    list.add(6);
    SortEvenOdd obj = new SortEvenOdd(list);
    EvenThread even = new EvenThread(obj);
    OddThread odd = new OddThread(obj);
    Thread th1 = new Thread(even);
    Thread th2 = new Thread(odd);
    th1.start();
    th2.start();
  }
}

package walmart;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;

/*
 * Simple Java client test to connect to trial cluster, create a time series data table, fill it, query it, and save it as csv for graphing.
 */

public class Cassandra {
	// the 3 node trial Cassandra test cluster Public IPs. These are dummy values.
	static String prod = "cass-270886594-1-332341676.prod.ca-price-offer-prod.ms-df-cassandra.dfw7.prod.walmart.com";
	static String cpqa ="cass-300917153-3-312969609.qa.ca-cassandra-qa-qa.ms-df-cassandra.cdcstg2.prod.walmart.com";
	static String dcName = "dfw"; // this is the DC name you used when created
	static String user = "team";
	static String password = "app_canada_offer";

	public static void main(String[] args) {

		long t1 = 0; // time each CQL operation, t1 is start time t2 is end time, time is t2-t1
		long t2 = 0;
		long time = 0;

		Cluster.Builder clusterBuilder = Cluster.builder().addContactPoints(prod).withLoadBalancingPolicy(DCAwareRoundRobinPolicy.builder().withLocalDc(dcName).build()) // your local data
																									// centre
				.withPort(9042).withAuthProvider(new PlainTextAuthProvider(user, password));

		Cluster cluster = null;
		try {
			cluster = clusterBuilder.build();

			Metadata metadata = cluster.getMetadata();
			System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

			Session session = cluster.connect();

			ResultSet rs;

			// get all the values for a sample
			System.out.println("Getting all rows for sample...");
			t1 = System.currentTimeMillis();
			rs = session.execute("select * from canada_offer.offer_data");
			for (Row rowN : rs) {
				System.out.println(rowN.toString());
				int n = rowN.toString().indexOf("tax_code");
				System.out.println(rowN.toString().substring(n, n+20));
			}
			t2 = System.currentTimeMillis();
			time = t2 - t1;
			System.out.println("time = " + time);

		} finally {
			if (cluster != null)
				cluster.close();
		}
	}
}
package walmart;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafaReadNPost implements Runnable {
	private final static String TOPIC = "ca-cbt-test-4";
	private final static String BOOTSTRAP_SERVERS = "kafka.kafka-cluster-shared.non-prod-2.walmart.com:9092";
	private final static String cvsSplitBy = ",";

	private static Producer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return new KafkaProducer<>(props);
	}

	private static void runProducer() throws Exception {
		final Producer<String, String> producer = createProducer();
		BufferedReader br = null;
		try {
			// read csv
			String csvFile = "src/main/resources/opee.csv";
			br = new BufferedReader(new FileReader(csvFile));
			String line = br.readLine();
			int count = 0;
			while ((line = br.readLine()) != null) {
				count++;
				if(count>8000) {
					String[] data = line.split(cvsSplitBy);
					System.out.println("Count:" + count + "key=" + data[0] + " , message=" + data[1] + "]");
					final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, data[0], data[1]);
					RecordMetadata metadata = producer.send(record).get();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			producer.flush();
			producer.close();
		}
	}

	public static void main(String... args) throws Exception {
		runProducer();
	}

	@Override
	public void run() {
		try {
			runProducer();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error Occured:" + e.getMessage());
		}
	}
}

package walmart;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class KafkaConsumerExample {
	private final static String TOPIC = "canada-audit-qa";
	private final static String BOOTSTRAP_SERVERS = "kafka.kafka-cluster-shared.non-prod-2.walmart.com:9092";
	// "dal-appblx084-16.prod.walmart.com:9092"
	// kafka.kafka-canada-prod.prod-cdc.walmart.com

	static void runConsumer() throws InterruptedException {
		final Consumer<Long, String> consumer = createConsumer();
		final int giveUp = 100;
		int noRecordsCount = 0;
		while (true) {
			System.out.println("In");
			// Map<String, List<PartitionInfo>> map = consumer.listTopics();
			// for(String key : map.keySet()) {
			// System.out.println(key +"--" +map.get(key));
			// }
			final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
			System.out.println("polling done count:" + consumerRecords.count());
			if (consumerRecords.count() == 0) {
				noRecordsCount++;
				if (noRecordsCount > giveUp)
					break;
				else
					continue;
			}
			System.out.println("start");
			consumerRecords.forEach(record -> {
				System.out.println("In forEach");
				System.out.println("Consumer Record:" + record.key() + "," + record.value() + "," + record.partition()
						+ "," + record.offset());
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				consumer.commitSync();
			});

		}
		consumer.close();
		System.out.println("DONE");
	}

	private static Consumer<Long, String> createConsumer() {
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "Consumer_0003");
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		// Create the consumer using props.
		final Consumer<Long, String> consumer = new KafkaConsumer<>(props);
		// Subscribe to the topic.
		consumer.subscribe(Collections.singletonList(TOPIC));
		return consumer;
	}

	public static void main(String... args) throws Exception {
		runConsumer();
	}
}

package walmart;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducerExample {
	private final static String TOPIC = "ca-price-test-1";// "canada-catalog-sku-events-cpqa";
	private final static String BOOTSTRAP_SERVERS = "kafka.kafka-cluster-shared.non-prod-2.walmart.com:9092";

	private static Producer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return new KafkaProducer<>(props);
	}

	static void runProducer(final int sendMessageCount) throws Exception {
		final Producer<String, String> producer = createProducer();
		BufferedReader br = null;
		try {
//			for (long index = time; index < time + sendMessageCount; index++) {
//				final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC, index, "walmart " + index);
//
//				RecordMetadata metadata = producer.send(record).get();
//				long elapsedTime = System.currentTimeMillis() - time;
//				System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n",
//						record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime);
//			}

			// read csv
			String csvFile = "src/main/resources/capriceoffer.csv";
			String cvsSplitBy = ",";
			br = new BufferedReader(new FileReader(csvFile));
			String line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] data = line.split(cvsSplitBy);
				System.out.println("key=" + data[1] + " , message=" + data[2] + "]");
				final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, data[1],
						 data[2]);
				RecordMetadata metadata = producer.send(record).get();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Flushing -------");
			producer.flush();
			producer.close();
		}
	}

	public static void main(String... args) throws Exception {
		int noOfTimes = 100000;
		runProducer(noOfTimes);
	}
}

package walmart;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.spy.memcached.MemcachedClient;

public class Memcache {
	private final static Logger slf4jLogger = LoggerFactory.getLogger(Memcache.class);

	public static void mainss(String[] args) {

		slf4jLogger.info("Hi, {}", "Nisar");
		slf4jLogger.debug("Hi debug, {}", "Nisar");
		slf4jLogger.error("Hi error, {}");
		System.out.println("******");
	}

	public static void main(String[] args) throws IOException {
		String stgCDC []= {"meghacache-319419806-1-333947370.dev.gm-canada2-dev.ms-df-cache.cdcstg2.prod.walmart.com",
				 "meghacache-319419806-2-333947373.dev.gm-canada2-dev.ms-df-cache.cdcstg2.prod.walmart.com"};
		String ProdDFW[] = { "meghacache-268791665-1-354690788.prod.gm-canada2-prod.ms-df-cache.dfw5.prod.walmart.com",
				"meghacache-268791665-2-354690791.prod.gm-canada2-prod.ms-df-cache.dfw5.prod.walmart.com",
				"meghacache-268791665-3-354690794.prod.gm-canada2-prod.ms-df-cache.dfw5.prod.walmart.com",
				"meghacache-268791665-4-354690797.prod.gm-canada2-prod.ms-df-cache.dfw5.prod.walmart.com",
				"meghacache-268791665-5-354690800.prod.gm-canada2-prod.ms-df-cache.dfw5.prod.walmart.com",
				"meghacache-268791671-1-354691090.prod.gm-canada2-prod.ms-df-cache.dfw6.prod.walmart.com",
				"meghacache-268791671-2-354691093.prod.gm-canada2-prod.ms-df-cache.dfw6.prod.walmart.com",
				"meghacache-268791671-3-354691096.prod.gm-canada2-prod.ms-df-cache.dfw6.prod.walmart.com",
				"meghacache-268791671-4-354691099.prod.gm-canada2-prod.ms-df-cache.dfw6.prod.walmart.com",
				"meghacache-268791671-5-354691102.prod.gm-canada2-prod.ms-df-cache.dfw6.prod.walmart.com",
				"meghacache-268791675-1-354691379.prod.gm-canada2-prod.ms-df-cache.dfw7.prod.walmart.com",
				"meghacache-268791675-2-354691382.prod.gm-canada2-prod.ms-df-cache.dfw7.prod.walmart.com",
				"meghacache-268791675-3-354691385.prod.gm-canada2-prod.ms-df-cache.dfw7.prod.walmart.com",
				"meghacache-268791675-4-354691388.prod.gm-canada2-prod.ms-df-cache.dfw7.prod.walmart.com",
				"meghacache-268791675-5-354691391.prod.gm-canada2-prod.ms-df-cache.dfw7.prod.walmart.com" };
		String prodCDC[] = {
				"meghacache-349912757-1-354691668.prod.gm-canada2-prod.ms-df-cache.cdcprod01.prod.walmart.com",
				"meghacache-349912757-2-354691671.prod.gm-canada2-prod.ms-df-cache.cdcprod01.prod.walmart.com",
				"meghacache-349912757-3-354691674.prod.gm-canada2-prod.ms-df-cache.cdcprod01.prod.walmart.com",
				"meghacache-349912757-4-354691677.prod.gm-canada2-prod.ms-df-cache.cdcprod01.prod.walmart.com",
				"meghacache-349912757-5-354691680.prod.gm-canada2-prod.ms-df-cache.cdcprod01.prod.walmart.com",
				"meghacache-349912920-1-354691957.prod.gm-canada2-prod.ms-df-cache.cdcprod02.prod.walmart.com",
				"meghacache-349912920-2-354691960.prod.gm-canada2-prod.ms-df-cache.cdcprod02.prod.walmart.com",
				"meghacache-349912920-3-354691963.prod.gm-canada2-prod.ms-df-cache.cdcprod02.prod.walmart.com",
				"meghacache-349912920-4-354691966.prod.gm-canada2-prod.ms-df-cache.cdcprod02.prod.walmart.com",
				"meghacache-349912920-5-354691969.prod.gm-canada2-prod.ms-df-cache.cdcprod02.prod.walmart.com",
				"meghacache-349912926-1-354692246.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com",
				"meghacache-349912926-2-354692249.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com",
				"meghacache-349912926-3-354692252.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com",
				"meghacache-349912926-4-354692255.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com",
				"meghacache-349912926-5-354692258.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com",
				"meghacache-349912926-1-354692246.prod.gm-canada2-prod.ms-df-cache.cdcprod03.prod.walmart.com" };
		for (String cdc : ProdDFW) {
			MemcachedClient mcc = new MemcachedClient(new InetSocketAddress(cdc, 5000));
			long start = System.currentTimeMillis();
			System.out.println("Connection to server sucessfully");
			mcc.add("test_123", 100000, "my value---");
			System.out.println("Add Time: "+ (System.currentTimeMillis()-start));
			start = System.currentTimeMillis();
			System.out.println("Get from Cache:" + mcc.get("test_123"));
			System.out.println("Get Time: "+ (System.currentTimeMillis()-start));
		}
		// Connecting to Memcached server on localhost
		// not set data into memcached server
		// System.out.println("set status:" + mcc.set("tutorialspoint", 900,
		// "memcached").done);
		// Get value from cache config.getKey() +"_Z_"+masterZone+"_S_"+sku;
		// CPQA_Z_82_S_17ACCG9T2Q0M
		// config.getKey() +"_A_"+availableStoreId+"_S_"+sku;
	}
}

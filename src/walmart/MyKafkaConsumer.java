package walmart;

import java.util.Collections;
import java.util.Properties;
import java.util.Scanner;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

public class MyKafkaConsumer {
	private static String BOOTSTRAP_SERVERS = "dal-appblx084-16.prod.walmart.com:9092,dal-appblx084-14.prod.walmart.com:9092,dal-appblx110-24.prod.walmart.com:9092,dal-appblx110-25.prod.walmart.com:9092,dal-155-18-conblx-001-07.prod.walmart.com:9092,dal-155-18-conblx-001-08.prod.walmart.com:9092";
	private static String GROUP_ID = "ca-priceoffer-sku-ingestion-20000";
	// private static String GROUP_ID = "ca-priceoffer-inventory-ingestion-2";

	// private static String GROUP_ID = "ca-priceoffer-sku-ingestion-2";

	public static void main(String[] argv) {

		Consumer consumer = createConsumer(BOOTSTRAP_SERVERS, GROUP_ID);
		Scanner sc = new Scanner(MyKafkaConsumer.class.getClassLoader().getResourceAsStream("offset.txt"));
		while (sc.hasNextLine()) {
			String[] split = sc.nextLine().split("\t");
			String topicName = split[0];
			int partition = Integer.valueOf(split[1].replaceAll(",", ""));
			int startOffset = Integer.valueOf(split[2].replaceAll(",", ""));
			int endOffset = Integer.valueOf(split[3].replaceAll(",", ""));
			int committedOffset = Integer.valueOf(split[4].replaceAll(",", ""));
			int lag = Integer.valueOf(split[5].replaceAll(",", ""));

			if (startOffset > committedOffset) {
				System.out.println(topicName + ", " + partition + ", " + startOffset + ", " + endOffset + ", "
						+ committedOffset + ", " + (startOffset + 2000) + ", " + lag);
				consumer.commitSync(Collections.singletonMap(new TopicPartition(topicName, partition),
						new OffsetAndMetadata(startOffset + 2000)));
			}
			if (committedOffset > endOffset) {
				System.out.println("****** " + topicName + ", " + partition + ", " + startOffset + ", " + endOffset
						+ ", " + committedOffset + ", " + (startOffset + 2000) + ", " + lag);
				// consumer.commitSync(Collections.singletonMap(new TopicPartition(topicName,
				// partition), new OffsetAndMetadata(endOffset)));
			}
			//    0  1 -100  110
		}
	}

	private static Consumer<Long, String> createConsumer(String bootstrapServers, String groupId) {
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		// Create the consumer using props.
		final Consumer<Long, String> consumer = new KafkaConsumer<>(props);
		return consumer;
	}
}
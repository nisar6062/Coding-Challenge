package walmart;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallelProcessing implements Runnable {
	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		executor.execute(new ParallelProcessing());
		executor.execute(new ParallelProcessing());
		executor.execute(new ParallelProcessing());
		executor.execute(new ParallelProcessing());
	}

	@Override
	public void run() {
		int num = 100;
		for (int i = 1; i <= num; i++)
			System.out.println("hooi::" + i + "-----" + Thread.currentThread().getName());

	}
}

package walmart;

public class SKU {
	private String sku_id;
	private String sku_json;

	public String getSku_id() {
		return sku_id;
	}

	public void setSku_id(String sku_id) {
		this.sku_id = sku_id;
	}

	public String getSku_json() {
		return sku_json;
	}

	public void setSku_json(String sku_json) {
		this.sku_json = sku_json;
	}

	public SKU() {
		super();
	}

	public SKU(String sku_id, String sku_json) {
		super();
		this.sku_id = sku_id;
		this.sku_json = sku_json;
	}

	@Override
	public String toString() {
		return "[sku_id=" + sku_id + ", sku_json=" + sku_json + "]";
	}
	
}

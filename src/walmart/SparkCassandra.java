package walmart;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.CassandraRow;
import com.datastax.spark.connector.japi.rdd.CassandraJavaRDD;

import org.apache.spark.api.java.function.*;

public class SparkCassandra {
	public static void main(String[] args) {

		SparkConf sparkConf = new SparkConf();
		sparkConf.setAppName("Spark-Cassandra Integration");
		sparkConf.setMaster("local[4]");
		sparkConf.set("spark.cassandra.connection.host", "127.0.0.1");
		sparkConf.set("spark.cassandra.connection.native.port", "9042");
		sparkConf.set("spark.cassandra.connection.rpc.port", "9160");
		sparkConf.set("spark.cassandra.connection.timeout_ms", "5000");
		sparkConf.set("spark.cassandra.read.timeout_ms", "200000");
		sparkConf.set("spark.cassandra.auth.username", "cassandra");
		sparkConf.set("spark.cassandra.auth.password", "cassandra");
		JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

		String keySpaceName = "canada_offer";
		String tableName = "sku";

		CassandraJavaRDD<CassandraRow> cassandraRDD = CassandraJavaUtil.javaFunctions(javaSparkContext)
				.cassandraTable(keySpaceName, tableName);

		JavaRDD<SKU> userRDD = cassandraRDD.map(new Function<CassandraRow, SKU>() {
			private static final long serialVersionUID = -165799649937652815L;

			@Override
			public SKU call(CassandraRow row) throws Exception {
				SKU sku = new SKU();
				System.out.println(sku);
				return sku;
			}
		});
	}
}

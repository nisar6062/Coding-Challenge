package walmart;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class WordChecker {
	private static final String FILENAME = "src/main/resources/stopwords.text";
	private static final String INPUT_FILENAME = "src/main/resources/input.text";
	
	public static void main(String[] args) {
		BufferedReader br = null;
		FileReader fr = null;
		String input = null;
		try {
			String sCurrentLine = null;
			br = new BufferedReader(new FileReader(INPUT_FILENAME));
			while ((sCurrentLine = br.readLine()) != null) {
				input += sCurrentLine;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		new WordChecker().wordChecker(input);
	}

	public String wordChecker(String input) {
		Set<String> stopWords = loadStopWords();
		System.out.println(stopWords);
		System.out.println(input);
		for (String word : input.split(" ")) {
			if (!stopWords.contains(word)) {
				System.out.println(word);
			}
		}
		return null;
	}

	public Set<String> loadStopWords() {
		Set<String> stopWords = new HashSet<>();
		BufferedReader br = null;
		FileReader fr = null;
		try {
			br = new BufferedReader(new FileReader(FILENAME));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				stopWords.add(sCurrentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return stopWords;
	}
}

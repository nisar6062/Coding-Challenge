package walmart;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

public class WordCount {

	public static void main(String[] args) throws Exception {
		SparkConf conf = new SparkConf().setAppName("cbtJob").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaRDD<String> lines = sc.textFile("src/main/resources/opee.csv");
		int count = 0;
		lines.foreachPartition(new VoidFunction<Iterator<String>>() {
			private static final long serialVersionUID = 1L;
			@Override
			public void call(Iterator<String> line) throws Exception {
				System.out.println("Count: " + count + "  " + line);
				try {
					while(line.hasNext()) {
						runProducer(line.next());
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
//		lines.foreach(new VoidFunction<String>() {
//			public void call(String line) {
//				System.out.println("Count: " + count + "  " + line);
//				try {
//					runProducer(line);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
	}

	private final static String TOPIC = "ca-cbt-test-4";
	private final static String BOOTSTRAP_SERVERS = "kafka.kafka-cluster-shared.non-prod-2.walmart.com:9092";
	private final static String cvsSplitBy = ",";

	private static Producer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return new KafkaProducer<>(props);
	}

	private static String runProducer(String line) throws Exception {
		System.out.println("runProducer---->>");
		final Producer<String, String> producer = createProducer();
		BufferedReader br = null;
		try {
			int count = 0;
			String[] data = line.split(cvsSplitBy);
			count++;
			System.out.println("Count:" + count + "key=" + data[0] + " , message=" + data[1] + "]");
			final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, data[0], data[1]);
			RecordMetadata metadata = producer.send(record).get();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			producer.flush();
			producer.close();
		}
		return line;
	}
}
